package com.vokilam.core.test

import com.vokilam.servescalc.domain.dsl.recipe
import com.vokilam.servescalc.domain.dsl.recipeList
import com.vokilam.servescalc.domain.model.FullRecipe
import com.vokilam.servescalc.domain.model.Recipe
import com.vokilam.servescalc.domain.model.RecipeId
import com.vokilam.servescalc.domain.model.Serving
import com.vokilam.servescalc.domain.model.g
import com.vokilam.servescalc.domain.model.pcs

object TestData {
    private val fullRecipes = recipeList {
        recipe {
            name = "White bread dough"
            serving = Serving.NONE
            ingredients {
                +("Flour" to 500.g)
                +("Water" to 320.g)
                +("Salt" to 10.g)
                +("Yeast" to 3.29.g)
            }
        }

        recipe {
            name = "Puff pastry"
            serving = Serving.NONE
            ingredients {
                +("Flour" to 250.g)
                +("Salt" to 5.g)
                +("Cold water" to 100.g)
                +("Lemon juice" to 0.25.pcs)
                +("Unsalted butter" to 200.g)
            }
        }
    }

    val recipes: List<Recipe> = fullRecipes.map { it.recipe }
    val singleRecipe: Recipe = fullRecipes.first().recipe
    val fullRecipe: FullRecipe = fullRecipes.first()
    val unsavedRecipe: FullRecipe = recipe {
        id = RecipeId.UNDEFINED
        name = "Unsaved recipe"
        serving = Serving.Custom(8f, "persons")
        ingredients {
            +("Flour" to 250.g)
            +("Salt" to 5.g)
            +("Cold water" to 100.g)
        }
    }
}
