package com.vokilam.core.test.util

import android.annotation.SuppressLint
import androidx.arch.core.executor.TaskExecutor

@SuppressLint("RestrictedApi")
class TestTaskExecutor : TaskExecutor() {
    override fun executeOnDiskIO(runnable: Runnable) = runnable.run()
    override fun postToMainThread(runnable: Runnable) = runnable.run()
    override fun isMainThread(): Boolean = true
}