package com.vokilam.logger

import android.os.Build
import android.util.Log
import com.vokilam.logger.timber.LogCatTree
import timber.log.Timber

class Logger private constructor(val tag: String) {

    inline fun verb(message: () -> String) = log(Log.VERBOSE, tag, null, message)

    inline fun debug(message: () -> String) = log(Log.DEBUG, tag, null, message)

    inline fun info(message: () -> String) = log(Log.INFO, tag, null, message)

    inline fun warn(message: () -> String) = log(Log.WARN, tag, null, message)

    inline fun warn(tr: Throwable? = null, message: () -> String) = log(Log.WARN, tag, tr, message)

    inline fun error(message: () -> String) = log(Log.ERROR, tag, null, message)

    inline fun error(tr: Throwable? = null, message: () -> String) = log(Log.ERROR, tag, tr, message)

    inline fun log(priority: Int, tag: String, tr: Throwable?, message: () -> String) {
        if (priority < logLevel) return
        // In combination with proguard, condition on statically resolved BuildConfig flag removes `StringBuilder`
        // instances when string interpolation is used in message block
        if (BuildConfig.DEBUG) {
            val msg = "[${Thread.currentThread().name}] ${message()}"
            Timber.tag(tag)
            Timber.log(priority, tr, msg)
        }
    }

    companion object {
        private const val MAX_TAG_LENGTH = 23
        private val ANONYMOUS_CLASS = Regex("(\\$\\d+)+$")

        var logLevel: Int = Log.VERBOSE

        fun init() {
            Timber.plant(LogCatTree())
        }

        operator fun invoke(tag: String = createTag()): Logger {
            return Logger(tag)
        }

        private fun createTag(): String {
            val stackTrace = Throwable().stackTrace
            val element = stackTrace.first { it.className != Companion::class.java.name }
            return createStackElementTag(element)
        }

        private fun createStackElementTag(element: StackTraceElement): String {
            val tag = ANONYMOUS_CLASS.replace(element.className.substringAfterLast('.'), "")

            // Tag length limit was removed in API 24.
            return if (tag.length <= MAX_TAG_LENGTH || Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tag
            } else {
                tag.substring(0, MAX_TAG_LENGTH)
            }
        }
    }
}
