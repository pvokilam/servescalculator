package com.vokilam.servescalc.domain.model

import kotlin.math.pow

sealed class Serving {
    abstract val value: Float
    abstract val unit: String
    abstract fun scaleFactor(newValue: Float): Float

    data class Custom(override val value: Float, override val unit: String = "") : Serving() {
        override fun scaleFactor(newValue: Float): Float = newValue / value
    }

    data class RingDiameter(override val value: Float, override val unit: String = "") : Serving() {
        override fun scaleFactor(newValue: Float): Float = newValue.pow(2) / value.pow(2)
    }

    companion object {
        val NONE = Custom(1f, "x")
    }
}