package com.vokilam.servescalc.domain.extensions

interface Identifiable<T> {
    val value: T
}

inline fun <reified T, I> identifiableEnumValueOf(value: I, default: T = enumFirstValue()): T where T : Enum<T>, T : Identifiable<I> {
    return enumValues<T>().find { it.value == value } ?: default
}

inline fun <reified T : Enum<T>> enumValueAt(index: Int): T {
    return enumValues<T>()[index]
}

inline fun <reified T : Enum<T>> enumFirstValue(): T = enumValueAt(0)
