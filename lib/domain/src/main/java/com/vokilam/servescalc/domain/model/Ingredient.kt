package com.vokilam.servescalc.domain.model

data class Ingredient(val name: String, val amount: Amount) {
    operator fun times(factor: Number): Ingredient {
        return copy(amount = this.amount * factor)
    }
    fun asString(): String = "$name ${amount.asString()}"
}