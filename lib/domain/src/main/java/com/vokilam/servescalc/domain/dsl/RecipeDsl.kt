package com.vokilam.servescalc.domain.dsl

import com.vokilam.servescalc.domain.model.Adjustment
import com.vokilam.servescalc.domain.model.Amount
import com.vokilam.servescalc.domain.model.FullRecipe
import com.vokilam.servescalc.domain.model.Ingredient
import com.vokilam.servescalc.domain.model.Recipe
import com.vokilam.servescalc.domain.model.RecipeId
import com.vokilam.servescalc.domain.model.Serving

@DslMarker
annotation class RecipeDsl

fun recipe(init: FullRecipeBuilder.() -> Unit): FullRecipe {
    return FullRecipeBuilder().apply(init).build()
}

fun recipeList(init: FullRecipeListBuilder.() -> Unit): List<FullRecipe> {
    return FullRecipeListBuilder().apply(init).build()
}

@RecipeDsl
class FullRecipeListBuilder {
    private val recipes = mutableListOf<FullRecipe>()

    fun recipe(init: FullRecipeBuilder.() -> Unit) {
        recipes += FullRecipeBuilder().apply(init).build()
    }

    fun build(): List<FullRecipe> {
        return recipes
    }
}

@RecipeDsl
class FullRecipeBuilder {
    var id: RecipeId = RecipeId.randomId()
    lateinit var name: String
    lateinit var serving: Serving
    var adjustment: Adjustment? = null
    private var ingredients: List<Ingredient> = emptyList()

    fun ingredients(init: IngredientsBuilder.() -> Unit) {
        ingredients = IngredientsBuilder().apply(init).build()
    }

    fun build(): FullRecipe {
        return FullRecipe(
            Recipe(id, name),
            serving,
            adjustment ?: Adjustment.fromServing(serving),
            ingredients.toList()
        )
    }
}

@RecipeDsl
class IngredientsBuilder {
    private val ingredients = mutableListOf<Ingredient>()

    operator fun Pair<String, Amount>.unaryPlus() {
        ingredients += Ingredient(first, second)
    }

    fun build(): List<Ingredient> {
        return ingredients
    }
}

