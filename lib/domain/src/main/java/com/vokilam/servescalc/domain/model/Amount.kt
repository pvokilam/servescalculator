package com.vokilam.servescalc.domain.model

sealed class Amount {
    abstract operator fun times(factor: Number): Amount
    abstract fun scaleFactor(newValue: Float): Float
    abstract fun asString(): String

    data class Measurable(val value: Float, val unit: String) : Amount() {
        override fun times(factor: Number): Amount = copy(value = value * factor.toFloat())
        override fun scaleFactor(newValue: Float): Float = newValue / value
        override fun asString(): String = "$value$unit"
    }

    data class Countable(val value: Float, val unit: String = "") : Amount() { // TODO: 11/14/19 accept int
        override fun times(factor: Number): Amount = copy(value = value * factor.toFloat())
        override fun scaleFactor(newValue: Float): Float = newValue / value
        override fun asString(): String = "$value$unit"
    }

    data class Unmeasurable(val unit: String) : Amount() {
        override fun times(factor: Number): Amount = this
        override fun scaleFactor(newValue: Float): Float = Float.NaN
        override fun asString(): String = unit
    }

    object Empty : Amount() {
        override fun times(factor: Number): Amount = this
        override fun scaleFactor(newValue: Float): Float = Float.NaN
        override fun asString(): String = ""
    }
}