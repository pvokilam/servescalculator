package com.vokilam.servescalc.domain


sealed class Result<out T> {
    data class Success<out T>(val value: T) : Result<T>()
    data class Error(val exception: Exception) : Result<Nothing>()
}

val Result<*>.isSuccess get() = this is Result.Success && value != null
val Result<*>.isError get() = this is Result.Error

fun <T> Result<T>.getOrDefault(default: T): T {
    return (this as? Result.Success<T>)?.value ?: default
}

fun <T> Result<T>.exceptionOrNull(): Exception? {
    return (this as? Result.Error)?.exception
}
