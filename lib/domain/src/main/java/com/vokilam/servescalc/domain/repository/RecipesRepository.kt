package com.vokilam.servescalc.domain.repository

import com.vokilam.servescalc.domain.model.Adjustment
import com.vokilam.servescalc.domain.model.FullRecipe
import com.vokilam.servescalc.domain.model.Recipe
import com.vokilam.servescalc.domain.model.RecipeId
import com.vokilam.servescalc.domain.model.RecipeListSortOrder
import kotlinx.coroutines.flow.Flow

interface RecipesRepository {
    fun getRecipes(order: RecipeListSortOrder): Flow<List<Recipe>>
    suspend fun getFullRecipe(id: RecipeId): FullRecipe?
    suspend fun saveRecipe(recipe: FullRecipe): RecipeId
    suspend fun deleteRecipe(id: RecipeId): Boolean
    suspend fun updateAdjustment(id: RecipeId, adjustment: Adjustment): Boolean
}
