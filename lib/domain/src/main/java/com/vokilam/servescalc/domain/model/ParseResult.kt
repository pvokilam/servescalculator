package com.vokilam.servescalc.domain.model

import com.vokilam.servescalc.domain.model.Amount as AmountDomainModel
import com.vokilam.servescalc.domain.model.Ingredient as IngredientDomainModel

sealed class ParseResult {
    data class Amount(val amount: AmountDomainModel) : ParseResult()
    data class Ingredient(val ingredient: IngredientDomainModel, val amountRange: IntRange) : ParseResult()
    object Blank : ParseResult() {
        override fun toString(): String = "Blank"
    }
    object NoMatch : ParseResult() {
        override fun toString(): String = "NoMatch"
    }
}

