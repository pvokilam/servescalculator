package com.vokilam.servescalc.domain.model

import kotlin.random.Random

inline class RecipeId(val value: Long) {
    companion object {
        val UNDEFINED = RecipeId(0)
        fun randomId(): RecipeId {
            return RecipeId(Random.nextLong())
        }
    }
}
data class Recipe(val id: RecipeId, val title: String)
