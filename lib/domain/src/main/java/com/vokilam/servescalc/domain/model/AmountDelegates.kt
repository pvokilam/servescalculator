package com.vokilam.servescalc.domain.model

import kotlin.properties.ReadOnlyProperty

object AmountDelegates {
    fun measurableAmount(unit: String? = null): ReadOnlyProperty<Number, Amount> =
        ReadOnlyProperty<Number, Amount> { thisRef, property ->
            Amount.Measurable(thisRef.toFloat(), unit ?: property.name)
        }

    fun countableAmount(unit: String? = null): ReadOnlyProperty<Number, Amount> =
        ReadOnlyProperty<Number, Amount> { thisRef, property ->
            Amount.Countable(thisRef.toFloat(), unit ?: property.name)
        }
}
