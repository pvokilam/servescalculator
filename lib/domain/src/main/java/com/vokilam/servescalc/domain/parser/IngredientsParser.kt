package com.vokilam.servescalc.domain.parser

import com.vokilam.servescalc.domain.model.ParseResult

interface IngredientsParser {
    suspend fun parse(input: String): ParseResult
    suspend fun parse(ingredients: List<String>): List<ParseResult>
}