package com.vokilam.servescalc.domain.model

data class Adjustment(val newValue: Float, val type: AdjustmentType) {
    companion object {
        fun fromServing(serving: Serving): Adjustment {
            return Adjustment(serving.value, AdjustmentType.BY_SERVING)
        }

        fun fromScaleFactor(scaleFactor: Float): Adjustment {
            return Adjustment(scaleFactor, AdjustmentType.BY_AMOUNT)
        }
    }
}

enum class AdjustmentType {
    BY_SERVING,
    BY_AMOUNT
}