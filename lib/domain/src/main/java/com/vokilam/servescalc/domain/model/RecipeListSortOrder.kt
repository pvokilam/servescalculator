package com.vokilam.servescalc.domain.model

import com.vokilam.servescalc.domain.extensions.Identifiable

enum class RecipeListSortOrder(override val value: Int) : Identifiable<Int> {
    A_Z(0),
    RECENT(1);

    companion object {
        val DEFAULT = A_Z
    }
}