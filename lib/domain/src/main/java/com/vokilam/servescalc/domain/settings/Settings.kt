package com.vokilam.servescalc.domain.settings

import kotlinx.coroutines.flow.Flow

interface Settings {

    fun contains(key: String): Boolean
    fun remove(key: String)

    fun trackSettingsChanged(vararg keys: String): Flow<String>
}