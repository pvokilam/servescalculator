package com.vokilam.servescalc.domain.model

import com.vokilam.servescalc.domain.model.AmountDelegates.countableAmount
import com.vokilam.servescalc.domain.model.AmountDelegates.measurableAmount

val Number.g: Amount by measurableAmount()
val Number.kg: Amount by measurableAmount()

val Number.ml: Amount by measurableAmount()
val Number.l: Amount by measurableAmount()

val Number.tsp: Amount by measurableAmount()
val Number.tbsp: Amount by measurableAmount()
val Number.cup: Amount by measurableAmount()

val Number.oz: Amount by measurableAmount()
val Number.lb: Amount by measurableAmount()

val Number.pcs: Amount by countableAmount()
