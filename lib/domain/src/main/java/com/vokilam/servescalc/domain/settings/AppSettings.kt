package com.vokilam.servescalc.domain.settings

import com.vokilam.servescalc.domain.model.RecipeListSortOrder

interface AppSettings {
    var shouldShowImportIngredientsFtue: Boolean
    var recipeListSortOrder: RecipeListSortOrder

    companion object {
        const val KEY_SHOULD_SHOWIMPORT_INDREDIENTS_FTUE = "should_show_import_ingredients_ftue"
        const val KEY_RECIPE_LIST_SORT_ORDER = "recipe_list_sort_order"
    }
}
