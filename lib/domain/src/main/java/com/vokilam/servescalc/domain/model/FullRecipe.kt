package com.vokilam.servescalc.domain.model

data class FullRecipe(
    val recipe: Recipe,
    val serving: Serving,
    val adjustment: Adjustment,
    val ingredients: List<Ingredient>
)