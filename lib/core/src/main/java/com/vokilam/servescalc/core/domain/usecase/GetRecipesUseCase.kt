package com.vokilam.servescalc.core.domain.usecase

import com.vokilam.servescalc.domain.model.Recipe
import com.vokilam.servescalc.domain.repository.RecipesRepository
import com.vokilam.servescalc.domain.settings.AppSettings
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetRecipesUseCase @Inject constructor(
    private val repository: RecipesRepository,
    private val appSettings: AppSettings
) {

    operator fun invoke(): Flow<List<Recipe>> {
        val order = appSettings.recipeListSortOrder
        return repository.getRecipes(order)
    }
}
