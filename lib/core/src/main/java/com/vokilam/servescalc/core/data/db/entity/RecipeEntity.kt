package com.vokilam.servescalc.core.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "recipes"
)
internal data class RecipeEntity(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name = "title") val title: String,
    @Embedded val serving: ServingEntity,
    @Embedded val adjustment: AdjustmentEntity
)