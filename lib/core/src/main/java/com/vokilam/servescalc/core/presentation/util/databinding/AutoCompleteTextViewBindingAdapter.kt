package com.vokilam.servescalc.core.presentation.util.databinding

import android.widget.AutoCompleteTextView
import androidx.databinding.BindingMethod
import androidx.databinding.BindingMethods

@BindingMethods(value = [
    BindingMethod(type = AutoCompleteTextView::class, attribute = "onItemClick", method="setOnItemClickListener")
])
object AutoCompleteTextViewBindingAdapter
