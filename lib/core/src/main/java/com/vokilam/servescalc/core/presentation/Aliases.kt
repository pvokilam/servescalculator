package com.vokilam.servescalc.core.presentation

typealias ItemInteractionListener<T> = ((item: T) -> Unit)
