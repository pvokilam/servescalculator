package com.vokilam.servescalc.core.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey


@Entity(
    tableName = "ingredients",
    foreignKeys = [
        ForeignKey(
            entity = RecipeEntity::class,
            parentColumns = ["id"],
            childColumns = ["recipe_id"],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
internal data class IngredientEntity(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name = "recipe_id", index = true) val recipeId: Long,
    @ColumnInfo(name = "name") val name: String,
    @Embedded val amount: AmountEntity
)
