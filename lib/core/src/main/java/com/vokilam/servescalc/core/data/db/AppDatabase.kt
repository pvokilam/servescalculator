package com.vokilam.servescalc.core.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.vokilam.servescalc.core.data.db.entity.IngredientEntity
import com.vokilam.servescalc.core.data.db.entity.RecipeEntity

@Database(
    entities = [
        RecipeEntity::class,
        IngredientEntity::class
    ],
    version = 3
)
@TypeConverters(Converters::class)
internal abstract class AppDatabase : RoomDatabase() {
    abstract fun recipeDao(): RecipeDao
    abstract fun ingredientDao(): IngredientDao
}
