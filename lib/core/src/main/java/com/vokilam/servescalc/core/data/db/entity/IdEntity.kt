package com.vokilam.servescalc.core.data.db.entity

internal data class IdEntity(val id: Long)