package com.vokilam.servescalc.core.data.db.entity

import androidx.room.Embedded
import androidx.room.Relation

internal data class RecipeWithIngredientsEntity(
    @Embedded val recipe: RecipeEntity,
    @Relation(parentColumn = "id", entityColumn = "recipe_id") val ingredients: List<IngredientEntity>
)