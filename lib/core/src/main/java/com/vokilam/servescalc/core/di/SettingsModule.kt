package com.vokilam.servescalc.core.di

import com.vokilam.servescalc.core.data.settings.AppSettingsImpl
import com.vokilam.servescalc.domain.settings.AppSettings
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
internal abstract class SettingsModule {

    @Binds
    abstract fun provideAppSettings(impl: AppSettingsImpl): AppSettings
}