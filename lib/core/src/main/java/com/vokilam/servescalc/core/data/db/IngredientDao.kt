package com.vokilam.servescalc.core.data.db

import androidx.room.Dao
import androidx.room.Insert
import com.vokilam.servescalc.core.data.db.entity.IngredientEntity

@Dao
internal interface IngredientDao {
    @Insert
    suspend fun insertIngredients(ingredients: List<IngredientEntity>): Array<Long>
}