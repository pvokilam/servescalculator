package com.vokilam.servescalc.core.presentation.ui

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.vokilam.servescalc.core.presentation.util.LiveEvent

class ImportRecipeDataViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _importRecipeData = LiveEvent<String>()
    val importRecipeData = _importRecipeData

    private val _navigateToAddRecipe = LiveEvent<String>()
    val navigateToAddRecipe = _navigateToAddRecipe

    fun handleImportRecipeData(text: String) {
        _importRecipeData.value = text
        _navigateToAddRecipe.value = text
    }
}