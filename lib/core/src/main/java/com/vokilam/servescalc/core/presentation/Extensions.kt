package com.vokilam.servescalc.core.presentation

import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.AttrRes
import androidx.annotation.LayoutRes
import androidx.core.content.getSystemService
import androidx.core.content.res.use
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.viewbinding.ViewBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder

fun View.hideKeyboard(clearFocus: Boolean = false) {
    val imm: InputMethodManager? = context.getSystemService()
    imm?.hideSoftInputFromWindow(windowToken, 0)

    if (clearFocus) clearFocus()
}

val View.layoutInflater: LayoutInflater get() = LayoutInflater.from(context)

fun Int.toDp(): Int = (this / Resources.getSystem().displayMetrics.density).toInt()
fun Int.toPx(): Int = (this * Resources.getSystem().displayMetrics.density).toInt()

fun <T> MutableLiveData<T>.update(block: (T) -> T) {
    value = value?.run(block)
}

@Suppress("FunctionName")
fun <T> MediatorLiveData(value: T): MediatorLiveData<T> {
    return MediatorLiveData<T>().apply { setValue(value) }
}

fun MaterialAlertDialogBuilder.setView(@LayoutRes layoutResId: Int, block: (View) -> Unit): MaterialAlertDialogBuilder {
    val view = LayoutInflater.from(context).inflate(layoutResId, null).also(block)
    return setView(view)
}

val ViewBinding.context: Context
    get() = root.context

fun Context.getAttrColor(@AttrRes attr: Int, defaultColor: Int = Color.TRANSPARENT): Int {
    return obtainStyledAttributes(intArrayOf(attr)).use {
        it.getColor(0, defaultColor)
    }
}