package com.vokilam.servescalc.core.domain.usecase

import com.vokilam.logger.Logger
import com.vokilam.servescalc.domain.model.FullRecipe
import com.vokilam.servescalc.domain.model.RecipeId
import com.vokilam.servescalc.domain.repository.RecipesRepository
import javax.inject.Inject

class GetFullRecipeUseCase @Inject constructor(
    private val repository: RecipesRepository
) {
    suspend operator fun invoke(id: RecipeId): FullRecipe? {
        return repository.getFullRecipe(id).also {
            L.info { "(): id=$id, res=$it" }
        }
    }

    companion object {
        private val L = Logger()
    }
}
