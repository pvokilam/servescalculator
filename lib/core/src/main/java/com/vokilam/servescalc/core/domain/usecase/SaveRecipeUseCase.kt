package com.vokilam.servescalc.core.domain.usecase

import com.vokilam.logger.Logger
import com.vokilam.servescalc.domain.model.FullRecipe
import com.vokilam.servescalc.domain.model.RecipeId
import com.vokilam.servescalc.domain.repository.RecipesRepository
import javax.inject.Inject

class SaveRecipeUseCase @Inject constructor(
    private val repository: RecipesRepository
) {
    suspend operator fun invoke(recipe: FullRecipe): RecipeId {
        return repository.saveRecipe(recipe).also {
            L.info { "(): recipe=$recipe, res=$it" }
        }
    }

    companion object {
        private val L = Logger()
    }
}
