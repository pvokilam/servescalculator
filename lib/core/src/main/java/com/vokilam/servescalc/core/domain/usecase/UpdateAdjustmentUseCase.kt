package com.vokilam.servescalc.core.domain.usecase

import com.vokilam.logger.Logger
import com.vokilam.servescalc.domain.model.Adjustment
import com.vokilam.servescalc.domain.model.RecipeId
import com.vokilam.servescalc.domain.repository.RecipesRepository
import javax.inject.Inject

class UpdateAdjustmentUseCase @Inject constructor(
    private val repository: RecipesRepository
) {
    suspend operator fun invoke(id: RecipeId, adjustment: Adjustment): Boolean {
        return repository.updateAdjustment(id, adjustment).also {
            L.info { "(): id=$id, adjustment=$adjustment, res=$it" }
        }
    }

    companion object {
        private val L = Logger()
    }
}
