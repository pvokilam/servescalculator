package com.vokilam.servescalc.core.presentation.util.databinding

import androidx.core.widget.ContentLoadingProgressBar
import androidx.databinding.BindingAdapter

object ContentLoadingProgressBarBindingAdapter {
    @JvmStatic
    @BindingAdapter("showProgress")
    fun setShowProgress(view: ContentLoadingProgressBar, show: Boolean) {
        if (show) view.show() else view.hide()
    }
}