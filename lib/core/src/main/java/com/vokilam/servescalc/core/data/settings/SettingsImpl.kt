package com.vokilam.servescalc.core.data.settings

import android.content.SharedPreferences
import androidx.core.content.edit
import com.vokilam.logger.Logger
import com.vokilam.servescalc.domain.settings.Settings
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

class SettingsImpl(private val prefs: SharedPreferences) : Settings {

    override fun contains(key: String): Boolean = prefs.contains(key)

    override fun remove(key: String) = prefs.edit { remove(key) }

    override fun trackSettingsChanged(vararg keys: String): Flow<String> = callbackFlow {
        val listener = SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
            if (keys.contains(key)) {
                L.verb { "trackSettingsChanged(): changed key '$key'" }
                offer(key)
            }
        }

        L.verb { "trackSettingsChanged(): register listener for keys ${keys.toList()}" }
        prefs.registerOnSharedPreferenceChangeListener(listener)

        awaitClose {
            L.verb { "trackSettingsChanged(): unregister listener for keys ${keys.toList()}" }
            prefs.unregisterOnSharedPreferenceChangeListener(listener)
        }
    }

    companion object {
        private val L = Logger()
    }
}