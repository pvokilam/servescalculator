package com.vokilam.servescalc.core.domain.usecase

import com.vokilam.servescalc.domain.model.RecipeId
import com.vokilam.servescalc.domain.repository.RecipesRepository
import javax.inject.Inject

class DeleteRecipeUseCase @Inject constructor(
    private val repository: RecipesRepository
) {
    suspend operator fun invoke(id: RecipeId): Boolean {
        return repository.deleteRecipe(id)
    }
}