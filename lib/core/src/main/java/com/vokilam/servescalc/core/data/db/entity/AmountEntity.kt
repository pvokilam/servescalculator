package com.vokilam.servescalc.core.data.db.entity

import androidx.room.ColumnInfo

internal data class AmountEntity(
    @ColumnInfo(name = "value") val value: Float,
    @ColumnInfo(name = "unit") val unit: String,
    @ColumnInfo(name = "type") val type: Type
) {
    enum class Type(val value: Int) {
        MEASURABLE(0),
        COUNTABLE(1),
        UNMEASURABLE(2),
        EMPTY(3)
    }
}