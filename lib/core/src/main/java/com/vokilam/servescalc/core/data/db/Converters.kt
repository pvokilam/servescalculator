package com.vokilam.servescalc.core.data.db

import androidx.room.TypeConverter
import com.vokilam.servescalc.core.data.db.entity.AdjustmentEntity
import com.vokilam.servescalc.core.data.db.entity.AmountEntity
import com.vokilam.servescalc.core.data.db.entity.ServingEntity
import com.vokilam.servescalc.domain.extensions.identifiableEnumValueOf

internal class Converters {
    @TypeConverter
    fun convertAmountEntity(value: Int): AmountEntity.Type {
        return AmountEntity.Type.values().firstOrNull { it.value == value }
            ?: AmountEntity.Type.EMPTY
    }

    @TypeConverter
    fun convertAmountEntity(value: AmountEntity.Type): Int {
        return value.value
    }

    @TypeConverter
    fun convertServingEntity(value: Int): ServingEntity.Type {
        return ServingEntity.Type.values().firstOrNull { it.value == value }
            ?: ServingEntity.Type.NONE
    }

    @TypeConverter
    fun convertServingEntity(value: ServingEntity.Type): Int {
        return value.value
    }

    @TypeConverter
    fun convertAdjustmentEntity(value: Int): AdjustmentEntity.Type {
        return identifiableEnumValueOf(value, AdjustmentEntity.Type.BY_SERVING)
    }

    @TypeConverter
    fun convertAdjustmentEntity(value: AdjustmentEntity.Type): Int {
        return value.value
    }
}