package com.vokilam.servescalc.core.domain

fun IntRange.shift(offset: Int): IntRange {
    return IntRange(
        (start + offset).coerceIn(Int.MIN_VALUE..Int.MAX_VALUE),
        (endInclusive + offset).coerceIn(Int.MIN_VALUE..Int.MAX_VALUE)
    )
}