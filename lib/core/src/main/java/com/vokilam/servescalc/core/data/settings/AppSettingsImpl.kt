package com.vokilam.servescalc.core.data.settings

import android.content.SharedPreferences
import com.vokilam.servescalc.domain.extensions.identifiableEnumValueOf
import com.vokilam.servescalc.domain.model.RecipeListSortOrder
import com.vokilam.servescalc.domain.settings.AppSettings
import com.vokilam.servescalc.domain.settings.AppSettings.Companion.KEY_RECIPE_LIST_SORT_ORDER
import com.vokilam.servescalc.domain.settings.AppSettings.Companion.KEY_SHOULD_SHOWIMPORT_INDREDIENTS_FTUE
import com.vokilam.servescalc.domain.settings.Settings
import javax.inject.Inject

internal class AppSettingsImpl @Inject constructor(
    private val prefs: SharedPreferences
) : AppSettings, Settings by SettingsImpl(prefs) {

    override var shouldShowImportIngredientsFtue by prefs.booleanPref(KEY_SHOULD_SHOWIMPORT_INDREDIENTS_FTUE, true)

    private var _recipeListSortOrder by prefs.intPref(KEY_RECIPE_LIST_SORT_ORDER, RecipeListSortOrder.DEFAULT.value)
    override var recipeListSortOrder: RecipeListSortOrder
        get() = identifiableEnumValueOf(_recipeListSortOrder, RecipeListSortOrder.DEFAULT)
        set(value) { _recipeListSortOrder = value.value }
}
