package com.vokilam.servescalc.core.data.settings

import android.content.SharedPreferences
import androidx.core.content.edit
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

fun SharedPreferences.stringPref(
    key: String,
    defaultValue: String = ""
): ReadWriteProperty<Any, String> = object : ReadWriteProperty<Any, String> {
    override fun getValue(thisRef: Any, property: KProperty<*>): String = getString(key, defaultValue) ?: defaultValue
    override fun setValue(thisRef: Any, property: KProperty<*>, value: String) = edit { putString(key, value) }
}

fun SharedPreferences.intPref(
    key: String,
    defaultValue: Int = 0
): ReadWriteProperty<Any, Int> = object : ReadWriteProperty<Any, Int> {
    override fun getValue(thisRef: Any, property: KProperty<*>): Int = getInt(key, defaultValue)
    override fun setValue(thisRef: Any, property: KProperty<*>, value: Int) = edit { putInt(key, value) }
}

fun SharedPreferences.booleanPref(
    key: String,
    defaultValue: Boolean = false
): ReadWriteProperty<Any, Boolean> = object : ReadWriteProperty<Any, Boolean> {
    override fun getValue(thisRef: Any, property: KProperty<*>): Boolean = getBoolean(key, defaultValue)
    override fun setValue(thisRef: Any, property: KProperty<*>, value: Boolean) = edit { putBoolean(key, value) }
}

fun SharedPreferences.longPref(
    key: String,
    defaultValue: Long = 0L
): ReadWriteProperty<Any, Long> = object : ReadWriteProperty<Any, Long> {
    override fun getValue(thisRef: Any, property: KProperty<*>): Long = getLong(key, defaultValue)
    override fun setValue(thisRef: Any, property: KProperty<*>, value: Long) = edit { putLong(key, value) }
}

fun SharedPreferences.floatPref(
    key: String,
    defaultValue: Float = 0f
): ReadWriteProperty<Any, Float> = object : ReadWriteProperty<Any, Float> {
    override fun getValue(thisRef: Any, property: KProperty<*>): Float = getFloat(key, defaultValue)
    override fun setValue(thisRef: Any, property: KProperty<*>, value: Float) = edit { putFloat(key, value) }
}
