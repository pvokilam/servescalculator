package com.vokilam.servescalc.core.data.db.entity

import androidx.room.ColumnInfo

internal data class ServingEntity(
    @ColumnInfo(name = "serving_value", defaultValue = "0") val value: Float,
    @ColumnInfo(name = "serving_unit", defaultValue = "") val unit: String,
    @ColumnInfo(name = "serving_type", defaultValue = "0") val type: Type
) {
    enum class Type(val value: Int) {
        NONE(0),
        CUSTOM(1),
        RING_DIAMETER(2)
    }
}