package com.vokilam.servescalc.core.data.repository

import androidx.room.withTransaction
import com.vokilam.logger.Logger
import com.vokilam.servescalc.core.data.db.AdjustmentUpdateEntity
import com.vokilam.servescalc.core.data.db.AppDatabase
import com.vokilam.servescalc.core.data.db.IngredientDao
import com.vokilam.servescalc.core.data.db.RecipeDao
import com.vokilam.servescalc.core.data.db.entity.RecipeEntity
import com.vokilam.servescalc.core.data.mapper.toAdjustmentEntity
import com.vokilam.servescalc.core.data.mapper.toFullRecipe
import com.vokilam.servescalc.core.data.mapper.toIdEntity
import com.vokilam.servescalc.core.data.mapper.toIngredientEntity
import com.vokilam.servescalc.core.data.mapper.toRecipe
import com.vokilam.servescalc.core.data.mapper.toRecipeEntity
import com.vokilam.servescalc.domain.model.Adjustment
import com.vokilam.servescalc.domain.model.FullRecipe
import com.vokilam.servescalc.domain.model.Recipe
import com.vokilam.servescalc.domain.model.RecipeId
import com.vokilam.servescalc.domain.model.RecipeListSortOrder
import com.vokilam.servescalc.domain.model.RecipeListSortOrder.A_Z
import com.vokilam.servescalc.domain.model.RecipeListSortOrder.RECENT
import com.vokilam.servescalc.domain.repository.RecipesRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

internal class RecipesRepositoryImpl @Inject constructor(
    private val db: AppDatabase,
    private val recipeDao: RecipeDao,
    private val ingredientDao: IngredientDao
) : RecipesRepository {

    override fun getRecipes(order: RecipeListSortOrder): Flow<List<Recipe>> {
        L.debug { "getRecipes(): order=$order" }
        return when (order) {
            A_Z -> recipeDao.getRecipesOrderedByAlpha()
            RECENT -> recipeDao.getRecipesOrderedByRecent()
        }.map { it.map(RecipeEntity::toRecipe) }
    }

    override suspend fun getFullRecipe(id: RecipeId): FullRecipe? {
        L.debug { "getFullRecipe(): $id" }
        return recipeDao.getRecipeWithIngredients(id.value)?.toFullRecipe()
    }

    override suspend fun saveRecipe(recipe: FullRecipe): RecipeId {
        L.debug { "saveRecipe(): $recipe" }
        return db.withTransaction {
            val recipeEntity = recipe.toRecipeEntity()
            val rowid = recipeDao.insertRecipe(recipeEntity)
            L.verb { "saveRecipe() insert recipe: rowid=$rowid, recipe=$recipeEntity" }

            val ingredientEntities = recipe.ingredients.map { it.toIngredientEntity(rowid) }
            val rowids = ingredientDao.insertIngredients(ingredientEntities)
            L.verb { "saveRecipe() insert ingredients: rowids=$rowids, ingredients=$ingredientEntities" }

            RecipeId(rowid)
        }
    }

    override suspend fun deleteRecipe(id: RecipeId): Boolean {
        L.debug { "deleteRecipe(): $id" }
        return recipeDao.deleteRecipe(id.toIdEntity()) > 0
    }

    override suspend fun updateAdjustment(id: RecipeId, adjustment: Adjustment): Boolean {
        L.debug { "updateAdjustment(): id=$id, adjustment=$adjustment" }
        val adjustmentEntity = adjustment.toAdjustmentEntity()
        return recipeDao.updateAdjustment(AdjustmentUpdateEntity(id.value, adjustmentEntity.value, adjustmentEntity.type)) > 0
    }

    companion object {
        private val L = Logger()
    }
}