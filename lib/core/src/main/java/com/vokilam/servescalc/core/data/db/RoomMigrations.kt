package com.vokilam.servescalc.core.data.db

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

internal object RoomMigrations {

    val M_1_2 = object : Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            // Add serving columns
            database.execSQL("ALTER TABLE recipes ADD COLUMN `serving_value` REAL NOT NULL DEFAULT 0")
            database.execSQL("ALTER TABLE recipes ADD COLUMN `serving_unit` TEXT NOT NULL DEFAULT ''")
            database.execSQL("ALTER TABLE recipes ADD COLUMN `serving_type` INTEGER NOT NULL DEFAULT 0")
        }
    }

    val M_2_3 = object : Migration(2, 3) {
        override fun migrate(database: SupportSQLiteDatabase) {
            // Add adjustment columns
            database.execSQL("ALTER TABLE recipes ADD COLUMN `adjustment_value` REAL NOT NULL DEFAULT 0")
            database.execSQL("ALTER TABLE recipes ADD COLUMN `adjustment_type` INTEGER NOT NULL DEFAULT 0")
            // serving_type 0 (none) is now deprecated
            database.execSQL("UPDATE recipes SET serving_value = 1, serving_type = 1, serving_unit='x' WHERE serving_type = 0")
            // Set default adjustment_value
            database.execSQL("UPDATE recipes SET `adjustment_value` = `serving_value`")
        }
    }
}