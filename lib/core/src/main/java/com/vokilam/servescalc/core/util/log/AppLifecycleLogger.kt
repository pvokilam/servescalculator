package com.vokilam.servescalc.core.util.log

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.vokilam.logger.Logger

object AppLifecycleLogger : LifecycleObserver {
    private val L = Logger()

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onMoveToForeground() = L.verb { "onMoveToForeground()" }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onMoveToBackground() = L.verb { "onMoveToBackground()" }
}
