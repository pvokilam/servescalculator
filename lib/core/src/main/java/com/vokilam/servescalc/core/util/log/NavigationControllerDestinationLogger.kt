package com.vokilam.servescalc.core.util.log

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import com.vokilam.logger.Logger

object NavigationControllerDestinationLogger : NavController.OnDestinationChangedListener {
    private val L = Logger()

    override fun onDestinationChanged(controller: NavController, destination: NavDestination, arguments: Bundle?) {
        L.verb { "onDestinationChanged(): controller=$controller, destination=${destination.label}, args=$arguments" }
    }
}
