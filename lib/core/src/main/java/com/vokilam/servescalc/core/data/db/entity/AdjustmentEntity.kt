package com.vokilam.servescalc.core.data.db.entity

import androidx.room.ColumnInfo
import com.vokilam.servescalc.domain.extensions.Identifiable

internal data class AdjustmentEntity(
    @ColumnInfo(name = "adjustment_value", defaultValue = "0") val value: Float,
    @ColumnInfo(name = "adjustment_type", defaultValue = "0") val type: Type
) {
    enum class Type(override val value: Int) : Identifiable<Int> {
        BY_SERVING(0),
        BY_AMOUNT(1)
    }
}