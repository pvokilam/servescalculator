package com.vokilam.servescalc.core.data.db

import androidx.room.*
import com.vokilam.servescalc.core.data.db.entity.AdjustmentEntity
import com.vokilam.servescalc.core.data.db.entity.IdEntity
import com.vokilam.servescalc.core.data.db.entity.RecipeEntity
import com.vokilam.servescalc.core.data.db.entity.RecipeWithIngredientsEntity
import kotlinx.coroutines.flow.Flow

@Dao
internal interface RecipeDao {
    @Query("SELECT * FROM recipes ORDER BY id DESC")
    fun getRecipesOrderedByRecent(): Flow<List<RecipeEntity>>

    @Query("SELECT * FROM recipes ORDER BY title ASC")
    fun getRecipesOrderedByAlpha(): Flow<List<RecipeEntity>>

    @Transaction
    @Query("SELECT * FROM recipes WHERE id = :recipeId")
    suspend fun getRecipeWithIngredients(recipeId: Long): RecipeWithIngredientsEntity?

    @Insert
    suspend fun insertRecipe(recipeEntity: RecipeEntity): Long

    @Delete(entity = RecipeEntity::class)
    suspend fun deleteRecipe(id: IdEntity): Int

    @Update(entity = RecipeEntity::class)
    suspend fun updateAdjustment(update: AdjustmentUpdateEntity): Int
}

@Entity
internal data class AdjustmentUpdateEntity(
    val id: Long,
    @ColumnInfo(name = "adjustment_value", defaultValue = "0") val value: Float,
    @ColumnInfo(name = "adjustment_type", defaultValue = "0") val type: AdjustmentEntity.Type
)