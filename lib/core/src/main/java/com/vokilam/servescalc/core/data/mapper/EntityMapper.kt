package com.vokilam.servescalc.core.data.mapper

import com.vokilam.servescalc.core.data.db.entity.AdjustmentEntity
import com.vokilam.servescalc.core.data.db.entity.AmountEntity
import com.vokilam.servescalc.core.data.db.entity.IdEntity
import com.vokilam.servescalc.core.data.db.entity.IngredientEntity
import com.vokilam.servescalc.core.data.db.entity.RecipeEntity
import com.vokilam.servescalc.core.data.db.entity.RecipeWithIngredientsEntity
import com.vokilam.servescalc.core.data.db.entity.ServingEntity
import com.vokilam.servescalc.domain.model.Adjustment
import com.vokilam.servescalc.domain.model.AdjustmentType
import com.vokilam.servescalc.domain.model.Amount
import com.vokilam.servescalc.domain.model.FullRecipe
import com.vokilam.servescalc.domain.model.Ingredient
import com.vokilam.servescalc.domain.model.Recipe
import com.vokilam.servescalc.domain.model.RecipeId
import com.vokilam.servescalc.domain.model.Serving
import com.vokilam.servescalc.core.data.db.entity.AmountEntity.Type as AmountType
import com.vokilam.servescalc.core.data.db.entity.ServingEntity.Type as ServingType

internal fun RecipeEntity.toRecipe(): Recipe {
    return Recipe(RecipeId(id), title)
}

internal fun RecipeWithIngredientsEntity.toFullRecipe(): FullRecipe {
    return FullRecipe(
        recipe.toRecipe(),
        recipe.serving.toServing(),
        recipe.adjustment.toAdjustment(),
        ingredients.map { it.toIngredient() }
    )
}

internal fun FullRecipe.toRecipeEntity(): RecipeEntity {
    return RecipeEntity(
        recipe.id.value,
        recipe.title,
        serving.toServingEntity(),
        adjustment.toAdjustmentEntity()
    )
}

internal fun IngredientEntity.toIngredient(): Ingredient {
    return Ingredient(name, amount.toAmount())
}

internal fun Ingredient.toIngredientEntity(recipeId: Long): IngredientEntity {
    return IngredientEntity(0, recipeId, name, amount.toAmountEntity())
}

internal fun AmountEntity.toAmount(): Amount {
    return when (type) {
        AmountType.MEASURABLE -> Amount.Measurable(value, unit)
        AmountType.COUNTABLE -> Amount.Countable(value, unit)
        AmountType.UNMEASURABLE -> Amount.Unmeasurable(unit)
        AmountType.EMPTY -> Amount.Empty
    }
}

internal fun Amount.toAmountEntity(): AmountEntity {
    return when (this) {
        is Amount.Measurable -> AmountEntity(value, unit, AmountType.MEASURABLE)
        is Amount.Countable -> AmountEntity(value, unit, AmountType.COUNTABLE)
        is Amount.Unmeasurable -> AmountEntity(0f, unit, AmountType.UNMEASURABLE)
        is Amount.Empty -> AmountEntity(0f, "", AmountType.EMPTY)
    }
}

internal fun ServingEntity.toServing(): Serving {
    return when (type) {
        ServingType.NONE -> Serving.NONE
        ServingType.CUSTOM -> Serving.Custom(value, unit)
        ServingType.RING_DIAMETER -> Serving.RingDiameter(value)
    }
}

internal fun Serving.toServingEntity(): ServingEntity {
    return when (this) {
        is Serving.Custom -> ServingEntity(value, unit, ServingType.CUSTOM)
        is Serving.RingDiameter -> ServingEntity(value, unit, ServingType.RING_DIAMETER)
    }
}

internal fun Adjustment.toAdjustmentEntity(): AdjustmentEntity {
    return AdjustmentEntity(newValue, type.toAdjustmentEntityType())
}

internal fun AdjustmentEntity.toAdjustment(): Adjustment {
    return Adjustment(value, type.toAdjustmentType())
}

internal fun AdjustmentType.toAdjustmentEntityType(): AdjustmentEntity.Type {
    return when (this) {
        AdjustmentType.BY_SERVING -> AdjustmentEntity.Type.BY_SERVING
        AdjustmentType.BY_AMOUNT -> AdjustmentEntity.Type.BY_AMOUNT
    }
}

internal fun AdjustmentEntity.Type.toAdjustmentType(): AdjustmentType {
    return when (this) {
        AdjustmentEntity.Type.BY_SERVING -> AdjustmentType.BY_SERVING
        AdjustmentEntity.Type.BY_AMOUNT -> AdjustmentType.BY_AMOUNT
    }
}

internal fun RecipeId.toIdEntity(): IdEntity {
    return IdEntity(value)
}
