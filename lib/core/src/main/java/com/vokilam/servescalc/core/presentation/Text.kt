package com.vokilam.servescalc.core.presentation

import android.content.Context
import android.os.Parcelable
import android.text.Html
import androidx.annotation.StringRes
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue

sealed class Text : Parcelable {
    abstract fun asString(): String

    @Parcelize
    data class Plain(val text: String) : Text() {
        override fun asString(): String = text
    }

    @Parcelize
    class Resource(@StringRes val resId: Int, vararg val formatArgs: @RawValue Any) : Text() {
        override fun asString(): String {
            return "R.string.$resId(${formatArgs.joinToString()})"
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Resource

            if (resId != other.resId) return false
            if (!formatArgs.contentEquals(other.formatArgs)) return false

            return true
        }

        override fun hashCode(): Int {
            var result = resId
            result = 31 * result + formatArgs.contentHashCode()
            return result
        }
    }

    @Parcelize
    class Composite(vararg val texts: Text, val separator: String = "") : Text() {
        override fun asString(): String = texts.joinToString(separator)
    }

    @Parcelize
    object Empty : Text() {
        override fun asString(): String = ""
    }

    fun getString(context: Context): String {
        return when (this) {
            is Plain -> text
            is Resource -> context.getString(resId, *formatArgs)
            is Composite -> texts.joinToString(separator) { it.getString(context) }
            is Empty -> ""
        }
    }

    fun getText(context: Context): CharSequence {
        return when (this) {
            is Plain -> text
            is Resource -> {
                if (formatArgs.isEmpty()) context.getText(resId)
                else Html.fromHtml(String.format(getString(context), *formatArgs))
            }
            is Composite -> texts.joinToString(separator) { it.getText(context) }
            is Empty -> ""
        }
    }

    fun isEmpty() = this is Empty
}