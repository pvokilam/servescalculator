package com.vokilam.servescalc.core.di

import android.content.Context
import androidx.room.Room
import com.vokilam.servescalc.core.data.db.AppDatabase
import com.vokilam.servescalc.core.data.db.IngredientDao
import com.vokilam.servescalc.core.data.db.RecipeDao
import com.vokilam.servescalc.core.data.db.RoomMigrations
import com.vokilam.servescalc.core.data.repository.RecipesRepositoryImpl
import com.vokilam.servescalc.domain.repository.RecipesRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
internal abstract class DatabaseModule {

    @Binds
    abstract fun bindRecipesRepository(impl: RecipesRepositoryImpl): RecipesRepository

    companion object {
        @Singleton
        @Provides
        fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, "recipes.db")
                .addMigrations(RoomMigrations.M_1_2)
                .addMigrations(RoomMigrations.M_2_3)
                .build()
        }

        @Provides
        fun provideRecipeDao(db: AppDatabase): RecipeDao {
            return db.recipeDao()
        }

        @Provides
        fun provideIngredientDao(db: AppDatabase): IngredientDao {
            return db.ingredientDao()
        }
    }
}