package com.vokilam.servescalc.core.presentation.util

import androidx.lifecycle.SavedStateHandle
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

object SavedStateDelegates {

    fun <T> state(key: String): ReadWriteProperty<HasSavedStateHandle, T?> =
        object : ReadWriteProperty<HasSavedStateHandle, T?> {

            override fun getValue(thisRef: HasSavedStateHandle, property: KProperty<*>): T? {
                return thisRef.state[key]
            }

            override fun setValue(thisRef: HasSavedStateHandle, property: KProperty<*>, value: T?) {
                thisRef.state[key] = value
            }
        }

    fun <T> state(key: String, defaultValue: T): ReadWriteProperty<HasSavedStateHandle, T> =
        object : ReadWriteProperty<HasSavedStateHandle, T> {

            override fun getValue(thisRef: HasSavedStateHandle, property: KProperty<*>): T {
                if (!thisRef.state.contains(key)) {
                    setValue(thisRef, property, defaultValue)
                }
                return thisRef.state[key]!!
            }

            override fun setValue(thisRef: HasSavedStateHandle, property: KProperty<*>, value: T) {
                thisRef.state[key] = value
            }
        }

    interface HasSavedStateHandle {
        val state: SavedStateHandle
    }
}