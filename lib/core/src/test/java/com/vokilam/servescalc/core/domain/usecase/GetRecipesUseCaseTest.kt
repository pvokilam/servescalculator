package com.vokilam.servescalc.core.domain.usecase

import com.vokilam.core.test.TestData
import com.vokilam.servescalc.domain.model.RecipeListSortOrder
import com.vokilam.servescalc.domain.repository.RecipesRepository
import com.vokilam.servescalc.domain.settings.AppSettings
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.coVerifyOrder
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import kotlin.random.Random

@ExperimentalCoroutinesApi
class GetRecipesUseCaseTest {

    lateinit var useCase: GetRecipesUseCase
    private val repository = mockk<RecipesRepository>()
    private val appSettings = mockk<AppSettings>()

    @Before
    fun setUp() {
        useCase = GetRecipesUseCase(repository, appSettings)
    }

    @Test
    fun `should return items from repository`() = runBlockingTest {
        val expected = TestData.recipes
        val order = RecipeListSortOrder.RECENT

        every { appSettings.recipeListSortOrder } returns order
        coEvery { repository.getRecipes(order) } returns flowOf(expected)

        val actual = useCase()
        assertEquals(expected, actual.single())

        coVerifyOrder {
            appSettings.recipeListSortOrder
            repository.getRecipes(order)
        }
    }
}
