package com.vokilam.servescalc.core.domain.usecase

import com.vokilam.core.test.TestData
import com.vokilam.servescalc.domain.repository.RecipesRepository
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class SaveRecipeUseCaseTest {
    lateinit var useCase: SaveRecipeUseCase
    private val repository = mockk<RecipesRepository>()

    @Before
    fun setUp() {
        useCase = SaveRecipeUseCase(repository)
    }

    @Test
    fun `should save entity to repository`() = runBlockingTest {
        val expected = TestData.fullRecipe
        coEvery { repository.saveRecipe(expected) } returns expected.recipe.id

        val actual = useCase(expected)
        coVerify { repository.saveRecipe(expected) }
        assertEquals(expected.recipe.id, actual)
    }
}
