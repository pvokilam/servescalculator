package com.vokilam.servescalc.core.presentation.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import com.vokilam.core.test.util.getOrAwaitValue
import com.vokilam.core.test.util.observeForTesting
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

class ImportRecipeDataViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    var viewModel = ImportRecipeDataViewModel(SavedStateHandle())

    @Test
    fun `handleImportRecipeData should post data and navigate to add recipe screen`() {
        observeForTesting(viewModel.importRecipeData, viewModel.navigateToAddRecipe) {
            val expected = "ingredients"
            viewModel.handleImportRecipeData(expected)

            assertEquals(expected, viewModel.importRecipeData.value)
            assertEquals(expected, viewModel.navigateToAddRecipe.value)
        }
    }
}