package com.vokilam.servescalc.core.domain.usecase

import com.vokilam.core.test.TestData
import com.vokilam.servescalc.domain.repository.RecipesRepository
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class GetFullRecipeUseCaseTest {
    lateinit var useCase: GetFullRecipeUseCase
    private val repository = mockk<RecipesRepository>()

    @Before
    fun setUp() {
        useCase = GetFullRecipeUseCase(repository)
    }

    @Test
    fun `should return entity from repository`() = runBlockingTest {
        val expected = TestData.fullRecipe
        val id = expected.recipe.id
        coEvery { repository.getFullRecipe(id) } returns expected

        assertEquals(expected, useCase(id))
    }
}
