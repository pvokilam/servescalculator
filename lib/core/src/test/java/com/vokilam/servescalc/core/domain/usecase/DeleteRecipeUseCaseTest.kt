package com.vokilam.servescalc.core.domain.usecase

import com.vokilam.core.test.TestData
import com.vokilam.servescalc.domain.repository.RecipesRepository
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class DeleteRecipeUseCaseTest {
    lateinit var useCase: DeleteRecipeUseCase
    private val repository = mockk<RecipesRepository>()

    @Before
    fun setUp() {
        useCase = DeleteRecipeUseCase(repository)
    }

    @Test
    fun `should delete entity from repository`() = runBlockingTest {
        val id = TestData.singleRecipe.id
        coEvery { repository.deleteRecipe(id) } returns true

        val actual = useCase(id)

        coVerify { repository.deleteRecipe(id) }
        assertEquals(true, actual)
    }
}