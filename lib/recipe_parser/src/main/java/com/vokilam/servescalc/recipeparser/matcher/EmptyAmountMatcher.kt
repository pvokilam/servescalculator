package com.vokilam.servescalc.recipeparser.matcher

import com.vokilam.servescalc.domain.model.Amount
import com.vokilam.servescalc.domain.model.Ingredient
import com.vokilam.servescalc.domain.model.ParseResult
import com.vokilam.servescalc.domain.model.ParseResult.NoMatch
import com.vokilam.servescalc.recipeparser.Matcher
import com.vokilam.servescalc.recipeparser.dsl.RegexContext
import com.vokilam.servescalc.recipeparser.dsl.regex

internal class EmptyAmountMatcher : Matcher {

    override fun matchEntire(input: String): ParseResult {
        var result: ParseResult = NoMatch

        for (regex in REGEXES) {
            result = matchInternal(regex, input)

            if (result == NoMatch) {
                return result
            }
        }

        return result
    }

    private fun matchInternal(regex: Regex, input: String): ParseResult {
        return regex.matchEntire(input)?.let { res ->
            val name = res.groupValues[0]

            if (name.isEmpty()) return NoMatch

            Ingredient(name, Amount.Empty).let {
                ParseResult.Ingredient(it, IntRange.EMPTY)
            }
        } ?: NoMatch
    }

    companion object {
        private fun RegexContext.shouldNotStartOrEndWith(block: RegexContext.() -> Unit) {
            // (?:(?!^<pattern>).(?!<pattern>$))+
            group("?:", "+") {
                group("?!") {
                    +"^"
                    +pattern(block)
                }
                +"."
                group("?!") {
                    +pattern(block)
                    +"$"
                }
            }
        }

        private val REGEXES by lazy {
            listOf(
                regex(RegexOption.IGNORE_CASE) {
                    shouldNotStartOrEndWith {
                        measurableAmount()
                    }
                },
                regex(RegexOption.IGNORE_CASE) {
                    shouldNotStartOrEndWith {
                        countableAmount()
                    }
                },
                regex(RegexOption.IGNORE_CASE) {
                    shouldNotStartOrEndWith {
                        unmeasurableAmount()
                    }
                }
            )
        }
    }
}