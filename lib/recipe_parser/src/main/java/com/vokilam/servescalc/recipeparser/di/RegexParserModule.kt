package com.vokilam.servescalc.recipeparser.di

import com.vokilam.servescalc.domain.parser.IngredientsParser
import com.vokilam.servescalc.recipeparser.MatcherChain
import com.vokilam.servescalc.recipeparser.RegexIngredientsParser
import com.vokilam.servescalc.recipeparser.matcher.BlankMatcher
import com.vokilam.servescalc.recipeparser.matcher.CountableAmountMatcher
import com.vokilam.servescalc.recipeparser.matcher.CountableIngredientMatcher
import com.vokilam.servescalc.recipeparser.matcher.EmptyAmountMatcher
import com.vokilam.servescalc.recipeparser.matcher.MeasurableAmountMatcher
import com.vokilam.servescalc.recipeparser.matcher.MeasurableIngredientMatcher
import com.vokilam.servescalc.recipeparser.matcher.UnmeasurableAmountMatcher
import com.vokilam.servescalc.recipeparser.matcher.UnmeasurableIngredientMatcher
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
internal abstract class RegexParserModule {

    @Binds
    abstract fun bindIngredientsParser(impl: RegexIngredientsParser): IngredientsParser

    companion object {
        @Provides
        fun provideMatcherChain(): MatcherChain {
            return MatcherChain(listOf(
                BlankMatcher(),
                MeasurableAmountMatcher(),
                CountableAmountMatcher(),
                UnmeasurableAmountMatcher(),
                MeasurableIngredientMatcher(),
                CountableIngredientMatcher(),
                UnmeasurableIngredientMatcher(),
                EmptyAmountMatcher()
            ))
        }
    }
}