package com.vokilam.servescalc.recipeparser.matcher

import com.vokilam.servescalc.domain.model.ParseResult
import com.vokilam.servescalc.recipeparser.Matcher

internal class BlankMatcher : Matcher {
    override fun matchEntire(input: String): ParseResult {
        return if (input.isBlank()) ParseResult.Blank else ParseResult.NoMatch
    }
}