package com.vokilam.servescalc.recipeparser

import com.vokilam.servescalc.domain.model.ParseResult

internal interface Matcher {
    fun matchEntire(input: String): ParseResult
}