package com.vokilam.servescalc.recipeparser.matcher

import com.vokilam.servescalc.domain.model.Amount
import com.vokilam.servescalc.domain.model.ParseResult
import com.vokilam.servescalc.domain.model.ParseResult.NoMatch
import com.vokilam.servescalc.recipeparser.Matcher
import com.vokilam.servescalc.recipeparser.dsl.regex
import com.vokilam.servescalc.recipeparser.matcher.Regexes.parseValueOrNull

internal class MeasurableAmountMatcher : Matcher {
    override fun matchEntire(input: String): ParseResult {
        return REGEX.matchEntire(input)?.let {
            val (_, value, unit) = it.destructured
            val floatValue = parseValueOrNull(value) ?: return NoMatch

            Amount.Measurable(floatValue, unit).let(ParseResult::Amount)
        } ?: NoMatch
    }

    companion object {
        private val REGEX by lazy {
            regex(RegexOption.IGNORE_CASE) {
                trimLineStart()
                group { measurableAmount() }
                trimLineEnd()
            }
        }
    }
}