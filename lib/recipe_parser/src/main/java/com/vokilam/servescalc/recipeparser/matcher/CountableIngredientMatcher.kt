package com.vokilam.servescalc.recipeparser.matcher

import com.vokilam.servescalc.domain.model.Amount
import com.vokilam.servescalc.domain.model.Ingredient
import com.vokilam.servescalc.domain.model.ParseResult
import com.vokilam.servescalc.domain.model.ParseResult.NoMatch
import com.vokilam.servescalc.recipeparser.Matcher
import com.vokilam.servescalc.recipeparser.dsl.regex

internal class CountableIngredientMatcher : Matcher {
    // TODO: 11/24/19 join with MeasurableIngredientMatcher?
    override fun matchEntire(input: String): ParseResult {
        for (config in REGEXES) {
            val result = matchInternal(config, input)

            if (result != NoMatch) {
                return result
            }
        }

        return NoMatch
    }

    private fun matchInternal(config: Config, input: String): ParseResult {
        return config.regex.matchEntire(input)?.let { res ->
            val name = res.groupValues[config.groupName]
            val value = res.groupValues[config.groupValue]
            val unit = res.groupValues[config.groupUnit]
            val amountRange = res.groups[config.groupAmount]?.range ?: IntRange.EMPTY

            if (name.isEmpty()) return NoMatch
            val floatValue = Regexes.parseValueOrNull(value) ?: return NoMatch

            Ingredient(name, Amount.Countable(floatValue, unit)).let {
                ParseResult.Ingredient(it, amountRange)
            }
        } ?: NoMatch
    }

    private data class Config(
        val regex: Regex,
        val groupName: Int,
        val groupValue: Int,
        val groupUnit: Int,
        val groupAmount: Int
    )

    companion object {
        private val REGEX_VALUE_END by lazy {
            Config(
                regex(RegexOption.IGNORE_CASE) {
                    trimLineStart()
                    name()
                    nameAmountSeparator()
                    group { countableAmount() }
                    trimLineEnd()
                },
                1,
                3,
                4,
                2
            )
        }

        private val REGEX_VALUE_START by lazy {
            Config(
                regex(RegexOption.IGNORE_CASE) {
                    trimLineStart()
                    group { countableAmount() }
                    amountNameSeparator()
                    name()
                    trimLineEnd()
                },
                4,
                2,
                3,
                1
            )
        }

        private val REGEXES by lazy {
            listOf(REGEX_VALUE_START, REGEX_VALUE_END)
        }
    }
}