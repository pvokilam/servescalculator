package com.vokilam.servescalc.recipeparser

import com.vokilam.servescalc.domain.CoroutinesDispatcherProvider
import com.vokilam.servescalc.domain.model.ParseResult
import com.vokilam.servescalc.domain.parser.IngredientsParser
import kotlinx.coroutines.withContext
import javax.inject.Inject

internal class RegexIngredientsParser @Inject constructor(
    private val chain: MatcherChain,
    private val dispatchers: CoroutinesDispatcherProvider
) : IngredientsParser {
    override suspend fun parse(input: String): ParseResult = withContext(dispatchers.computation) {
        chain.matchEntire(input)
    }

    override suspend fun parse(ingredients: List<String>): List<ParseResult> = withContext(dispatchers.computation) {
        ingredients.map { parse(it) }
    }
}