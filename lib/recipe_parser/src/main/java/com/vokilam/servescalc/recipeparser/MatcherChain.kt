package com.vokilam.servescalc.recipeparser

import com.vokilam.servescalc.domain.model.ParseResult

internal class MatcherChain(
    private val matchers: List<Matcher>
) : Matcher {

    override fun matchEntire(input: String): ParseResult {
        return matchEntire(input, matchers.iterator())
    }

    private fun matchEntire(input: String, iterator: Iterator<Matcher>): ParseResult {
        if (!iterator.hasNext()) return ParseResult.NoMatch

        return iterator.next().matchEntire(input).let {
            if (it == ParseResult.NoMatch) matchEntire(input, iterator)
            else it
        }
    }
}
