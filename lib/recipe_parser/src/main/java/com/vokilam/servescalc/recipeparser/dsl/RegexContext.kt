package com.vokilam.servescalc.recipeparser.dsl

@DslMarker
internal annotation class RegexMarker

@RegexMarker
internal class RegexContext {
    private val patterns = mutableListOf<String>()

    private fun append(pattern: String) {
        patterns.add(pattern)
    }

    fun pattern(block: RegexContext.() -> Unit): String {
        return RegexContext().apply(block).toString()
    }

    operator fun String.unaryPlus() {
        append(this)
    }

    fun group(modifier: String = "", quantifier: String = "", block: RegexContext.() -> Unit) {
        listOf(pattern(block)).joinToString("", "($modifier", ")$quantifier").also(::append)
    }

    fun anyOf(vararg patterns: String) {
        group("?:") {
            +patterns.joinToString("|")
        }
    }

    fun optional(block: RegexContext.() -> Unit) {
        group("?:", "?") {
            +pattern(block)
        }
    }

    fun zeroOrMore(block: RegexContext.() -> Unit) {
        group("?:", "*") {
            +pattern(block)
        }
    }

    fun oneOrMore(block: RegexContext.() -> Unit) {
        group("?:", "+") {
            +pattern(block)
        }
    }

    override fun toString(): String {
        return patterns.joinToString("")
    }
}

internal fun regex(vararg options: RegexOption, block: RegexContext.() -> Unit): Regex {
    return RegexContext().apply(block).toString().toRegex(options.toSet())
}