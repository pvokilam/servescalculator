package com.vokilam.servescalc.recipeparser.di

import dagger.Module

@Module(includes = [RegexParserModule::class])
abstract class RecipeParserModule