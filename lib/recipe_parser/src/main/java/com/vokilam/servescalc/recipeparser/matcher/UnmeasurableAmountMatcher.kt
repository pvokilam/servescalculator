package com.vokilam.servescalc.recipeparser.matcher

import com.vokilam.servescalc.domain.model.Amount
import com.vokilam.servescalc.domain.model.ParseResult
import com.vokilam.servescalc.recipeparser.Matcher
import com.vokilam.servescalc.recipeparser.dsl.regex

internal class UnmeasurableAmountMatcher : Matcher {

    override fun matchEntire(input: String): ParseResult {
        return REGEX.matchEntire(input)?.let {
            val (unit) = it.destructured
            Amount.Unmeasurable(unit).let(ParseResult::Amount)
        } ?: ParseResult.NoMatch
    }

    companion object {
        private val REGEX by lazy {
            regex(RegexOption.IGNORE_CASE) {
                trimLineStart()
                unmeasurableAmount()
                trimLineEnd()
            }
        }
    }
}