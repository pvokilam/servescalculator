package com.vokilam.servescalc.recipeparser.matcher

import com.vokilam.servescalc.domain.model.Amount
import com.vokilam.servescalc.domain.model.Ingredient
import com.vokilam.servescalc.domain.model.ParseResult
import com.vokilam.servescalc.domain.model.ParseResult.NoMatch
import com.vokilam.servescalc.recipeparser.Matcher
import com.vokilam.servescalc.recipeparser.dsl.regex

internal class UnmeasurableIngredientMatcher : Matcher {

    override fun matchEntire(input: String): ParseResult {
        return REGEX.matchEntire(input)?.let { res ->
            val (name, _, unit) = res.destructured
            val amountRange = res.groups[GROUP_AMOUNT]?.range ?: IntRange.EMPTY

            if (name.isEmpty()) return NoMatch

            Ingredient(name, Amount.Unmeasurable(unit)).let {
                ParseResult.Ingredient(it, amountRange)
            }
        } ?: NoMatch
    }

    companion object {
        private val REGEX by lazy {
            regex(RegexOption.IGNORE_CASE) {
                trimLineStart()
                name()
                nameAmountSeparator()
                group { unmeasurableAmount() }
                trimLineEnd()
            }
        }

        private const val GROUP_AMOUNT = 2
    }
}