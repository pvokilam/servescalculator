@file:Suppress("TooManyFunctions")

package com.vokilam.servescalc.recipeparser.matcher

import com.vokilam.servescalc.recipeparser.dsl.RegexContext
import java.text.DecimalFormat
import java.text.ParseException
import java.text.ParsePosition
import java.util.*

internal object Regexes {

    val FRACTIONS by lazy {
        FRACTIONS_MAP.keys.joinToString("")
    }

    private val FRACTIONS_MAP = mapOf(
        "¼" to 1/4f,
        "½" to 1/2f,
        "¾" to 3/4f,
        "⅓" to 1/3f,
        "⅜" to 3/8f,
        "⅔" to 2/3f,
        "⅙" to 1/6f,
        "⅚" to 5/6f,
        "⅘" to 4/5f,
        "⅕" to 1/5f,
        "⅐" to 1/7f,
        "⅑" to 1/9f,
        "⅒" to 1/10f,
        "⅖" to 2/5f,
        "⅗" to 3/5f,
        "⅛" to 1/8f,
        "⅝" to 5/8f,
        "⅞" to 7/8f
    )

    private val FRACTION_NUMBER_REGEX = """(?:(\d+)\s)?(\d+)/(\d+)""".toRegex()

    private fun matchFractionNumber(value: String): Float? {
        val floatValue = FRACTIONS_MAP[value]
        if (floatValue != null) return floatValue

        return FRACTION_NUMBER_REGEX.matchEntire(value)?.let {
            val (whole, numerator, denominator) = it.destructured

            try {
                if (whole.isBlank()) {
                    numerator.toFloat() / denominator.toFloat()
                } else if (numerator.isBlank() && denominator.isBlank()) {
                    whole.toFloat()
                } else {
                    whole.toFloat() + numerator.toFloat() / denominator.toFloat()
                }
            } catch (e: NumberFormatException) {
                null
            }
        }
    }

    private fun matchCommaDecimalSeparatorValue(value: String): Float? {
        val format = DecimalFormat.getNumberInstance(Locale.forLanguageTag("ru"))

        return try {
            val pos = ParsePosition(0)
            val res = format.parse(value, pos)

            if (pos.index != value.length || pos.errorIndex != -1) null
            else res.toFloat()
        } catch (e: ParseException) {
            null
        }
    }

    fun parseValueOrNull(value: String): Float? {
        if (value.isBlank()) {
            return null
        }

        return value.toFloatOrNull()
            ?: matchCommaDecimalSeparatorValue(value)
            ?: matchFractionNumber(value)
    }
}

internal fun RegexContext.value() = group { +"""[\d.,/ ${Regexes.FRACTIONS}]+?""" }

internal fun RegexContext.measurableUnit() {
    group {
        anyOf(
            "g", "grams?", "kg", "kilograms?",
            "oz", "ounces?", "lbs?", "pounds?",
            "tsp\\.?", "tsps?", "teaspoons?", "tbsp\\.?", "tbsps?", "tablespoons?",
            "cups?", "ml", "milliliters?", "l", "litres?",
            "pints?", "qt", "quarts?",
            "гр?\\.?", "грамм?", "грамм?ов",
            "кг\\.?", "килограмм?", "килограмм?ов",
            "ст\\.? ?л\\.?", "столов.. лож..", "ст\\. ?лож..",
            "ч\\.? ?л\\.?", "чайн.. лож..", "ч\\. ?лож..",
            "мл\\.?", "л\\.?", "литр.?.?",
            "ст\\.?", "стакан.?.?"
        )
    }
}

internal fun RegexContext.unmeasurableUnit() {
    group {
        anyOf(
            "to taste",
            "q.b.",
            "по вкусу",
            "на кончике ножа"
        )
    }
}

internal fun RegexContext.countableUnit() {
    group {
        anyOf(
            "pcs", "pieces?", "quantity", "quantities",
            "pinch", "pinches",
            "slices?", "bunch", "bunches", "stalks?", "sticks?",
            "jars?", "cans?", "bottles?",
            "cloves?",
            "шт\\.?", "штук.?",
            "зубчик.?.?", "голов.?.?",
            "щепот..",
            "пучок", "пучк.?.?", "стебель", "стебл.?.?", "веточка", "веточ.?.?",
            "банк.", "банок", "пачк.?", "пачек",
            "кусок", "куск.?.?",
            "горст.?.?",
            "палоч.?.?",
            "струч.?.?"
        )
    }
}
internal fun RegexContext.name() = group { +""".*?""" }

internal fun RegexContext.valueUnitSeparator() = +"""\s?"""

internal fun RegexContext.nameAmountSeparator() = +"""[\s-,]*"""

internal fun RegexContext.amountNameSeparator() = +"""[\s]+"""

internal fun RegexContext.trimLineStart() = +"""[\s-]*"""

internal fun RegexContext.trimLineEnd() = +"""[\s,.;]*"""

internal fun RegexContext.measurableAmount() {
    value()
    valueUnitSeparator()
    measurableUnit()
}

internal fun RegexContext.unmeasurableAmount() {
    unmeasurableUnit()
}

internal fun RegexContext.countableAmount() {
    value()
    optional {
        valueUnitSeparator()
        countableUnit()
    }
}
