package com.vokilam.servescalc.recipeparser

import com.vokilam.servescalc.domain.CoroutinesDispatcherProvider
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import javax.inject.Provider

@ExperimentalCoroutinesApi
class RegexIngredientsParserSpek : Spek({

    describe("RegexIngredientsParser") {
        val chain = mockk<MatcherChain>(relaxed = true)
        val parser by memoized {
            RegexIngredientsParser(chain, CoroutinesDispatcherProvider(TestCoroutineDispatcher()))
        }

        afterEachTest {
            clearAllMocks()
        }

        context("parse(String)") {
            beforeEachTest {
                runBlockingTest { parser.parse("input") }
            }

            it("should call matcher once") {
                verify(exactly = 1) { chain.matchEntire("input") }
            }
        }

        context("parse(List)") {
            val lines = (1..5).map(Int::toString)
            beforeEachTest {
                runBlockingTest { parser.parse(lines) }
            }

            val args = mutableListOf<String>()
            it("should call chain size times") {
                verify(exactly = 5) { chain.matchEntire(capture(args)) }
                Assert.assertEquals(args, lines)
            }
        }
    }
})