package com.vokilam.servescalc.recipeparser.matcher

import com.vokilam.servescalc.domain.model.Amount
import com.vokilam.servescalc.domain.model.ParseResult
import org.junit.Assert.assertEquals
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class CountableAmountMatcherSpek : Spek({

    describe("CountableAmountMatcher") {
        val matcher by memoized { CountableAmountMatcher() }

        context("should match") {
            mapOf(
                "2 pinches" to result(2f,   "pinches"),
                "4"         to result(4f,   ""),
                "1/2 can"   to result(0.5f, "can")
            ).forEach {
                it("'${it.key}'") {
                    assertEquals(it.value, matcher.matchEntire(it.key))
                }
            }
        }

        context("should not match") {
            listOf(
                "salt 2 pinches",
                "4 eggs",
                "pineapple 1/2 can",
                "2 pinches salt",
                "eggs 4",
                "1/2 can pineapple"
            ).forEach {
                it("'$it'") {
                    assertEquals(ParseResult.NoMatch, matcher.matchEntire(it))
                }
            }
        }
    }
})

private fun result(value: Float, unit: String): ParseResult.Amount {
    return ParseResult.Amount(Amount.Countable(value, unit))
}