package com.vokilam.servescalc.recipeparser.matcher

import com.vokilam.servescalc.domain.model.Amount
import com.vokilam.servescalc.domain.model.ParseResult
import com.vokilam.servescalc.domain.model.g
import org.junit.Assert.assertEquals
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class MeasurableAmountMatcherSpek : Spek({

    describe("MeasurableAmountMatcher") {
        val matcher by memoized { MeasurableAmountMatcher() }

        context("should match") {
            mapOf(
                "500g"      to result(500.g),
                "500 g"     to result(500.g),
                "1/2 cup"   to result(0.5f, "cup")
            ).forEach {
                it("'${it.key}'") {
                    assertEquals(it.value, matcher.matchEntire(it.key))
                }
            }
        }

        context("should not match") {
            listOf(
                "flour 500g",
                "flour 1/2 cup",
                "500g flour",
                "1/2 cup flour"
            ).forEach {
                it("'$it'") {
                    assertEquals(ParseResult.NoMatch, matcher.matchEntire(it))
                }
            }
        }
    }

})

private fun result(amount: Amount): ParseResult.Amount {
    return ParseResult.Amount(amount)
}

private fun result(value: Float, unit: String): ParseResult.Amount {
    return ParseResult.Amount(Amount.Measurable(value, unit))
}