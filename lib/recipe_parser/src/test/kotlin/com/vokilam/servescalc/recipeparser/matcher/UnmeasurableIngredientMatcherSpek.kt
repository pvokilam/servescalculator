package com.vokilam.servescalc.recipeparser.matcher

import com.vokilam.servescalc.domain.model.Amount.Unmeasurable
import com.vokilam.servescalc.domain.model.Ingredient
import com.vokilam.servescalc.domain.model.ParseResult
import com.vokilam.servescalc.domain.model.ParseResult.NoMatch
import org.junit.Assert.assertEquals
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class UnmeasurableIngredientMatcherSpek: Spek({

    describe("UnmeasurableIngredientMatcher") {
        val matcher by memoized { UnmeasurableIngredientMatcher() }

        context("should match") {
            mapOf(
                // basic
                ("salt to taste"        to result("salt", "to taste", 5..12)),
                // name variations
                ("salt, to taste"       to result("salt", "to taste", 6..13)),
                // spacing variatiaons
                ("salt   , to taste"    to result("salt", "to taste", 9..16)),
                ("salt    to taste"     to result("salt", "to taste", 8..15)),
                ("salt\t\tto taste"     to result("salt", "to taste", 6..13)),
                ("salt - to taste"      to result("salt", "to taste", 7..14)),
                (" salt to taste"       to result("salt", "to taste", 6..13)),
                ("salt to taste "       to result("salt", "to taste", 5..12)),
                ("saltto taste"        to result("salt", "to taste", 4..11)),
                // separators
                ("salt to taste."        to result("salt", "to taste", 5..12)),
                ("salt to taste,"        to result("salt", "to taste", 5..12)),
                ("salt to taste;"        to result("salt", "to taste", 5..12)),
                //  unicode
                ("соль по вкусу"        to result("соль", "по вкусу", 5..12)),
                // case insensitivity
                ("соль ПО ВКУСУ"        to result("соль", "ПО ВКУСУ", 5..12))
            ).forEach {
                it("'${it.key}'") {
                    assertEquals(it.value, matcher.matchEntire(it.key))
                }
            }
        }

        context("should match unit") {
            listOf(
                "to taste",
                "q.b.",
                "по вкусу",
                "на кончике ножа"
            ).map {
                ("ingredient $it" to result("ingredient", it, 11 until 11 + it.length))
            }.forEach {
                it("'${it.first}'") {
                    assertEquals(it.second, matcher.matchEntire(it.first))
                }
            }
        }

        context("should not match") {
            listOf(
                ("to taste"),
                ("   to taste"),
                ("to taste   ")
            ).forEach {
                it("'$it'") {
                    assertEquals(NoMatch, matcher.matchEntire(it))
                }
            }
        }
    }
})

private fun result(name: String, unit: String, amountRange: IntRange): ParseResult.Ingredient {
    return ParseResult.Ingredient(Ingredient(name, Unmeasurable(unit)), amountRange)
}