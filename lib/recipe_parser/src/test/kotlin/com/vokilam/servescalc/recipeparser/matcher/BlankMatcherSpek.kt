package com.vokilam.servescalc.recipeparser.matcher

import com.vokilam.servescalc.domain.model.ParseResult
import org.junit.Assert.assertEquals
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class BlankMatcherSpek : Spek({

    describe("BlankMatcher") {
        val matcher by memoized { BlankMatcher() }

        context("should match") {
            listOf(
                "",
                " ",
                "   ",
                "\t",
                " \t"
            ).forEach {
                it("'$it'") {
                    assertEquals(ParseResult.Blank, matcher.matchEntire(it))
                }
            }
        }

        context("should not match") {
            listOf(
                " a",
                "\t    s"
            ).forEach {
                it("'$it'") {
                    assertEquals(ParseResult.NoMatch, matcher.matchEntire(it))
                }
            }
        }
    }
})