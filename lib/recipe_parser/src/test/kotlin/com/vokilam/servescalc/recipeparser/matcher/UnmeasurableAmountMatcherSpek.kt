package com.vokilam.servescalc.recipeparser.matcher

import com.vokilam.servescalc.domain.model.Amount
import com.vokilam.servescalc.domain.model.ParseResult
import org.junit.Assert.assertEquals
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class UnmeasurableAmountMatcherSpek : Spek({

    describe("UnmeasurableAmountMatcher") {
        val matcher by memoized { UnmeasurableAmountMatcher() }

        context("should match") {
            mapOf(
                ("to taste" to result("to taste")),
                //  unicode
                ("по вкусу" to result("по вкусу")),
                // case insensitivity
                ("ПО ВКУСУ" to result("ПО ВКУСУ"))
            ).forEach {
                it("'${it.key}'") {
                    assertEquals(it.value, matcher.matchEntire(it.key))
                }
            }
        }

        context("should not match") {
            listOf(
                "salt to taste"
            ).forEach {
                it("'$it'") {
                    assertEquals("'$it' -> NoMatch", ParseResult.NoMatch, matcher.matchEntire(it))
                }
            }
        }
    }
})

private fun result(unit: String): ParseResult.Amount {
    return ParseResult.Amount(Amount.Unmeasurable(unit))
}