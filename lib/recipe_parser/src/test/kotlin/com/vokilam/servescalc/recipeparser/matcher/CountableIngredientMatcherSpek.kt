package com.vokilam.servescalc.recipeparser.matcher

import com.vokilam.servescalc.domain.model.Amount.Countable
import com.vokilam.servescalc.domain.model.Ingredient
import com.vokilam.servescalc.domain.model.ParseResult
import com.vokilam.servescalc.domain.model.ParseResult.NoMatch
import org.junit.Assert.assertEquals
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class CountableIngredientMatcherSpek : Spek({

    describe("CountableIngredientMatcher") {
        val matcher by memoized { CountableIngredientMatcher() }

        context("should match value end") {
            mapOf(
                ("eggs 1"               to result("eggs",       1f,     "",     5..5)),
                // value variations
                ("eggs 1.5"             to result("eggs",       1.5f,   "",     5..7)),
                ("eggs 0.5"             to result("eggs",       0.5f,   "",     5..7)),
                ("eggs .5"              to result("eggs",       .5f,    "",     5..6)),
                ("eggs 1,5"             to result("eggs",       1.5f,   "",     5..7)),
                // fraction value variations
                ("tomatoes 1 can"       to result("tomatoes",   1f,     "can",  9..13)),
                ("tomatoes 1 1/2 can"   to result("tomatoes",   1.5f,   "can",  9..17)),
                ("tomatoes ¾ can"       to result("tomatoes",   .75f,   "can",  9..13)),
                // spacing variations
                ("eggs   1"             to result("eggs",       1f,     "",     7..7)),
                ("eggs 1 "              to result("eggs",       1f,     "",     5..5)),
                (" eggs 1"              to result("eggs",       1f,     "",     6..6)),
                ("eggs1"                to result("eggs",       1f,     "",     4..4)),
                ("tomatoes1can"         to result("tomatoes",   1f,     "can",  8..11)),
                ("eggs\t\t1"            to result("eggs",       1f,     "",     6..6)),
                // separators
                ("tomatoes - 3 cans"    to result("tomatoes",   3f,     "cans", 11..16)),
                ("tomatoes-3 cans"      to result("tomatoes",   3f,     "cans", 9..14)),
                ("tomatoes-3 cans."     to result("tomatoes",   3f,     "cans", 9..14)),
                ("tomatoes-3 cans,"     to result("tomatoes",   3f,     "cans", 9..14)),
                ("tomatoes-3 cans;"     to result("tomatoes",   3f,     "cans", 9..14)),
                // unicode
                ("яйца 2 шт."           to result("яйца",       2f,     "шт.",  5..9)),
                // case insensitivity
                ("tomatoes 1 CAN"       to result("tomatoes",   1f,     "CAN",  9..13))
            ).forEach {
                it("'${it.key}'") {
                    assertEquals(it.value, matcher.matchEntire(it.key))
                }
            }
        }

        context("should match value start") {
            mapOf(
                ("1 egg"                to result("egg",        1f,     "",     0..0)),
                // value variations
                ("1.5 eggs"             to result("eggs",       1.5f,   "",     0..2)),
                ("0.5 eggs"             to result("eggs",       0.5f,   "",     0..2)),
                (".5 eggs"              to result("eggs",       .5f,    "",     0..1)),
                ("1,5 eggs"             to result("eggs",       1.5f,   "",     0..2)),
                // fraction value variations
                ("1 can tomatoes"       to result("tomatoes",   1f,     "can",  0..4)),
                // todo ("1 1/2 can tomatoes"   to result("tomatoes",   1.5f,   "can",  0..8)),
                ("¾ can tomatoes"       to result("tomatoes",   .75f,   "can",  0..4)),
                // spacing variations
                ("1   egg"              to result("egg",        1f,     "",     0..0)),
                ("1\t\tegg"             to result("egg",        1f,     "",     0..0)),
                // unicode
                ("2 яйца"               to result("яйца",        2f,    "",     0..0)),
                // case insensitivity
                ("1 CAN tomatoes"       to result("tomatoes",    1f,    "CAN",  0..4))
            ).forEach {
                it("'${it.key}'") {
                    assertEquals(it.value, matcher.matchEntire(it.key))
                }
            }
        }

        context("should match unit") {
            val units = listOf(
                "pcs", "piece", "pieces", "quantity", "quantities",
                "pinch", "pinches",
                "slice", "slices", "bunch", "bunches", "stalk", "stalks", "stick", "sticks",
                "jar", "jars", "can", "cans", "bottle", "bottles",
                "clove", "cloves",
                "шт", "шт.", "штука", "штуки", "штук",
                "зубчик", "зубчика", "зубчиков", "головка", "головки", "головок",
                "щепотка", "щепотки", "щепоток",
                "пучок", "пучка", "пучков", "стебель", "стебля", "стеблей", "веточка", "веточки", "веточек",
                "банка", "банки", "банок", "пачка", "пачки", "пачек",
                "кусок", "куска", "кусков",
                "горсть", "горсти", "горстей",
                "палочка", "палочки", "палочек",
                "стручок", "стручка"
            )

            units.map {
                "potatoes 1 $it" to result("potatoes", 1f, it, 9 until 11 + it.length)
            }.forEach {
                it("'${it.first}'") {
                    assertEquals(it.second, matcher.matchEntire(it.first))
                }
            }

            units.map {
                "1 $it potatoes" to result("potatoes", 1f, it, 0 until 2 + it.length)
            }.forEach {
                it("'${it.first}'") {
                    assertEquals(it.second, matcher.matchEntire(it.first))
                }
            }
        }

        context("should not match") {
            listOf(
                ("500"),
                ("   500"),
                ("tomatoes"),
                ("tomatoes can"),
                ("500   "),
                ("can tomatoes")
            ).forEach {
                it("'$it'") {
                    assertEquals(NoMatch, matcher.matchEntire(it))
                }
            }
        }
    }
})

private fun result(name: String, value: Float, unit: String, amountRange: IntRange): ParseResult.Ingredient {
    return ParseResult.Ingredient(Ingredient(name, Countable(value, unit)), amountRange)
}