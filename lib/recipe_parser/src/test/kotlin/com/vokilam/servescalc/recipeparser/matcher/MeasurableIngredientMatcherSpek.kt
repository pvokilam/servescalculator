package com.vokilam.servescalc.recipeparser.matcher

import com.vokilam.servescalc.domain.model.Amount
import com.vokilam.servescalc.domain.model.Amount.Measurable
import com.vokilam.servescalc.domain.model.Ingredient
import com.vokilam.servescalc.domain.model.ParseResult
import com.vokilam.servescalc.domain.model.ParseResult.NoMatch
import com.vokilam.servescalc.domain.model.g
import org.junit.Assert.assertEquals
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class MeasurableIngredientMatcherSpek : Spek({

    describe("MeasurableIngredientMatcher") {
        val matcher by memoized { MeasurableIngredientMatcher() }

        context("should match value end") {
            mapOf(
                // basic
                ("flour 500g"               to result("flour",              500.g, 6..9)),
                // ingredient name variations
                ("white flour 500g"         to result("white flour",        500.g, 12..15)),
                ("flour, bleached 500g"     to result("flour, bleached",    500.g, 16..19)),
                ("low-fat (1%) milk 500g"   to result("low-fat (1%) milk",  500.g, 18..21)),
                ("1% milk 500g"             to result("1% milk",            500.g, 8..11)),
                ("well-shaken low-fat (1 1/2%) buttermilk 500g" to result("well-shaken low-fat (1 1/2%) buttermilk", 500.g, 40..43)),
                ("semisweet chocolate (61-72% cacao), chopped 500g" to result("semisweet chocolate (61-72% cacao), chopped", 500.g, 44..47)),
                // value variations
                ("flour 500.0g"         to result("flour",  500.g,      6..11)),
                ("flour 500.5g"         to result("flour",  500.5.g,    6..11)),
                ("flour 0.5g"           to result("flour",  0.5.g,      6..9)),
                ("flour .5g"            to result("flour",  0.50.g,     6..8)),
                ("flour 1,5g"           to result("flour",  1.50.g,     6..9)),
                // fraction value variations
                ("flour 1 cup"          to result("flour", 1f,      "cup", 6..10)),
                ("flour 1 1/2 cup"      to result("flour", 1.5f,    "cup", 6..14)),
                ("flour 1 3/4 cup"      to result("flour", 1.75f,   "cup", 6..14)),
                ("flour 3/4 cup"        to result("flour", .75f,    "cup", 6..12)),
                ("flour ¾ cup"          to result("flour", .75f,    "cup", 6..10)),
                ("flour ½ cup"          to result("flour", .5f,     "cup", 6..10)),
                ("flour ¼ cup"          to result("flour", .25f,    "cup", 6..10)),
                ("flour ⅓ cup"          to result("flour", 1/3f,    "cup", 6..10)),
                // spacing variations
                ("flour500g"        to result("flour", 500.g, 5..8)),
                ("flour 500g "      to result("flour", 500.g, 6..9)),
                (" flour 500g"      to result("flour", 500.g, 7..10)),
                ("flour   500g"     to result("flour", 500.g, 8..11)),
                ("flour 500 g"      to result("flour", 500.g, 6..10)),
                ("flour\t\t500g"    to result("flour", 500.g, 7..10)),
                ("  flour 500g"     to result("flour", 500.g, 8..11)),
                // separators
                ("flour - 500g"     to result("flour", 500.g, 8..11)),
                ("flour-500g"       to result("flour", 500.g, 6..9)),
                ("flour 500g."      to result("flour", 500.g, 6..9)),
                ("flour 500g,"      to result("flour", 500.g, 6..9)),
                ("flour 500g;"      to result("flour", 500.g, 6..9)),
                // case insensitivity
                ("flour 500G"       to result("flour", 500f, "G", 6..9)),
                // unicode
                ("мука 500г"        to result("мука", 500f, "г", 5..8))
            ).forEach {
                it("'${it.key}'") {
                    assertEquals(it.value, matcher.matchEntire(it.key))
                }
            }
        }

        context("should match value start") {
            mapOf(
                // basic
                ("500g flour"               to result("flour",              500.g, 0..3)),
                // ingredient name variations
                ("500g white flour"         to result("white flour",        500.g, 0..3)),
                ("500g flour, bleached"     to result("flour, bleached",    500.g, 0..3)),
                ("500g low-fat (1%) milk"   to result("low-fat (1%) milk",  500.g, 0..3)),
                ("500g 1% milk"             to result("1% milk",            500.g, 0..3)),
                ("500g well-shaken low-fat (1 1/2%) buttermilk" to result("well-shaken low-fat (1 1/2%) buttermilk", 500.g, 0..3)),
                ("500g semisweet chocolate (61-72% cacao), chopped" to result("semisweet chocolate (61-72% cacao), chopped", 500.g, 0..3)),
                // value variations
                ("500.5g flour"     to result("flour", 500.5.g,     0..5)),
                ("0.5g flour"       to result("flour", 0.5.g,       0..3)),
                (".5g flour"        to result("flour", 0.50.g,      0..2)),
                ("1,5g flour"        to result("flour", 1.5.g,      0..3)),
                // fraction value variations
                ("1 1/2 cup flour"  to result("flour", 1.5f,    "cup", 0..8)),
                ("3/4 cup flour"    to result("flour", .75f,    "cup", 0..6)),
                ("¾ cup flour"      to result("flour", .75f,    "cup", 0..4)),
                ("½ cup flour"      to result("flour", .5f,     "cup", 0..4)),
                // spacing variations
                ("flour500g"        to result("flour", 500.g, 5..8)),
                ("500g   flour"     to result("flour", 500.g, 0..3)),
                ("500 g flour"      to result("flour", 500.g, 0..4)),
                ("500g\t\tflour"    to result("flour", 500.g, 0..3))
            ).forEach {
                it("'${it.key}'") {
                    assertEquals(it.value, matcher.matchEntire(it.key))
                }
            }
        }

        context("should match unit") {
            val units = listOf(
                "g", "gram", "grams",
                "kg", "kilogram", "kilograms",
                "oz", "ounce", "ounces", "lb", "lbs", "pound", "pounds",
                "tsp", "tsp.", "tsps", "teaspoon", "teaspoons",
                "tbsp", "tbsp.", "tbsps", "tablespoon", "tablespoons",
                "cup", "cups",
                "г", "г.", "гр", "гр.", "грам", "грамм", "грамов", "граммов",
                "кг", "кг.", "килограм", "килограмм", "килограммов",
                "ч.л.", "ч. л.", "ч л", "чайная ложка", "чайные ложки", "чайных ложек",
                "ч. ложка", "ч. ложки", "ч. ложек", "ч.ложка", "ч.ложки", "ч.ложек",
                "ст.л.", "ст. л.", "ст л", "столовая ложка", "столовые ложки", "столовых ложек",
                "ст. ложка", "ст. ложки", "ст. ложек", "ст.ложка", "ст.ложки", "ст.ложек",
                "мл", "мл.", "л", "л.", "литр", "литра", "литров",
                "ст.", "ст.", "стакан", "стакана", "стаканов"
            )

            units.map {
                ("ingredient 500 $it" to result("ingredient", 500f, it, 11 until 15 + it.length))
            }.forEach {
                it("'${it.first}'") {
                    assertEquals(it.second, matcher.matchEntire(it.first))
                }
            }

            units.map {
                ("500 $it ingredient" to result("ingredient", 500f, it, 0 until 4 + it.length))
            }.forEach {
                it("'${it.first}'") {
                    assertEquals(it.second, matcher.matchEntire(it.first))
                }
            }
        }

        context("should not match") {
            listOf(
                ("500g"),
                ("   500g"),
                ("flour"),
                ("flour 500"),
                ("flour g"),
                ("500g   "),
                ("500 flour"),
                ("g flour")
            ).forEach {
                it("'$it'") {
                    assertEquals(NoMatch, matcher.matchEntire(it))
                }
            }
        }
    }
})

private fun result(name: String, amount: Amount, amountRange: IntRange): ParseResult.Ingredient {
    return ParseResult.Ingredient(Ingredient(name, amount), amountRange)
}

private fun result(name: String, value: Float, unit: String, amountRange: IntRange): ParseResult.Ingredient {
    return ParseResult.Ingredient(Ingredient(name, Measurable(value, unit)), amountRange)
}