package com.vokilam.servescalc.recipeparser

import com.vokilam.servescalc.domain.model.ParseResult
import io.mockk.clearAllMocks
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Assert.assertEquals
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class MatcherChainSpek : Spek({

    describe("MatcherChain") {

        context("when chain has matchers") {
            val matcher1 = mockk<Matcher>()
            val matcher2 = mockk<Matcher>()
            val matchers by memoized { listOf(matcher1, matcher2) }
            val chain by memoized { MatcherChain(matchers) }

            afterEachTest {
                clearAllMocks()
            }

            context("when current matcher returns NoMatch") {
                beforeEachTest {
                    every { matcher1.matchEntire("input") } returns ParseResult.NoMatch
                    every { matcher2.matchEntire("input") } returns ParseResult.Blank
                    chain.matchEntire("input")
                }

                it("should invoke next matcher") {
                    verify { matcher1.matchEntire("input") }
                    verify { matcher2.matchEntire("input") }
                    confirmVerified(matcher1, matcher2)
                }
            }

            context("when input matches") {
                lateinit var actual: ParseResult

                beforeEachTest {
                    every { matcher1.matchEntire("input") } returns ParseResult.Blank
                    actual = chain.matchEntire("input")
                }

                it("should return result") {
                    assertEquals(ParseResult.Blank, actual)
                }

                it("should interact with matcher1 only") {
                    verify { matcher1.matchEntire("input") }
                    confirmVerified(matcher1, matcher2)
                }
            }
        }

        context("when chain is empty") {
            val chain by memoized { MatcherChain(emptyList()) }

            it("should return NoMatch") {
                val actual = chain.matchEntire("input")
                assertEquals(ParseResult.NoMatch, actual)
            }
        }
    }
})