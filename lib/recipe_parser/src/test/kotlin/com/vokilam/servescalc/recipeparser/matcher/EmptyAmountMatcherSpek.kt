package com.vokilam.servescalc.recipeparser.matcher

import com.vokilam.servescalc.domain.model.Amount.Empty
import com.vokilam.servescalc.domain.model.Ingredient
import com.vokilam.servescalc.domain.model.ParseResult
import org.junit.Assert.assertEquals
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class EmptyAmountMatcherSpek : Spek({
    describe("EmptyAmountMatcher") {
        val matcher by memoized { EmptyAmountMatcher() }

        context("should match") {
            mapOf(
                // basic
                ("coarsely ground pepper" to result("coarsely ground pepper")),
                // encoding
                ("мука для обваливания" to result("мука для обваливания")),
                // case insensitivity
                ("FLOUR FOR DUSTING") to result("FLOUR FOR DUSTING")
            ).forEach {
                it("'${it.key}'") {
                    assertEquals(it.value, matcher.matchEntire(it.key))
                }
            }
        }

        context("should not match") {
            listOf(
                "flour 500g",
                "500g",
                "1/2 cup flour",
                "1/2 cup",
                "4 eggs",
                "eggs 4",
                "salt 4 pinches",
                "salt to taste",
                "to taste"
            ).forEach {
                it("'$it'") {
                    assertEquals("$it -> NoMatch", ParseResult.NoMatch, matcher.matchEntire(it))
                }
            }
        }
    }
})

private fun result(name: String): ParseResult.Ingredient {
    return ParseResult.Ingredient(Ingredient(name, Empty), IntRange.EMPTY)
}