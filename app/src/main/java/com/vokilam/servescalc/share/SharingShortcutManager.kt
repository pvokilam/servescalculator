package com.vokilam.servescalc.share

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.core.content.pm.ShortcutInfoCompat
import androidx.core.content.pm.ShortcutManagerCompat
import androidx.core.graphics.drawable.IconCompat
import javax.inject.Inject
import com.vokilam.servescalc.R

class SharingShortcutManager @Inject constructor() {
    fun pushDirectShareTargets(context: Context) {
        val shortcuts = mutableListOf<ShortcutInfoCompat>()

        shortcuts.add(
            ShortcutInfoCompat.Builder(context, "add_recipe")
                .setShortLabel(context.getString(R.string.shortcut_label_add_recipe))
                .setIcon(IconCompat.createWithResource(context, R.mipmap.ic_shortcut_add_recipe))
                .setIntent(Intent(Intent.ACTION_VIEW).setData(Uri.parse("servescalc://recipes/add")))
                .setCategories(setOf(CATEGORY_TEXT_SHARE_TARGET))
                .build()
        )

        ShortcutManagerCompat.addDynamicShortcuts(context, shortcuts)
    }

    fun removeAllDirectShareTargets(context: Context) {
        ShortcutManagerCompat.removeAllDynamicShortcuts(context)
    }

    companion object {
        private const val CATEGORY_TEXT_SHARE_TARGET =
            "com.vokilam.servescalc.sharingshortcuts.category.TEXT_SHARE_TARGET"
    }
}
