package com.vokilam.servescalc

import android.app.Application
import android.content.Context
import androidx.lifecycle.ProcessLifecycleOwner
import com.facebook.drawee.backends.pipeline.Fresco
import com.vokilam.logger.Logger
import com.vokilam.servescalc.core.util.log.ActivityLifecycleLogger
import com.vokilam.servescalc.core.util.log.AppLifecycleLogger
import com.vokilam.servescalc.core.util.log.ComponentCallbacksLogger
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        setupLogging()
        Fresco.initialize(this)
    }

    private fun setupLogging() {
        if (BuildConfig.DEBUG) {
            Logger.init()
            System.setProperty("kotlinx.coroutines.debug", "on")
            ProcessLifecycleOwner.get().lifecycle.addObserver(AppLifecycleLogger)
            registerActivityLifecycleCallbacks(ActivityLifecycleLogger)
            registerComponentCallbacks(ComponentCallbacksLogger)
        }
    }

    companion object
}

fun App.Companion.get(context: Context) = context.applicationContext as App
