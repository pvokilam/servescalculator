package com.vokilam.servescalc.presentation.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.vokilam.logger.Logger
import com.vokilam.servescalc.BuildConfig
import com.vokilam.servescalc.R
import com.vokilam.servescalc.core.presentation.ui.ImportRecipeDataViewModel
import com.vokilam.servescalc.core.presentation.hideKeyboard
import com.vokilam.servescalc.core.util.log.FragmentLifecycleLogger
import com.vokilam.servescalc.core.util.log.NavigationControllerDestinationLogger
import com.vokilam.servescalc.databinding.ActivityMainBinding
import com.vokilam.servescalc.feature.recipelist.presentation.ui.RecipeListFragmentDirections.Companion.actionToAddRecipe
import com.vokilam.servescalc.share.SharingShortcutManager
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import kotlin.LazyThreadSafetyMode.NONE

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject lateinit var sharingShortcutManager: SharingShortcutManager
    private val viewModel by viewModels<ImportRecipeDataViewModel>()

    private val binding by lazy(NONE) { ActivityMainBinding.inflate(layoutInflater) }
    private val navController by lazy(NONE) {
        (supportFragmentManager.findFragmentById(R.id.navHost) as NavHostFragment).navController
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setupToolbar()
        setupLogging()
        publishSharingShortcuts()

        if (savedInstanceState == null) {
            handleIntent(intent)
        }

        viewModel.navigateToAddRecipe.observe(this) {
            L.verb { "navigateToAddRecipe: $it" }
            navController.run {
                if (currentDestination?.id != R.id.addRecipeFragment) {
                    navigate(actionToAddRecipe(it))
                }
            }
        }

        navController.addOnDestinationChangedListener { _, _, _ ->
            findViewById<View>(android.R.id.content).hideKeyboard(true)
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(binding.toolbar)
        NavigationUI.setupActionBarWithNavController(this, navController)
    }

    private fun setupLogging() {
        if (BuildConfig.DEBUG) {
            navController.addOnDestinationChangedListener(NavigationControllerDestinationLogger)
            supportFragmentManager.registerFragmentLifecycleCallbacks(FragmentLifecycleLogger, true)
        }
    }

    private fun publishSharingShortcuts() {
        sharingShortcutManager.pushDirectShareTargets(applicationContext)
    }

    private fun handleIntent(intent: Intent) {
        L.debug { "handleIntent(): intent=$intent" }

        if (Intent.ACTION_SEND == intent.action && intent.type == "text/plain") {
            val body = intent.getStringExtra(Intent.EXTRA_TEXT).orEmpty()
            L.debug { "handleIntent(): body=$body" }
            viewModel.handleImportRecipeData(body)
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        L.debug { "onNewIntent(): intent=$intent" }
        setIntent(intent)
        handleIntent(intent)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp()
    }

    companion object {
        private val L = Logger()
    }
}
