# Serve app
This document is not a conventional project description but rather a collection of key things I learned during Android app development.

## TL;DR
This is my recent side project in a culinary area. Serve app lets you enter recipe ingredients easily and adjust amounts according to your needs.

My goal was self-improvement in modern Android app development and publishing. Check out the beta on [Play Store][play_store].

This app is part of a larger project that includes user research and analysis. Results of the whole project are described [here][home_cook_project].

### Dry facts

- **Dev process:** 4 full-time weeks of scrum-like process with user stories applied ([trello board][trello_board])
- **Architecture:** multi module clean with install-time dynamic feature modules
- **Dev Stack:** Kotlin, coroutines, Dagger2, assisted injection, MVVM, data binding, navigation, room 
- **Testing:** JUnit4, Spek, TDD + freestyle
- **Tasks:** import ingredients from web and photo, text recognition, text language detection, sanitize and parse input with regex, calculate adjusted servings

---

## Introduction
My main goal is self-development, gaining wide knowledge of full product design and development. Rather wide than deep knowledge, ability to bring structure to anything definitely describe me.

Other thing is that I like cooking (baking bread and pastry in particular) so culinary topic keeps me highly motivated.

To be motivated even more, I decided to pick a problem area, find the right problem to solve and provide a solution to it.

> Problem area: How might we improve cooking process of home cooks by creating a mobile application?

I structured my work according to [double-diamond framework][dd_framework]. In a nutshell, there are 4 stages: 

- discover – look for the right problem to solve, conduct research
- define – analyse research results, define potential problems to solve
- develop – generate ideas, pick promising ones, make them tangible
- deliver – build prototypes and test on users, pick winning idea and build an app

This Android app represents the winning idea. See complete project description [here][home_cook_project].

---

## App overview

Check out the beta on [Play Store][play_store].

Serve app let you enter recipe ingredients easily & adjust amounts to your needs.

You don't need to type ingredients by hand:

- Make a photo of your ingredients list from a book, laptop display, handwritten cookbook and Serve app will recognize text on the photo.
- Select ingredients text in browser and share to the app.

Serve app is for passionate home cooks who value precise measurement. Adjust your ingredients:

- by custom multiplier, e.g. 1.5x
- by cooking ring diameter (valuable for cake makers), e.g. ⌀18
- proportional to leftover ingredient

Save recipes and use them for measurements next time.

Import from photo demo:

![demo](docs/demo_import_from_photo_ru.gif)

Import from web demo:

![demo](docs/demo_import_from_website_ru.gif)

---

## Key learnings

### Testing

To emphasize TDD style, this section is placed at the beginning :)

Overall, code covered with tests makes me much more confident and open to introducing changes and experiments. This was especially true for `:lib:recipe_parser` module, where indredient lines are split into name, value and unit using regex parser. See `MeasurableIngredientMatcherSpek` for example.

Setting up testing dependencies for multi module project for the first time was a pain. I spent total of several days trying to set things up. After several attempts I decided to leave setting up espresso and robolectric and focus on unit tests.

#### TDD
I noticed that TDD style forces me to think ahead before coding, create a dev plan. 

Moreover, I noticed that when I start at UI level by writing a view model test, it forces me to go through the whole data flow from UI through domain to data and back.

> Okay, let's write a view model test. What are the inputs and outputs? 

> Oh, view model needs a use case to perform that action - ok, let's write a test for the use case. What is the interface of the use case? What domain entities do I need? 

> Oh, use case needs a repository to obtain that data - ok, let's write a test for the repository. What is the interface of the repository? What is the simpliest implementation I can do for now?

> Cool, now I can run application and verify that data flow is working.

My thinking was shifted towards testing-first pretty soon.

##### Violation of TDD rules

Opposed to what Kent Beck showed in his book, I declared interfaces (methods, inputs, outputs) and then used them in tests, skipping "code won't compile" stage. In other words, I made larger steps.

When I experimented with a solution and made it working, I sometimes kept it as is and covered _existing_ code with tests. No need to say that test coverage was worse and edge use cases might not be covered.

#### Test engines

##### JUnit4
JUnit4 is a classic solution with existing rules for mocking main executors of live data and coroutines. However, it is hard to group tests and reuse logic of setting object's "given" state.

##### Spek
Spek test gives you grouping options, you can set object's state for each group and perform parametrized tests using language features. However, it does not support neither JUnit4 rules nor JUnit5 extensions, so you have to mock main executors manually or use [hacks](https://github.com/spekframework/spek/issues/118).

Android Spek community is relatively small and most articles are outdated.

Also I have problems in setting up Spek for Android modules in multi module project. So currenlty I use it only in kotlin modules.

#### Some observations
- Test behavior not implemantation details. Practically, it means: prefer testing live data output and method return values rather than verifying mocks. This will minimize changes to tests during codebase evolution.
- Kotlin allows to make test method names more readable (however chars like `.[]/` are not allowed but you can use commas): 
      
	    fun `shouldShowSortAction should be false when recipe list is empty`()

- I switched to mockk because of better support of coroutines.

        coEvery { getRecipesUseCase() } returns TestData.recipes

- It is important to use `LiveData#getOrAwaitValue()` and `LiveData#observeForTesting()` for transformed streams. This is well described in this [article](https://medium.com/androiddevelopers/unit-testing-livedata-and-other-common-observability-problems-bb477262eb04). I spent some time debugging such tests :)


- Inject coroutines dispatcher in order to mock non-main dispatchers in tests. See `CoroutinesDispatcherProvider`. Test version always returns `TestCoroutineDispatcher`.

- I defined DSL for easier creation of recipe objects. See `RecipeDsl.kt` for definition and `TestData` for usage.
		
		val fullRecipe = recipe {
		    name = "recipe"
		    serving = Serving.None
		    ingredients {
		        +("water" to 320.g)
		        +("flour" to 500.g)
		        +("yeast" to 5.g)
		    }
		}

#### Improvements
- Try an assertion library (currently default junit assetions are used)
- Split large tests or, even better, split large classes under test into smaller

---

### Multi module structure
Module structure has evolved during development. And in the end it [screams](https://blog.cleancoder.com/uncle-bob/2011/09/30/Screaming-Architecture.html) what this project is about.

![project_structure](docs/project_structure.png)

Although I don't have exact proofs, I was left with a feeling that multi module structure is harder to setup. I faced with such problems as obfuscation errors, testing framework errors, library conflicts caused by dynamic modules, I believe.

Layered structure (presentation, domain, data) is maintained across feature modules and library modules. `:feature:recipe_add` is an example where all layers are present, while `:feature:recipe_list` has only presentation level.

#### Dynamic feature modules
I defined three feature modules (display list, display details, add recipe) according to The Common Closure Principle.

> Gather into components those classes that change for the same reasons and at the same times. Separate into different components those classes that change at different times and for different reasons.

In fact, usually all modules were affected during early stages of development. But separation becomes more clear towards the end of development. It was especially true for `:feature:recipe_add` module, that we would remove in a readonly version of the app.

Currently each feature module contains only one fragment but more screens are acceptable. E.g. recipe list feature may contain list-related screens like tag assignment and management, search or filtering. 

To stay away from dependency hell most of android dependencies that are used in feature modules are defined once in `:app` module using `api` instead of `implementation`.

#### Other modules

- :app – base mobile app module, contains single activity and definition of navigation graph
- :lib:core – common module with shared utils and database repository
- :lib:domain – java module, contains domain level data models and interfaces
- :lib:core_test – common module for tests, contains junit rules and shared test data
- :lib:recipe_parser – java module, regex parser of ingredients line text

#### Parallel build
Parallelism on 6-core Intel Core i7 processor
![parallel build](docs/parallel_build.png)

---

### DI

[Component dependencies](https://developer.android.com/training/dependency-injection/dagger-multi-module) are used instead of subcomponents because of reversed dependency between `:app` and dynamic feature modules (subcomponents defined in feature modules are not accessible by base module). This leads to several implementation aspects:

- We cannot use android injection because it is implemented on subcomponents
- Child components declare dependencies to parent components
- When creating a child component, parent components should be provided via factory method or builder
- Parent components must explicitly declare dependencies they provide to child components in component's interface
- Multibinding works only within a single component (this affects, how view models are injected, see below)

#### Dependencies exposure

General rule is to expose dagger module or component only and keep all implementation details module-private. A good example of such approach is `:lib:recipe_parser` module that has only one public class - `RecipeParserModule`.

`:lib:core` also provides `CoreModule` that is installed into `AppComponent`.

---

### ViewModel

#### Injection

A classic approach to provide a view model is to inject view model factory and pass it when obtaining instance of view model. View model factory collects viewmodel providers using map multibinding to which every module can contribute its view model.

The issue is that multibinding works only within a single component for component dependencies as opposed to subcomponents. The solution was to keep view model factory approach at injection level, but inject a factory that provides a particular view model only. See `SingleViewModelFactory`.

#### ViewModel state & assisted injection

[Assisted injection](https://github.com/square/AssistedInject) helps dagger to satisfy dependencies that are not available at compile time. We use it to provide `SavedStateHandle` to view model's constructor.

Two changes were made on injection side:
- `SingleSavedStateViewModelFactory` is used as a view model factory
- We bind fragment instance to a component in order to create that factory

Overall, my impression of this approach is mixed. Although it helps to inject saved state in traditional way, I encounter following:

- Sometimes you need to build twice to get all code generated correctly. Also you need to build at least once to be able to attach assisted module to a component.
- Generalizing this approach to `ViewModelAssistedFactory` was not intuitive.
- Assisted injection resembles instance binding (using `@BindsInstance`)

#### Communication between activity and fragment

App supports text share flow, when ingredients text selected in browser is shared into the app. The task was to pass that data from intent handling activity to `AddRecipeFragment`. `MainViewModel` was shared to the fragment and public properties used by activity only were marked as module-private, so that fragment cannot access them.

#### Communication between fragment and dialog

Several dialogs are displayed in context of a fragment. To communicate from dialog to fragment, fragment's view model was shared. See `ConfirmExitDialogFragment` for details. This is a simple but not reusable approach.

Dialog can be defined in navigation graph, but in such case it is added as a child of NavHostFragment and we cannot access shared view model of a fragment. Therefore, dialogs are displayed manually as a child of calling fragment.

---

### Navigation component

All destinations are declared in `:app` module. So this is the only place where base module has knowledge about dynamic features.

Alternative way that also supports on-demand feature modules is [in development by Google](https://developer.android.com/guide/navigation/navigation-dynamic).

----

### Data binding

Two way data binding was used in `AddRecipeFragment` – text input updates are sent to view model directly via live data.

Also, data binding was used at list item level, mainly because of convenient way to obtain entity associated with specific list item and pass it to a callback.

---

### Coroutines

Easy to use and understand. Coroutines source code is not so easy to understand :)

Learned how to debug and log coroutines. Also converted callback based api to coroutines, see `FirebaseTextLanguageDetector`.

---

### Parsing ingredients

I processed several datasets in order to extract common patterns of writing ingredients list. English datasets were taken from Kaggle. I had to write several web crawlers for russian datasets.

Writing regular expressions took a substantial amount of time. To keep expressions manageable, I introduced a regex DSL builder. See `com.vokilam.servescalc.recipeparser.matcher` package for more examples.

Before:

    private val REGEX_VALUE = "([\\d.,/ ]+)"
	private val MEASURABLE_UNITS by lazy {
	    listOf("g", "oz", ...).joinToString("|")
	}
	private val REGEX_MEASURABLE_UNIT = "($MEASURABLE_UNITS)"
	val REGEX_MEASURABLE_AMOUNT = """($REGEX_VALUE\s?$REGEX_MEASURABLE_UNIT)"""

After:

	fun RegexContext.value() = group { +"""[\d.,/ ]+?""" }
	fun RegexContext.valueUnitSeparator() = +"""\s?"""
	fun RegexContext.measurableUnit() {
	    group { anyOf("g", "oz", ...) }
	}
	
	fun RegexContext.measurableAmount() { // matches '500 g'
	    value()
	    valueUnitSeparator()
	    measurableUnit()
	}

---

### Referenced projects
- [Plaid](https://github.com/android/plaid)
- [Google I/O 2019 Android App](https://github.com/google/iosched)
- [android-showcase](https://github.com/igorwojda/android-showcase)
- [Material components - Owl](https://github.com/material-components/material-components-android-examples/tree/develop/Owl)
- [Android architecture samples](https://github.com/android/architecture-samples)
- [Android testing samples](https://github.com/android/testing-samples)

[play_store]: https://play.google.com/store/apps/details?id=com.vokilam.servescalc
[dd_framework]: https://medium.com/digital-experience-design/how-to-apply-a-design-thinking-hcd-ux-or-any-creative-process-from-scratch-b8786efbf812
[trello_board]: https://trello.com/b/HSVOhp8x
[home_cook_project]: https://docs.google.com/document/d/1Tlwn7jawOwwsLOCfvm3Y4DWtZcXHUSVYO-C5Lk77xtg/edit?usp=sharing