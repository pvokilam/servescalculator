package com.vokilam.servescalc.feature.recipedetails.presentation.model

import com.vokilam.servescalc.domain.model.Amount
import com.vokilam.servescalc.domain.model.Ingredient
import org.junit.Assert.assertEquals
import org.junit.Test

class IngredientModelTest {

    @Test
    fun `mapper should format amount value`() {
        val countableModel = IngredientModel.from(1, false, Ingredient("name", Amount.Countable(1f))) { "countable" }
        assertEquals("countable", (countableModel as IngredientModel.WithValueAndUnit).formattedValue)

        val measurableModel = IngredientModel.from(1, false, Ingredient("name", Amount.Measurable(2f, "g"))) { "measurable" }
        assertEquals("measurable", (measurableModel as IngredientModel.WithValueAndUnit).formattedValue)
    }
}
