package com.vokilam.servescalc.feature.recipedetails.presentation

import androidx.arch.core.executor.ArchTaskExecutor
import androidx.lifecycle.SavedStateHandle
import com.vokilam.core.test.TestData
import com.vokilam.core.test.util.TestTaskExecutor
import com.vokilam.core.test.util.getOrAwaitValue
import com.vokilam.servescalc.core.domain.usecase.GetFullRecipeUseCase
import com.vokilam.servescalc.core.domain.usecase.UpdateAdjustmentUseCase
import com.vokilam.servescalc.domain.dsl.recipe
import com.vokilam.servescalc.domain.model.Adjustment
import com.vokilam.servescalc.domain.model.AdjustmentType
import com.vokilam.servescalc.domain.model.Serving
import com.vokilam.servescalc.domain.model.g
import com.vokilam.servescalc.domain.repository.RecipesRepository
import com.vokilam.servescalc.feature.recipedetails.presentation.model.CurrentServingModel
import com.vokilam.servescalc.feature.recipedetails.presentation.model.CustomServingModel
import com.vokilam.servescalc.feature.recipedetails.presentation.model.IngredientModel
import com.vokilam.servescalc.feature.recipedetails.presentation.ui.NavigationEvent.*
import com.vokilam.servescalc.feature.recipedetails.presentation.ui.RecipeDetailsViewModel
import com.vokilam.servescalc.feature.recipedetails.presentation.model.AdjustmentModel
import com.vokilam.servescalc.feature.recipedetails.presentation.ui.RecipeDetailsViewModel.Companion.AMOUNT_VALUE_FORMAT
import io.mockk.Runs
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.just
import io.mockk.mockk
import io.mockk.slot
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class RecipeDetailsViewModelSpek : Spek({
    val testDispatcher = TestCoroutineDispatcher()

    beforeEachTest {
        ArchTaskExecutor.getInstance().setDelegate(TestTaskExecutor())
        Dispatchers.setMain(testDispatcher)
    }

    afterEachTest {
        ArchTaskExecutor.getInstance().setDelegate(null)
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    val getFullRecipeUseCase = mockk<GetFullRecipeUseCase>()
    val updateAdjustmentUseCase = mockk<UpdateAdjustmentUseCase>()
    val state by memoized { SavedStateHandle() }

    val viewModel by memoized {
        RecipeDetailsViewModel(
            getFullRecipeUseCase,
            updateAdjustmentUseCase,
            state
        )
    }

    afterEachTest {
        clearAllMocks()
    }

    describe("init") {
        // TODO test models' default states
        describe("on restore") {
            beforeEachTest {
                state.set(
                    RecipeDetailsViewModel.KEY_ADJUSTMENT, AdjustmentModel(0.5f, AdjustmentType.BY_SERVING)
                )

                val fullRecipe = recipe {
                    name = "recipe"
                    serving = Serving.NONE
                    ingredients {
                        +("water" to 320.g)
                        +("flour" to 500.g)
                        +("yeast" to 5.g)
                    }
                }

                val id = fullRecipe.recipe.id
                coEvery { getFullRecipeUseCase(id) } returns fullRecipe
                viewModel.init(id)
            }

            it("should restore scale factor") {
                val actual = viewModel.ingredients.getOrAwaitValue()
                    .filterIsInstance<IngredientModel.WithValueAndUnit>()
                    .map { it.formattedValue }
                val expected = listOf("160", "250", "2.5")
                assertEquals(expected, actual)
            }

            it("should restore current serving") {
                val actual = viewModel.currentServingModel.getOrAwaitValue()
                val expected = CurrentServingModel(true, "0.5 x", true)
                assertEquals(expected, actual)
            }
        }
    }

    describe("load recipe") {
        context("common") {
            val fullRecipe = TestData.fullRecipe

            beforeEachTest {
                val id = fullRecipe.recipe.id
                coEvery { getFullRecipeUseCase(id) } returns fullRecipe

                viewModel.init(id)
            }

            it("should load entity by id") {
                val expected = fullRecipe.ingredients.mapIndexed { index, it ->
                    IngredientModel.from(index, false, it, AMOUNT_VALUE_FORMAT::format)
                }
                val actual = viewModel.ingredients.getOrAwaitValue()
                assertEquals(expected, actual)
            }

            it("should update current serving") {
                val actual = viewModel.currentServingModel.getOrAwaitValue()
                val expected = CurrentServingModel(true, "1 x", false)
                assertEquals(expected, actual)
            }

            it("should format amount with #.# precision") {
                val actual = viewModel.ingredients.getOrAwaitValue()
                assertEquals("3.3", (actual[3] as IngredientModel.WithValueAndUnit).formattedValue)
            }
        }

        context("adjusted") {
            val fullRecipe = recipe {
                name = "Adjusted recipe"
                serving = Serving.Custom(2f)
                adjustment = Adjustment(4f, AdjustmentType.BY_SERVING)
                ingredients {
                    +("flour" to 500.g)
                }
            }

            beforeEachTest {
                val id = fullRecipe.recipe.id
                coEvery { getFullRecipeUseCase(id) } returns fullRecipe

                viewModel.init(id)
            }

            it("should load entity by id") {
                val expected = fullRecipe.ingredients.mapIndexed { index, it ->
                    IngredientModel.from(index, false, it * 2, AMOUNT_VALUE_FORMAT::format)
                }
                val actual = viewModel.ingredients.getOrAwaitValue()
                assertEquals(expected, actual)
            }

            it("should update current serving") {
                val actual = viewModel.currentServingModel.getOrAwaitValue()
                val expected = CurrentServingModel(true, "4 ", true)
                assertEquals(expected, actual)
            }
        }
    }

    describe("item interaction") {
        val fullRecipe = TestData.fullRecipe

        beforeEachTest {
            val id = fullRecipe.recipe.id
            coEvery { getFullRecipeUseCase(id) } returns fullRecipe

            viewModel.init(id)
        }

        context("on amount change") {
            beforeEachTest {
                val ingredient = fullRecipe.ingredients.first().let {
                    IngredientModel.from(0, false, it, AMOUNT_VALUE_FORMAT::format)
                }
                viewModel.onAmountValueChanged(ingredient as IngredientModel.WithValueAndUnit, "250")
            }

            it("should update current serving") {
                val actual = viewModel.currentServingModel.getOrAwaitValue()
                val expected = CurrentServingModel(true, "0.5 x", true)
                assertEquals(expected, actual)
            }

            it("should adjust other ingredients") {
                val actual = viewModel.ingredients.getOrAwaitValue()
                    .filterIsInstance<IngredientModel.WithValueAndUnit>()
                    .map { it.formattedValue }
                // TODO TestData.fullRecipeScaled_0_5?
                val expected = listOf("250", "160", "5", "1.6")
                assertEquals(expected, actual)
            }

            context("on back pressed") {
                val adjustment = slot<Adjustment>()

                beforeEachTest {
                    coEvery { updateAdjustmentUseCase(fullRecipe.recipe.id, capture(adjustment)) } returns true
                    viewModel.onBackPressed()
                }

                it("should save last adjustment") {
                    assertEquals(Adjustment(0.5f, AdjustmentType.BY_AMOUNT), adjustment.captured)
                }
            }
        }

        context("on item click") {
            beforeEachTest {
                val ingredient = fullRecipe.ingredients.first().let {
                    IngredientModel.from(0, false, it, AMOUNT_VALUE_FORMAT::format)
                }
                viewModel.onItemClicked(ingredient)
            }

            it("should toggle selected state") {
                val ingredients = viewModel.ingredients.getOrAwaitValue()
                val selectedIngredient = ingredients.first()
                assertTrue(selectedIngredient.isChecked)
            }
        }
    }

    describe("scale factor") {
        val fullRecipe = TestData.fullRecipe

        beforeEachTest {
            val id = fullRecipe.recipe.id
            coEvery { getFullRecipeUseCase(id) } returns fullRecipe

            viewModel.init(id)

            val ingredient = fullRecipe.ingredients.first().let {
                IngredientModel.from(0, false, it, AMOUNT_VALUE_FORMAT::format)
            }
            viewModel.onAmountValueChanged(ingredient as IngredientModel.WithValueAndUnit, "250")
        }

        context("on reset") {
            beforeEachTest {
                viewModel.onResetScaleFactor()
            }

            it("should update current serving") {
                val actual = viewModel.currentServingModel.getOrAwaitValue()
                val expected = CurrentServingModel(true, "1 x", false)
                assertEquals(expected, actual)
            }

            it("should adjust other ingredients") {
                val actual = viewModel.ingredients.getOrAwaitValue()
                    .filterIsInstance<IngredientModel.WithValueAndUnit>()
                    .map { it.formattedValue }
                val expected = listOf("500", "320", "10", "3.3")
                assertEquals(expected, actual)
            }
        }

        context("on click") {
            beforeEachTest {
                viewModel.onScaleFactorClick()
            }

            it("should show custom serving prompt") {
                val actual = viewModel.navigationEvents.value
                val expected = ShowCustomServingDialog(CustomServingModel(suffix = "x", value = "0.5"))
                assertEquals(expected, actual)
            }
        }

        context("on custom serving selected") {
            beforeEachTest {
                viewModel.onCustomServingSelected("5")
            }

            it("should update custom serving") {
                val actual = viewModel.currentServingModel.getOrAwaitValue()
                val expected = CurrentServingModel(true, "5 x", true)
                assertEquals(expected, actual)
            }

            it("should adjust ingredients") {
                val actual = viewModel.ingredients.getOrAwaitValue()
                    .filterIsInstance<IngredientModel.WithValueAndUnit>()
                    .map { it.formattedValue }
                val expected = listOf("2,500", "1,600", "50", "16.5")
                assertEquals(expected, actual)
            }

            context("on back pressed") {
                val adjustment = slot<Adjustment>()

                beforeEachTest {
                    coEvery { updateAdjustmentUseCase(fullRecipe.recipe.id, capture(adjustment)) } returns true
                    viewModel.onBackPressed()
                }

                it("should save last adjustment") {
                    assertEquals(Adjustment(5f, AdjustmentType.BY_AMOUNT), adjustment.captured)
                }
            }
        }
    }

    describe("exit") {
        val fullRecipe = TestData.fullRecipe
        val adjustment = slot<Adjustment>()

        beforeEachTest {
            val id = fullRecipe.recipe.id
            coEvery { getFullRecipeUseCase(id) } returns fullRecipe
            coEvery { updateAdjustmentUseCase(id, capture(adjustment)) } returns true

            viewModel.init(id)
            viewModel.onCustomServingSelected("2")
        }

        context("on back pressed") {
            beforeEachTest {
                viewModel.onBackPressed()
            }

            it("should save last adjustment") {
                assertEquals(Adjustment(2f, AdjustmentType.BY_SERVING), adjustment.captured)
            }

            it("should exit") {
                assertEquals(Exit, viewModel.navigationEvents.value)
            }
        }

        context("on up pressed") {
            beforeEachTest {
                viewModel.onUpPressed()
            }

            it("should save last adjustment") {
                assertEquals(Adjustment(2f, AdjustmentType.BY_SERVING), adjustment.captured)
            }

            it("should exit") {
                assertEquals(Exit, viewModel.navigationEvents.value)
            }
        }
    }

    describe("edge cases") {
        context("on amount change of a repeated ingredient name") {
            beforeEachTest {
                val fullRecipe = recipe {
                    name = "White bread dough"
                    serving = Serving.NONE
                    ingredients {
                        +("flour" to 500.g)
                        +("flour" to 250.g)
                    }
                }
                val id = fullRecipe.recipe.id
                coEvery { getFullRecipeUseCase(id) } returns fullRecipe
                viewModel.init(id)

                val ingredient = fullRecipe.ingredients[1].let {
                    IngredientModel.from(1, false, it, AMOUNT_VALUE_FORMAT::format)
                }
                viewModel.onAmountValueChanged(ingredient as IngredientModel.WithValueAndUnit, "125")
            }

            it("should select changed ingredient") {
                val actual = viewModel.ingredients.getOrAwaitValue()
                    .filterIsInstance<IngredientModel.WithValueAndUnit>()
                    .map { it.formattedValue }
                val expected = listOf("250", "125")
                assertEquals(expected, actual)
            }
        }
    }
})