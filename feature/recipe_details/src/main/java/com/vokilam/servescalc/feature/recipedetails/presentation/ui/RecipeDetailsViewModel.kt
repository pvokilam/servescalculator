package com.vokilam.servescalc.feature.recipedetails.presentation.ui

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vokilam.logger.Logger
import com.vokilam.servescalc.core.domain.usecase.GetFullRecipeUseCase
import com.vokilam.servescalc.core.domain.usecase.UpdateAdjustmentUseCase
import com.vokilam.servescalc.core.presentation.MediatorLiveData
import com.vokilam.servescalc.core.presentation.update
import com.vokilam.servescalc.core.presentation.util.LiveEvent
import com.vokilam.servescalc.core.presentation.util.SavedStateDelegates
import com.vokilam.servescalc.domain.model.Adjustment
import com.vokilam.servescalc.domain.model.AdjustmentType
import com.vokilam.servescalc.domain.model.FullRecipe
import com.vokilam.servescalc.domain.model.RecipeId
import com.vokilam.servescalc.domain.model.Serving
import com.vokilam.servescalc.feature.recipedetails.presentation.model.AdjustmentModel
import com.vokilam.servescalc.feature.recipedetails.presentation.model.CurrentServingModel
import com.vokilam.servescalc.feature.recipedetails.presentation.model.CustomServingModel
import com.vokilam.servescalc.feature.recipedetails.presentation.model.IngredientModel
import com.vokilam.servescalc.feature.recipedetails.presentation.model.ToolbarModel
import com.vokilam.servescalc.feature.recipedetails.presentation.toAdjustment
import com.vokilam.servescalc.feature.recipedetails.presentation.toAdjustmentModel
import com.vokilam.servescalc.feature.recipedetails.presentation.ui.NavigationEvent.Exit
import com.vokilam.servescalc.feature.recipedetails.presentation.ui.NavigationEvent.ShowCustomServingDialog
import kotlinx.coroutines.launch
import java.text.DecimalFormat
import java.text.NumberFormat

@Suppress("TooManyFunctions")
class RecipeDetailsViewModel @ViewModelInject constructor(
    private val getFullRecipeUseCase: GetFullRecipeUseCase,
    private val updateAdjustmentUseCase: UpdateAdjustmentUseCase,
    @Assisted override val state: SavedStateHandle
) : ViewModel(), SavedStateDelegates.HasSavedStateHandle {

    private val _ingredients = MediatorLiveData<List<IngredientModel>>()
    val ingredients: LiveData<List<IngredientModel>> = _ingredients

    private val _toolbarModel = MediatorLiveData(ToolbarModel())
    val toolbarModel: LiveData<ToolbarModel> = _toolbarModel

    private val _currentServingModel = MediatorLiveData(CurrentServingModel())
    val currentServingModel: LiveData<CurrentServingModel> = _currentServingModel

    private val _navigationEvents = LiveEvent<NavigationEvent>()
    val navigationEvents: LiveData<NavigationEvent> = _navigationEvents

    private val adjustment = state.getLiveData<AdjustmentModel>(KEY_ADJUSTMENT)
    private val checkedItems: MutableMap<Int, Boolean> by SavedStateDelegates.state(KEY_CHECKED_ITEMS, mutableMapOf())

    private val recipe = MutableLiveData<FullRecipe>()

    init {
        _currentServingModel.addSource(recipe) { updateCurrentServing() }
        _currentServingModel.addSource(adjustment) { updateCurrentServing() }
        _ingredients.addSource(recipe) { updateIngredients() }
        _ingredients.addSource(adjustment) { updateIngredients() }

        state.get<Long>(KEY_RECIPE_ID)?.let {
            init(RecipeId(it))
        }
    }

    fun init(id: RecipeId) = viewModelScope.launch {
        L.debug { "init(): id=$id" }
        state.set(KEY_RECIPE_ID, id.value)

        getFullRecipeUseCase(id)?.also { res ->
            recipe.value = res
            adjustment.value = adjustment.value ?: res.adjustment.toAdjustmentModel()

            _toolbarModel.update { it.copy(title = res.recipe.title) }
        }
    }

    private fun updateIngredients() {
        val ingredients = recipe.value?.ingredients ?: return
        val adjustment = adjustment.value ?: return
        val serving = getCurrentServing() ?: return

        val scaleFactor = serving.scaleFactor(adjustment.newValue)

        _ingredients.value = ingredients.mapIndexed { index, ingredient ->
            IngredientModel.from(
                index,
                checkedItems[index] == true,
                ingredient * scaleFactor,
                AMOUNT_VALUE_FORMAT::format
            )
        }
    }

    private fun updateCurrentServing() {
        val adjustment = adjustment.value ?: return
        val serving = getCurrentServing() ?: return

        val scaleFactor = serving.scaleFactor(adjustment.newValue)
        val resetButtonVisible = scaleFactor != 1f
        val value = SCALE_FACTOR_FORMAT.format(adjustment.newValue)
        val unit = serving.unit

        _currentServingModel.value = when (serving) {
            is Serving.Custom -> CurrentServingModel(true, "$value $unit", resetButtonVisible)
            is Serving.RingDiameter -> CurrentServingModel(true, "$DIAMETER_SIGN $value $unit", resetButtonVisible)
        }
    }

    fun onItemClicked(model: IngredientModel) {
        L.debug { "onItemClicked(): $model" }
        checkedItems[model.id] = !model.isChecked
        updateIngredients()
    }

    fun onAmountValueChanged(model: IngredientModel.WithValueAndUnit, newValue: String) {
        L.debug { "onAmountValueChanged(): model=$model, newValue: newValue=$newValue" }
        val ingredients = recipe.value?.ingredients ?: return
        val value = newValue.toFloatOrNull() ?: return
        // TODO: feature/last_serving 12/7/20 ingredient has its own database ids, use them instead of indexes
        val ingredient = ingredients.getOrNull(model.id) ?: return // model.id is zero-based index
        val newScaleFactor = ingredient.amount.scaleFactor(value)

        if (!newScaleFactor.isNaN()) {
            adjustment.value = Adjustment.fromScaleFactor(newScaleFactor).toAdjustmentModel()
        }
    }

    fun onResetScaleFactor() {
        L.debug { "onResetScaleFactor()" }
        val recipe = recipe.value ?: return
        adjustment.value = Adjustment.fromServing(recipe.serving).toAdjustmentModel()
    }

    fun onScaleFactorClick() {
        L.debug { "onScaleFactorClick()" }
        val adjustment = adjustment.value ?: return
        val serving = getCurrentServing() ?: return

        val value = SCALE_FACTOR_FORMAT.format(adjustment.newValue)
        val unit = serving.unit

        _navigationEvents.value = ShowCustomServingDialog(
            when (serving) {
                is Serving.Custom -> CustomServingModel(suffix = unit, value = value)
                is Serving.RingDiameter -> CustomServingModel(prefix = "$DIAMETER_SIGN ", suffix = unit, value = value)
            }
        )
    }

    fun onCustomServingSelected(newValue: String) {
        L.debug { "onCustomServingSelected(): $newValue" }
        val value = newValue.toFloatOrNull() ?: return
        adjustment.update { it.copy(newValue = value) }
    }

    fun onBackPressed() {
        saveAdjustmentAndExit()
    }

    fun onUpPressed() {
        saveAdjustmentAndExit()
    }

    private fun saveAdjustmentAndExit() {
        val recipe = recipe.value ?: return
        val adjustment = adjustment.value ?: return

        viewModelScope.launch {
            updateAdjustmentUseCase(recipe.recipe.id, adjustment.toAdjustment())
            _navigationEvents.value = Exit
        }
    }

    private fun getCurrentServing(): Serving? {
        val adjustment = adjustment.value ?: return null
        val recipe = recipe.value ?: return null

        return when (adjustment.type) {
            AdjustmentType.BY_AMOUNT -> Serving.NONE
            AdjustmentType.BY_SERVING -> recipe.serving
        }
    }

    companion object {
        private val L = Logger()

        const val KEY_RECIPE_ID = "recipe_id"
        const val KEY_CHECKED_ITEMS = "checked_items"
        const val KEY_ADJUSTMENT = "adjustment"
        const val DIAMETER_SIGN = "\u2300"

        val SCALE_FACTOR_FORMAT: NumberFormat = DecimalFormat.getInstance().apply {
            maximumFractionDigits = 2
        }
        val AMOUNT_VALUE_FORMAT: NumberFormat = DecimalFormat.getInstance().apply {
            maximumFractionDigits = 1
        }
    }
}
