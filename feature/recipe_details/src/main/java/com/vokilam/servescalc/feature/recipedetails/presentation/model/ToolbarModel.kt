package com.vokilam.servescalc.feature.recipedetails.presentation.model

data class ToolbarModel(
    val title: String = ""
)