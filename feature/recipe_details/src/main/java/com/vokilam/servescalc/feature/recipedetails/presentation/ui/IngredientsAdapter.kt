package com.vokilam.servescalc.feature.recipedetails.presentation.ui

import android.graphics.Rect
import android.view.KeyEvent
import android.view.TouchDelegate
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.recyclerview.widget.ListAdapter
import com.vokilam.servescalc.core.presentation.ItemInteractionListener
import com.vokilam.servescalc.core.presentation.hideKeyboard
import com.vokilam.servescalc.core.presentation.layoutInflater
import com.vokilam.servescalc.core.presentation.toPx
import com.vokilam.servescalc.core.presentation.ui.adapter.BaseViewHolder
import com.vokilam.servescalc.feature.recipedetails.BR
import com.vokilam.servescalc.feature.recipedetails.databinding.ItemIngredientEmptyBinding
import com.vokilam.servescalc.feature.recipedetails.databinding.ItemIngredientUnitBinding
import com.vokilam.servescalc.feature.recipedetails.databinding.ItemIngredientValueUnitBinding
import com.vokilam.servescalc.feature.recipedetails.presentation.model.IngredientModel
import com.vokilam.servescalc.feature.recipedetails.presentation.model.IngredientModel.*
import com.vokilam.servescalc.feature.recipedetails.presentation.model.IngredientModelDiff
import com.vokilam.servescalc.feature.recipedetails.presentation.ui.widget.KeyPreImeAwareEditText

class IngredientsAdapter : ListAdapter<IngredientModel, BaseViewHolder<IngredientModel>>(IngredientModelDiff) {

    private var onItemClickListener: ItemInteractionListener<IngredientModel>? = null
    private var onAmountValueChangeListener: ((WithValueAndUnit, String) -> Unit)? = null

    fun setItemInteractionListeners(
        onItemClick: ItemInteractionListener<IngredientModel>,
        onAmountValueChange: (model: WithValueAndUnit, newValue: String) -> Unit
    ) {
        onItemClickListener = onItemClick
        onAmountValueChangeListener = onAmountValueChange
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is WithValueAndUnit -> ItemType.VALUE_UNIT
            is WithUnit -> ItemType.UNIT
            is Empty -> ItemType.EMPTY
            else -> throw IllegalStateException()
        }.viewType
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<IngredientModel> {
        @Suppress("UNCHECKED_CAST")
        return when (ItemType.from(viewType)) {
            ItemType.VALUE_UNIT -> ItemIngredientValueUnitBinding.inflate(parent.layoutInflater, parent, false)
                .let(::IngredientWithValueAndUnitViewHolder)
            ItemType.UNIT -> ItemIngredientUnitBinding.inflate(parent.layoutInflater, parent, false)
                .let(::IngredientWithUnitViewHolder)
            ItemType.EMPTY -> ItemIngredientEmptyBinding.inflate(parent.layoutInflater, parent, false)
                .let(::EmptyIngredientViewHolder)
        } as BaseViewHolder<IngredientModel>
    }

    override fun onBindViewHolder(holder: BaseViewHolder<IngredientModel>, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onViewDetachedFromWindow(holder: BaseViewHolder<IngredientModel>) {
        when (ItemType.values()[holder.itemViewType]) {
            ItemType.VALUE_UNIT -> {
                holder as IngredientWithValueAndUnitViewHolder
                holder.detach()
            }
            else -> {}
        }
    }

    inner class IngredientWithValueAndUnitViewHolder(
        private val binding: ItemIngredientValueUnitBinding
    ) : BaseViewHolder<WithValueAndUnit>(binding.root) {

        init {
            itemView.setOnClickListener {
                binding.model?.let {
                    onItemClickListener?.invoke(it)
                }
            }

            binding.amountValue.run {
                setOnEditorActionListener { v, actionId, _ ->
                    when (actionId) {
                        EditorInfo.IME_ACTION_DONE -> {
                            v.hideKeyboard(true)
                            notifyOnAmountValueChange()
                            true
                        }
                        else -> false
                    }
                }

                setOnKeyPreImeListener { v, keyCode, _ ->
                    when (keyCode) {
                        KeyEvent.KEYCODE_BACK -> {
                            v.clearFocus()
                            notifyOnAmountValueChange()
                            false
                        }
                        else -> false
                    }
                }
            }
        }

        override fun bind(model: WithValueAndUnit) {
            binding.run {
                this.model = model
                binding.root.isActivated = model.isChecked
                executePendingBindings()
            }

            ensureTouchableWidth(binding.amountValue)
        }

        private fun ensureTouchableWidth(child: KeyPreImeAwareEditText) {
            val minTouchableWidth = 48.toPx()
            val parent = child.parent as View

            parent.post {
                val rect = Rect().also(child::getHitRect)
                val delta = minTouchableWidth - rect.width()

                if (delta > 0) {
                    rect.run {
                        left -= delta / 2
                        right += delta / 2
                    }

                    parent.touchDelegate = TouchDelegate(rect, child)
                } else {
                    parent.touchDelegate = null
                }
            }
        }

        fun detach() {
            binding.amountValue.also {
                if (it.hasFocus()) {
                    it.hideKeyboard(true)
                    notifyOnAmountValueChange()
                }
            }
        }

        private fun notifyOnAmountValueChange() {
            binding.model?.let {
                onAmountValueChangeListener?.invoke(it, binding.amountValue.text.toString())
            }
        }
    }

    inner class IngredientWithUnitViewHolder(
        private val binding: ItemIngredientUnitBinding
    ) : BaseViewHolder<WithUnit>(binding.root) {

        init {
            itemView.setOnClickListener {
                binding.model?.let {
                    onItemClickListener?.invoke(it)
                }
            }
        }

        override fun bind(model: WithUnit) {
            binding.run {
                binding.setVariable(BR.model, model)
                binding.root.isActivated = model.isChecked
                executePendingBindings()
            }
        }
    }

    // TODO docs: 12/25/19 merge with IngredientWithUnitViewHolder
    inner class EmptyIngredientViewHolder(
        private val binding: ItemIngredientEmptyBinding
    ) : BaseViewHolder<Empty>(binding.root) {

        init {
            itemView.setOnClickListener {
                binding.model?.let {
                    onItemClickListener?.invoke(it)
                }
            }
        }

        override fun bind(model: Empty) {
            binding.run {
                binding.setVariable(BR.model, model)
                binding.root.isActivated = model.isChecked
                executePendingBindings()
            }
        }
    }

    private enum class ItemType {
        VALUE_UNIT,
        UNIT,
        EMPTY;

        val viewType: Int = ordinal

        companion object {
            fun from(viewType: Int): ItemType = values()[viewType]
        }
    }
}
