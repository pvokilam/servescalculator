package com.vokilam.servescalc.feature.recipedetails.presentation.ui

import android.app.Dialog
import android.os.Bundle
import android.view.WindowManager
import android.widget.EditText
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputLayout
import com.vokilam.servescalc.core.presentation.hideKeyboard
import com.vokilam.servescalc.feature.recipedetails.R

class CustomServingDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = requireActivity().layoutInflater.inflate(R.layout.dialog_custom_serving, null)
        val editText = view.findViewById<EditText>(R.id.customServing)

        editText.setText(requireArguments().getString(KEY_VALUE))
        // https://stackoverflow.com/a/31551639/317928
        editText.setSelectAllOnFocus(true)
        editText.requestFocus()

        view.findViewById<TextInputLayout>(R.id.customServingLayout).run {
            prefixText = requireArguments().getString(KEY_PREFIX)
            suffixText = requireArguments().getString(KEY_SUFFIX)
        }

        return MaterialAlertDialogBuilder(requireContext())
            .setView(view)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                editText.hideKeyboard()
                setFragmentResult(REQUEST_KEY, bundleOf(KEY_CUSTOM_SERVING to editText.text.toString()))
            }
            .setNegativeButton(android.R.string.cancel) { _, _ ->
                editText.hideKeyboard()
            }
            .create()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        isCancelable = false // TODO: 12/6/19 hide keyboard!
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
    }

    companion object {
        const val REQUEST_KEY = "CustomServingDialogFragment"
        const val KEY_CUSTOM_SERVING = "custom_serving"

        fun newInstance(prefixText: String, suffixText: String, value: String): CustomServingDialogFragment {
            return CustomServingDialogFragment().apply {
                arguments = bundleOf(
                    KEY_PREFIX to prefixText,
                    KEY_SUFFIX to suffixText,
                    KEY_VALUE to value
                )
            }
        }

        private const val KEY_PREFIX = "prefix"
        private const val KEY_SUFFIX = "suffix"
        private const val KEY_VALUE = "value"
    }
}