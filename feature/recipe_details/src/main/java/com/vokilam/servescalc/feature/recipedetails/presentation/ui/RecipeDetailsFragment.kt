package com.vokilam.servescalc.feature.recipedetails.presentation.ui

import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.android.material.chip.Chip
import com.vokilam.logger.Logger
import com.vokilam.servescalc.core.presentation.util.viewBinding
import com.vokilam.servescalc.domain.model.RecipeId
import com.vokilam.servescalc.feature.recipedetails.R
import com.vokilam.servescalc.feature.recipedetails.databinding.RecipeDetailsFragmentBinding
import com.vokilam.servescalc.feature.recipedetails.presentation.model.CurrentServingModel
import com.vokilam.servescalc.feature.recipedetails.presentation.model.IngredientModel
import com.vokilam.servescalc.feature.recipedetails.presentation.model.ToolbarModel
import com.vokilam.servescalc.feature.recipedetails.presentation.ui.NavigationEvent.ShowCustomServingDialog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RecipeDetailsFragment : Fragment(R.layout.recipe_details_fragment) {

    private val viewModel by viewModels<RecipeDetailsViewModel>()
    private val args by navArgs<RecipeDetailsFragmentArgs>()
    private val binding by viewBinding(RecipeDetailsFragmentBinding::bind)

    private lateinit var adapter: IngredientsAdapter
    private lateinit var chip: Chip
    private lateinit var onBackPressedCallback: OnBackPressedCallback

    override fun onAttach(context: Context) {
        super.onAttach(context)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()

        onBackPressedCallback = requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            viewModel.onBackPressed()
        }

        viewModel.run {
            toolbarModel.observe(viewLifecycleOwner, ::updateToolbar)
            currentServingModel.observe(viewLifecycleOwner, ::updateCurrentServing)
            ingredients.observe(viewLifecycleOwner, ::updateIngredients)
            navigationEvents.observe(viewLifecycleOwner, ::handleNavigation)
        }

        if (savedInstanceState == null) viewModel.init(RecipeId(args.recipeId))
    }

    private fun setupRecyclerView() {
        adapter = IngredientsAdapter().apply {
            setItemInteractionListeners(
                viewModel::onItemClicked,
                viewModel::onAmountValueChanged
            )
        }

        binding.recyclerView.apply {
            setHasFixedSize(true)
            adapter = this@RecipeDetailsFragment.adapter
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }
    }

    private fun updateToolbar(model: ToolbarModel) {
        L.verb { "updateToolbar(): model=$model" }
        val toolbar = requireNotNull((activity as AppCompatActivity).supportActionBar)
        toolbar.title = model.title
    }

    private fun updateCurrentServing(model: CurrentServingModel) {
        L.verb { "updateCurrentServing(): model=$model" }
        if (!::chip.isInitialized) return

        chip.run {
            isVisible = model.visible
            text = model.formattedValue
            isCloseIconVisible = model.resetButtonVisible
        }
    }

    private fun updateIngredients(items: List<IngredientModel>) {
        L.verb { "update(): items=$items" }
        adapter.submitList(items)
    }

    private fun handleNavigation(event: NavigationEvent) {
        L.verb { "handleNavigation(): event=$event" }
        when (event) {
            is ShowCustomServingDialog -> {
                CustomServingDialogFragment.newInstance(event.model.prefix, event.model.suffix, event.model.value)
                    .show(childFragmentManager, "CustomServingDialogFragment")

                childFragmentManager.setFragmentResultListener(
                    CustomServingDialogFragment.REQUEST_KEY,
                    viewLifecycleOwner
                ) { _, bundle ->
                    bundle.getString(CustomServingDialogFragment.KEY_CUSTOM_SERVING)?.let {
                        viewModel.onCustomServingSelected(it)
                    }
                }
            }
            is NavigationEvent.Exit -> {
                findNavController().popBackStack()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_recipe_details, menu)

        val chipMenuItem = menu.findItem(R.id.menu_scale_factor)

        chip = requireNotNull(chipMenuItem.actionView.findViewById<Chip>(R.id.chip)).apply {
            setOnCloseIconClickListener { viewModel.onResetScaleFactor() }
            setOnClickListener { viewModel.onScaleFactorClick() }
        }

        viewModel.currentServingModel.value?.let { updateCurrentServing(it) }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            android.R.id.home -> { viewModel.onUpPressed(); true }
            else -> super.onOptionsItemSelected(item)
        }
    }
    
    companion object {
        private val L = Logger()
    }
}
