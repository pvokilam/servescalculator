package com.vokilam.servescalc.feature.recipedetails.presentation.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.View
import com.google.android.material.textfield.TextInputEditText

class KeyPreImeAwareEditText : TextInputEditText {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    private var onKeyPreImeListener: ((v: View, keyCode: Int, event: KeyEvent?) -> Boolean)? = null

    fun setOnKeyPreImeListener(listener: (v: View, keyCode: Int, event: KeyEvent?) -> Boolean) {
        onKeyPreImeListener = listener
    }

    override fun onKeyPreIme(keyCode: Int, event: KeyEvent?): Boolean {
        return onKeyPreImeListener.let {
            if (it == null || !it(this, keyCode, event)) {
                super.onKeyPreIme(keyCode, event)
            } else {
                true
            }
        }
    }
}
