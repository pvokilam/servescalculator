package com.vokilam.servescalc.feature.recipedetails.presentation.ui

import com.vokilam.servescalc.feature.recipedetails.presentation.model.CustomServingModel

sealed class NavigationEvent {
    data class ShowCustomServingDialog(val model: CustomServingModel) : NavigationEvent()
    object Exit : NavigationEvent()
}