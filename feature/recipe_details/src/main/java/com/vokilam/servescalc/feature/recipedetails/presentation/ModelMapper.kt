package com.vokilam.servescalc.feature.recipedetails.presentation

import com.vokilam.servescalc.domain.model.Adjustment
import com.vokilam.servescalc.feature.recipedetails.presentation.model.AdjustmentModel

internal fun Adjustment.toAdjustmentModel(): AdjustmentModel {
    return AdjustmentModel(newValue, type)
}

internal fun AdjustmentModel.toAdjustment(): Adjustment {
    return Adjustment(newValue, type)
}