package com.vokilam.servescalc.feature.recipedetails.presentation.model

data class CurrentServingModel(
    val visible: Boolean = false,
    val formattedValue: String = "",
    val resetButtonVisible: Boolean = false
)

data class CustomServingModel(
    val suffix: String = "",
    val prefix: String = "",
    val value: String = ""
)