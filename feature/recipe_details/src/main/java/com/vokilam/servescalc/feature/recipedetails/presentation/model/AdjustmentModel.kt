package com.vokilam.servescalc.feature.recipedetails.presentation.model

import android.os.Parcelable
import com.vokilam.servescalc.domain.model.AdjustmentType
import kotlinx.parcelize.Parcelize

@Parcelize
data class AdjustmentModel(val newValue: Float, val type: AdjustmentType) : Parcelable
