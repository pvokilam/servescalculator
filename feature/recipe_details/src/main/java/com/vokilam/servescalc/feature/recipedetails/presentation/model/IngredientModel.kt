package com.vokilam.servescalc.feature.recipedetails.presentation.model

import androidx.recyclerview.widget.DiffUtil
import com.vokilam.servescalc.domain.model.Amount
import com.vokilam.servescalc.domain.model.Ingredient

sealed class IngredientModel {
    abstract val id: Int
    abstract val isChecked: Boolean

    data class WithValueAndUnit(
        override val id: Int,
        override val isChecked: Boolean,
        val name: String,
        val formattedValue: String = "",
        val unit: String = ""
    ) : IngredientModel()

    data class WithUnit(
        override val id: Int,
        override val isChecked: Boolean,
        val name: String,
        val unit: String = ""
    ) : IngredientModel()

    data class Empty(
        override val id: Int,
        override val isChecked: Boolean,
        val name: String
    ) : IngredientModel()

    companion object Mapper {
        fun from(id: Int, isChecked: Boolean, item: Ingredient, formatter: (Float) -> String = Float::toString): IngredientModel {
            return when (val amount = item.amount) {
                is Amount.Empty -> Empty(
                    id = id,
                    isChecked = isChecked,
                    name = item.name
                )
                is Amount.Measurable -> WithValueAndUnit(
                    id = id,
                    isChecked = isChecked,
                    name = item.name,
                    formattedValue = formatter(amount.value),
                    unit = amount.unit
                )
                is Amount.Unmeasurable -> WithUnit(
                    id = id,
                    isChecked = isChecked,
                    name = item.name,
                    unit = amount.unit
                )
                is Amount.Countable -> WithValueAndUnit(
                    id = id,
                    isChecked = isChecked,
                    name = item.name,
                    formattedValue = formatter(amount.value),
                    unit = amount.unit
                )
            }
        }
    }
}

object IngredientModelDiff : DiffUtil.ItemCallback<IngredientModel>() {
    override fun areItemsTheSame(oldItem: IngredientModel, newItem: IngredientModel): Boolean = oldItem.id == newItem.id
    override fun areContentsTheSame(oldItem: IngredientModel, newItem: IngredientModel): Boolean = oldItem == newItem
}
