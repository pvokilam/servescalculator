package com.vokilam.servescalc.feature.recipelist.presentation.ui

import androidx.arch.core.executor.ArchTaskExecutor
import androidx.lifecycle.SavedStateHandle
import com.vokilam.core.test.TestData
import com.vokilam.core.test.util.TestTaskExecutor
import com.vokilam.core.test.util.getOrAwaitValue
import com.vokilam.core.test.util.observeForTesting
import com.vokilam.servescalc.core.domain.usecase.DeleteRecipeUseCase
import com.vokilam.servescalc.core.domain.usecase.GetRecipesUseCase
import com.vokilam.servescalc.domain.model.Recipe
import com.vokilam.servescalc.domain.model.RecipeListSortOrder
import com.vokilam.servescalc.domain.settings.AppSettings
import com.vokilam.servescalc.feature.recipelist.presentation.ui.NavigationEvent.*
import io.mockk.Runs
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.coVerifyOrder
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.verify
import io.mockk.verifyOrder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.spekframework.spek2.Spek
import org.spekframework.spek2.lifecycle.CachingMode
import org.spekframework.spek2.lifecycle.CachingMode.SCOPE
import org.spekframework.spek2.style.specification.describe

class RecipeListViewModelSpek : Spek({
    val testDispatcher = TestCoroutineDispatcher()

    beforeEachTest {
        ArchTaskExecutor.getInstance().setDelegate(TestTaskExecutor())
        Dispatchers.setMain(testDispatcher)
    }

    afterEachTest {
        ArchTaskExecutor.getInstance().setDelegate(null)
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    val getRecipesUseCase = mockk<GetRecipesUseCase>()
    val deleteRecipeUseCase = mockk<DeleteRecipeUseCase>()
    val appSettings = mockk<AppSettings>()
    val state = SavedStateHandle()

    val viewModel by memoized {
        RecipeListViewModel(
            getRecipesUseCase,
            deleteRecipeUseCase,
            appSettings,
            state
        )
    }

    beforeEachTest {
        every { appSettings.recipeListSortOrder } returns RecipeListSortOrder.RECENT
    }

    afterEachTest { clearAllMocks() }

    describe("recipe list") {
        context("when empty") {
            beforeEachTest {
                every { getRecipesUseCase() } returns flowOf(emptyList())
                viewModel.loadRecipes()
            }

            it("should return empty list") {
                val actual = viewModel.recipes.getOrAwaitValue()
                assertEquals(emptyList<Recipe>(), actual)
            }

            it("should show empty view") {
                val actual = viewModel.recipeListModel.getOrAwaitValue()
                assertTrue(actual.emptyViewVisible)
            }

            it("should hide sorting action") {
                val actual = viewModel.toolbarModel.getOrAwaitValue()
                assertFalse(actual.sortOrderActionVisible)
            }
        }

        context("when is not empty") {
            val recipes by memoized(SCOPE) { TestData.recipes }

            beforeEachTest {
                every { getRecipesUseCase() } returns flowOf(recipes)
                viewModel.loadRecipes()
            }

            it("should return recipe list") {
                val actual = viewModel.recipes.getOrAwaitValue()
                assertEquals(recipes, actual)
            }

            it("should hide empty view") {
                val actual = viewModel.recipeListModel.getOrAwaitValue()
                assertFalse(actual.emptyViewVisible)
            }

            it("should show sorting action") {
                val actual = viewModel.toolbarModel.getOrAwaitValue()
                assertTrue(actual.sortOrderActionVisible)
            }
        }
    }

    describe("item interaction") {
        val recipe = TestData.singleRecipe

        context("on click") {
            beforeEachTest {
                viewModel.onItemClick(recipe)
            }

            it("should trigger navigation event") {
                assertEquals(OpenRecipeDetails(recipe), viewModel.navigationEvents.value)
            }
        }

        context("on delete") {
            beforeEachTest {
                coEvery { deleteRecipeUseCase(recipe.id) } returns true
                viewModel.onItemDelete(recipe)
            }

            it("should delete recipe from storage") {
                coVerify {
                    deleteRecipeUseCase(recipe.id)
                }
            }
        }
    }

    describe("sorting order") {
        it("should return current order") {
            assertEquals(RecipeListSortOrder.RECENT, viewModel.toolbarModel.getOrAwaitValue().selectedSortOrder)
        }

        context("when sorting order changed") {
            val order = RecipeListSortOrder.RECENT

            beforeEachTest {
                every { appSettings.recipeListSortOrder = order } just Runs
                every { getRecipesUseCase() } returns flowOf(TestData.recipes)
                viewModel.onSortingOrderChanged(order)
            }

            it("should save new order and update recipe list") {
                verifyOrder {
                    appSettings.recipeListSortOrder = order
                    getRecipesUseCase()
                }
            }

            it("should update selected sort order") {
                assertEquals(order, viewModel.toolbarModel.getOrAwaitValue().selectedSortOrder)
            }
        }
    }

    describe("add recipe") {
        context("on click") {
            beforeEachTest {
                viewModel.onAddRecipe()
            }

            it("should navigate to add recipe screen") {
                assertEquals(OpenAddRecipe, viewModel.navigationEvents.value)
            }
        }
    }
})