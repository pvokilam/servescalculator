package com.vokilam.servescalc.feature.recipelist.presentation.ui

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vokilam.logger.Logger
import com.vokilam.servescalc.core.domain.usecase.DeleteRecipeUseCase
import com.vokilam.servescalc.core.domain.usecase.GetRecipesUseCase
import com.vokilam.servescalc.core.presentation.MediatorLiveData
import com.vokilam.servescalc.core.presentation.update
import com.vokilam.servescalc.core.presentation.util.LiveEvent
import com.vokilam.servescalc.domain.model.Recipe
import com.vokilam.servescalc.domain.model.RecipeListSortOrder
import com.vokilam.servescalc.domain.settings.AppSettings
import com.vokilam.servescalc.feature.recipelist.presentation.model.RecipeListModel
import com.vokilam.servescalc.feature.recipelist.presentation.model.ToolbarModel
import com.vokilam.servescalc.feature.recipelist.presentation.ui.NavigationEvent.OpenAddRecipe
import com.vokilam.servescalc.feature.recipelist.presentation.ui.NavigationEvent.OpenRecipeDetails
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.launch

class RecipeListViewModel @ViewModelInject constructor(
    private val getRecipesUseCase: GetRecipesUseCase,
    private val deleteRecipeUseCase: DeleteRecipeUseCase,
    private val appSettings: AppSettings,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _recipes = MutableLiveData<List<Recipe>>()
    val recipes: LiveData<List<Recipe>> = _recipes

    private val _toolbarModel = MediatorLiveData(ToolbarModel())
    val toolbarModel: LiveData<ToolbarModel> = _toolbarModel

    private val _recipeListModel = MediatorLiveData(RecipeListModel())
    val recipeListModel: LiveData<RecipeListModel> = _recipeListModel

    private val _navigationEvents = LiveEvent<NavigationEvent>()
    val navigationEvents: LiveData<NavigationEvent> = _navigationEvents

    private var loadRecipesJob: Job? = null

    init {
        _toolbarModel.update { it.copy(selectedSortOrder = appSettings.recipeListSortOrder) }
        _toolbarModel.addSource(_recipes) { list ->
            _toolbarModel.update { it.copy(sortOrderActionVisible = list.isNotEmpty()) }
        }
        _recipeListModel.addSource(_recipes) { list ->
            _recipeListModel.update { it.copy(emptyViewVisible = list.isEmpty()) }
        }
    }

    fun loadRecipes() {
        L.debug { "loadRecipes()" }
        loadRecipesJob?.cancel()
        loadRecipesJob = viewModelScope.launch {
            getRecipesUseCase()
                .onCompletion { L.verb { "loadRecipes(): completed, exception=$it" } }
                .collect { _recipes.value = it }
        }
    }

    fun onItemClick(item: Recipe) {
        L.debug { "onItemClick(): item=$item" }
        _navigationEvents.value = OpenRecipeDetails(item)
    }

    fun onAddRecipe() {
        L.debug { "onAddRecipe()" }
        _navigationEvents.value = OpenAddRecipe
    }

    fun onItemDelete(recipe: Recipe) {
        L.debug { "onItemDelete(): $recipe" }
        viewModelScope.launch {
            // list is updated automatically
            deleteRecipeUseCase(recipe.id)
        }
    }

    fun onSortingOrderChanged(order: RecipeListSortOrder) {
        L.debug { "onSortingOrderChanged(): order=$order" }
        appSettings.recipeListSortOrder = order
        _toolbarModel.update { it.copy(selectedSortOrder = order) }
        loadRecipes()
    }

    companion object {
        private val L = Logger()
    }
}
