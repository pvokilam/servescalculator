package com.vokilam.servescalc.feature.recipelist.presentation.model

import androidx.recyclerview.widget.DiffUtil
import com.vokilam.servescalc.domain.model.Recipe

object RecipeDiff : DiffUtil.ItemCallback<Recipe>() {
    override fun areItemsTheSame(oldItem: Recipe, newItem: Recipe): Boolean = oldItem.id == newItem.id
    override fun areContentsTheSame(oldItem: Recipe, newItem: Recipe): Boolean = oldItem == newItem
}