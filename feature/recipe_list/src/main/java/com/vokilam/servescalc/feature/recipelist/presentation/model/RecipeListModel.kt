package com.vokilam.servescalc.feature.recipelist.presentation.model

data class RecipeListModel(
    val emptyViewVisible: Boolean = false
)