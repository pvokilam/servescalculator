package com.vokilam.servescalc.feature.recipelist.presentation.model

import com.vokilam.servescalc.domain.model.RecipeListSortOrder

data class ToolbarModel(
    val sortOrderActionVisible: Boolean = false,
    val selectedSortOrder: RecipeListSortOrder = RecipeListSortOrder.DEFAULT
)