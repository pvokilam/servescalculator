package com.vokilam.servescalc.feature.recipelist.presentation.ui

import com.vokilam.servescalc.domain.model.Recipe

sealed class NavigationEvent {
    object OpenAddRecipe : NavigationEvent()
    data class OpenRecipeDetails(val recipe: Recipe) : NavigationEvent()
}
