package com.vokilam.servescalc.feature.recipelist.presentation.ui

import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.vokilam.servescalc.core.presentation.ItemInteractionListener
import com.vokilam.servescalc.domain.model.Recipe
import com.vokilam.servescalc.feature.recipelist.R
import com.vokilam.servescalc.feature.recipelist.databinding.ItemRecipeBinding
import com.vokilam.servescalc.feature.recipelist.presentation.model.RecipeDiff

class RecipeListAdapter : ListAdapter<Recipe, RecipeListAdapter.RecipeViewHolder>(RecipeDiff) {

    private var onItemClickListener: ItemInteractionListener<Recipe>? = null
    private var onItemDeleteListener: ItemInteractionListener<Recipe>? = null

    fun setOnItemInteractionListeners(
        onClick: ItemInteractionListener<Recipe>,
        onDelete: ItemInteractionListener<Recipe>
    ) {
        onItemClickListener = onClick
        onItemDeleteListener = onDelete
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder {
        return ItemRecipeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            .let(::RecipeViewHolder)
    }

    override fun onBindViewHolder(holder: RecipeViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    interface RecipeItemInteractionListener {
        fun onClick(item: Recipe)
    }

    inner class RecipeViewHolder(
        private val binding: ItemRecipeBinding
    ) : RecyclerView.ViewHolder(binding.root), RecipeItemInteractionListener, View.OnCreateContextMenuListener {

        init {
            itemView.setOnCreateContextMenuListener(this)
        }

        fun bind(model: Recipe) {
            binding.run {
                this.model = model
                listener = this@RecipeViewHolder
                executePendingBindings()
            }
        }

        override fun onClick(item: Recipe) {
            onItemClickListener?.invoke(item)
        }

        override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenu.ContextMenuInfo?) {
            menu.add(0, 0, 0, R.string.menu_delete)
                .setOnMenuItemClickListener {
                    binding.model?.let { onItemDeleteListener?.invoke(it) }
                    true
                }
        }
    }
}
