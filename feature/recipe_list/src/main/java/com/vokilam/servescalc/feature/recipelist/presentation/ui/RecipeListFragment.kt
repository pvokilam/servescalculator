package com.vokilam.servescalc.feature.recipelist.presentation.ui

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.facebook.drawee.backends.pipeline.Fresco
import com.vokilam.logger.Logger
import com.vokilam.servescalc.core.presentation.util.viewBinding
import com.vokilam.servescalc.domain.model.Recipe
import com.vokilam.servescalc.domain.model.RecipeListSortOrder.*
import com.vokilam.servescalc.feature.recipelist.R
import com.vokilam.servescalc.feature.recipelist.databinding.RecipeListFragmentBinding
import com.vokilam.servescalc.feature.recipelist.presentation.model.RecipeListModel
import com.vokilam.servescalc.feature.recipelist.presentation.model.ToolbarModel
import com.vokilam.servescalc.feature.recipelist.presentation.ui.RecipeListFragmentDirections.Companion.actionRecipeListToAddRecipe
import com.vokilam.servescalc.feature.recipelist.presentation.ui.RecipeListFragmentDirections.Companion.actionRecipeListToRecipeDetails
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RecipeListFragment : Fragment(R.layout.recipe_list_fragment) {

    private val viewModel by viewModels<RecipeListViewModel>()
    private val binding by viewBinding(RecipeListFragmentBinding::bind)

    private lateinit var adapter: RecipeListAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupRecyclerView()
        setupFab()
        setupEmptyView()

        viewModel.run {
            toolbarModel.observe(viewLifecycleOwner, ::updateToolbar)
            recipes.observe(viewLifecycleOwner, ::updateRecipes)
            recipeListModel.observe(viewLifecycleOwner, ::updateRecipeList)
            navigationEvents.observe(viewLifecycleOwner, ::handleNavigation)
            loadRecipes() // TODO: feature/mvvm 11/21/20 add saved state handler and call only once
        }
    }

    private fun setupToolbar() {
        val toolbar = requireNotNull((activity as AppCompatActivity).supportActionBar)
        toolbar.setTitle(R.string.title_recipe_list)
    }

    private fun setupRecyclerView() {
        adapter = RecipeListAdapter().apply {
            setOnItemInteractionListeners(
                viewModel::onItemClick,
                viewModel::onItemDelete
            )
        }

        binding.recyclerView.apply {
            setHasFixedSize(true)
            adapter = this@RecipeListFragment.adapter
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }
    }

    private fun setupFab() {
        binding.fab.setOnClickListener { viewModel.onAddRecipe() }
    }

    private fun setupEmptyView() {
        binding.emptyView.animationView.controller = Fresco.newDraweeControllerBuilder()
            .setUri(Uri.parse("asset:///animations/ftue_share_text_anim.webp"))
            .setAutoPlayAnimations(true) // TODO: feature/mvvm 11/17/20 check animations are paused when view is not visible
            .build()
    }

    private fun updateToolbar(model: ToolbarModel) {
        L.verb { "updateToolbar(): model=$model" }
        activity?.invalidateOptionsMenu()
    }

    private fun updateRecipes(items: List<Recipe>) {
        L.verb { "update(): items=$items" }
        adapter.submitList(items)
    }

    private fun updateRecipeList(model: RecipeListModel) {
        L.verb { "update(): model=$model" }
        binding.viewSwitcher.displayedChild = if (model.emptyViewVisible) 1 else 0
    }

    private fun handleNavigation(event: NavigationEvent) {
        L.verb { "handleNavigation(): event=$event" }
        when (event) {
            NavigationEvent.OpenAddRecipe -> {
                findNavController().navigate(actionRecipeListToAddRecipe(null))
            }
            is NavigationEvent.OpenRecipeDetails -> {
                findNavController().navigate(actionRecipeListToRecipeDetails(event.recipe.id.value))
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.recipe_list, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        // TODO: feature/mvvm 11/19/20 simplify this
        val model = viewModel.toolbarModel.value ?: return

        menu.findItem(R.id.menu_sort)?.run {
            isVisible = model.sortOrderActionVisible

            val selectedItemId = when (model.selectedSortOrder) {
                A_Z -> R.id.menu_sort_az
                RECENT -> R.id.menu_sort_recent
            }
            menu.findItem(selectedItemId)?.isChecked = true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_sort_az -> {
                viewModel.onSortingOrderChanged(A_Z)
                true
            }
            R.id.menu_sort_recent -> {
                viewModel.onSortingOrderChanged(RECENT)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        private val L = Logger()
    }
}

