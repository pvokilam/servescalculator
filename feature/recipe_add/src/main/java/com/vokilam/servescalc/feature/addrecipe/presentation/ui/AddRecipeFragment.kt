package com.vokilam.servescalc.feature.addrecipe.presentation.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.activity.result.contract.ActivityResultContract
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import com.vokilam.logger.Logger
import com.vokilam.servescalc.core.presentation.ui.ImportRecipeDataViewModel
import com.vokilam.servescalc.feature.addrecipe.R
import com.vokilam.servescalc.feature.addrecipe.databinding.AddRecipeFragmentBinding
import com.vokilam.servescalc.feature.addrecipe.presentation.model.TextStyle
import com.vokilam.servescalc.feature.addrecipe.presentation.model.ToolbarModel
import com.vokilam.servescalc.feature.addrecipe.presentation.ui.AddRecipeAction.ShowMessage
import com.vokilam.servescalc.feature.addrecipe.presentation.ui.AddRecipeFragmentDirections.Companion.actionAddRecipeToRecipeDetails
import com.vokilam.servescalc.feature.addrecipe.presentation.ui.NavigationEvent.Exit
import com.vokilam.servescalc.feature.addrecipe.presentation.ui.NavigationEvent.OpenPhotoCropper
import com.vokilam.servescalc.feature.addrecipe.presentation.ui.NavigationEvent.OpenRecipeDetails
import com.vokilam.servescalc.feature.addrecipe.presentation.ui.NavigationEvent.ShowConfirmExitDialog
import com.vokilam.servescalc.feature.addrecipe.presentation.ui.NavigationEvent.ShowImportIngredientsFtueDialog
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class AddRecipeFragment : Fragment(R.layout.add_recipe_fragment) {

    @Inject lateinit var textStyleFormatter: TextStyleFormatter
    private val viewModel by viewModels<AddRecipeViewModel>()
    private val activityViewModel by activityViewModels<ImportRecipeDataViewModel>()
    private val args by navArgs<AddRecipeFragmentArgs>()

    private lateinit var binding: AddRecipeFragmentBinding
    private lateinit var onBackPressedCallback: OnBackPressedCallback

    private val cropPhotoRequest = registerForActivityResult(CropPhotoContract()) { uri ->
        L.debug { "cropPhotoRequest: uri=$uri" }
        uri?.let { viewModel.onPhotoSelected(it.toString()) }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = AddRecipeFragmentBinding.inflate(layoutInflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            viewModel = this@AddRecipeFragment.viewModel
        }
        return binding.root
    }

    @Suppress("LongMethod", "ComplexMethod")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupTitle()
        setupServesType()

        onBackPressedCallback = requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, false) {
            viewModel.onBackPressed()
        }

        viewModel.run {
            toolbarModel.observe(viewLifecycleOwner, ::updateToolbar)
            ingredientsTextStyle.observe(viewLifecycleOwner, ::applyTextStyles)
            actions.observe(viewLifecycleOwner, ::handleAction)
            navigationEvents.observe(viewLifecycleOwner, ::handleNavigation)
            onBackPressedEnabled.observe(viewLifecycleOwner, onBackPressedCallback::setEnabled)
        }

        activityViewModel.importRecipeData.observe(viewLifecycleOwner, viewModel::handleImportRecipeData)

        args.importText?.let { viewModel.handleImportRecipeData(it) }
    }

    private fun setupToolbar() {
        val toolbar = requireNotNull((activity as AppCompatActivity).supportActionBar)
        toolbar.setTitle(R.string.title_recipe_add)
    }

    private fun setupTitle() {
        binding.title.run {
            // https://stackoverflow.com/a/53614047/317928
            imeOptions = EditorInfo.IME_ACTION_NEXT
            setRawInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES)
        }
    }

    private fun setupServesType() {
        binding.servesType.run {
            // TODO: feature/mvvm 11/19/20 adapter should be populated externally
            setAdapter(
                ArrayAdapter(
                    context,
                    android.R.layout.simple_spinner_dropdown_item,
                    AddRecipeViewModel.SERVES_TYPE_VALUES.map(context::getString)
                )
            )
        }
    }

    private fun updateToolbar(model: ToolbarModel) {
        L.verb { "updateToolbar(): model=$model" }
        activity?.invalidateOptionsMenu()
    }

    private fun applyTextStyles(model: List<TextStyle>) {
        binding.ingredients.run {
            text?.let { post { textStyleFormatter.applyTextStyles(context, it, model) } }
        }
    }

    private fun handleAction(action: AddRecipeAction) {
        L.verb { "handleAction(): action=$action" }
        when (action) {
            is ShowMessage -> Snackbar.make(binding.root, action.text, Snackbar.LENGTH_LONG).show()
        }
    }

    private fun handleNavigation(event: NavigationEvent) {
        L.verb { "handleNavigation(): $event" }
        when (event) {
            ShowImportIngredientsFtueDialog -> {
                ImportIngredientsFtueDialogFragment.newInstance()
                    .show(childFragmentManager, "ShowImportIngredientsFtueDialog")

                childFragmentManager.setFragmentResultListener(
                    ImportIngredientsFtueDialogFragment.REQUEST_KEY,
                    viewLifecycleOwner
                ) { _, _ ->
                    viewModel.onImportButtonClicked()
                }
            }

            OpenPhotoCropper -> cropPhotoRequest.launch(Unit)

            is ShowConfirmExitDialog -> {
                ConfirmExitDialogFragment.newInstance(event.navigateUp)
                    .show(childFragmentManager, "ConfirmExitDialogFragment")

                childFragmentManager.setFragmentResultListener(
                    ConfirmExitDialogFragment.REQUEST_KEY,
                    viewLifecycleOwner
                ) { _, bundle ->
                    val navigateUp = bundle.getBoolean(ConfirmExitDialogFragment.KEY_NAVIGATE_UP)
                    viewModel.onExitActionConfirmed(navigateUp)
                }
            }

            is Exit -> {
                if (event.navigateUp) findNavController().navigateUp()
                else findNavController().popBackStack()
            }

            is OpenRecipeDetails -> {
                findNavController().navigate(actionAddRecipeToRecipeDetails(event.recipe.id.value))
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.add_recipe, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        // TODO: feature/mvvm 11/19/20 simplify this
        menu.findItem(R.id.menu_save)?.isEnabled = viewModel.toolbarModel.value?.saveActionEnabled == true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            android.R.id.home -> { viewModel.onUpPressed(); true }
            R.id.menu_save -> { viewModel.onSaveClicked(); true }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private class CropPhotoContract : ActivityResultContract<Unit, Uri?>() {

        override fun createIntent(context: Context, input: Unit): Intent {
            return CropImage.activity()
                .setAllowFlipping(false)
                .setAllowRotation(true)
                .setAllowCounterRotation(true)
                .setAutoZoomEnabled(true)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .getIntent(context)
        }

        override fun parseResult(resultCode: Int, intent: Intent?): Uri? {
            if (resultCode != Activity.RESULT_OK) return null
            return CropImage.getActivityResult(intent).uri
        }
    }

    companion object {
        private val L = Logger()
        fun newInstance() = AddRecipeFragment()
    }
}

