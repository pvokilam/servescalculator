package com.vokilam.servescalc.feature.addrecipe.domain.usecase

import com.vokilam.servescalc.domain.CoroutinesDispatcherProvider
import com.vokilam.servescalc.domain.model.Amount
import com.vokilam.servescalc.domain.model.ParseResult
import com.vokilam.servescalc.domain.parser.IngredientsParser
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SanitizeIngredientsDataUseCase @Inject constructor(
    private val parser: IngredientsParser,
    private val dispatchers: CoroutinesDispatcherProvider
) {
    suspend operator fun invoke(data: String): String = withContext(dispatchers.computation) {
        val lines = data.split(DELIMITER).map(String::trim)
        val categories = parser.parse(lines)
        joinIngredientAndAmountSubsequentLines(lines, categories).joinToString(DELIMITER)
    }

    private fun joinIngredientAndAmountSubsequentLines(lines: List<String>, categories: List<ParseResult>): Sequence<String> {
        val iterator = lines.zip(categories, ::Result).listIterator()

        return sequence {
            while (iterator.hasNext()) {
                val a = iterator.next()

                if (a.category is ParseResult.Ingredient
                    && a.category.ingredient.amount == Amount.Empty
                    && iterator.hasNext()) {
                    val b = iterator.next()

                    if (b.category is ParseResult.Amount) {
                        yield("${a.input}$CONCAT_GLUE${b.input}")
                    } else {
                        iterator.previous()
                        yield(a.input)
                    }
                } else {
                    yield(a.input)
                }
            }
        }
    }

    data class Result(val input: String, val category: ParseResult)

    companion object {
        private const val DELIMITER = "\n"
        private const val CONCAT_GLUE = " "
    }
}