package com.vokilam.servescalc.feature.addrecipe.presentation.model

import com.vokilam.servescalc.core.presentation.Text

data class IngredientsModel(
    val enabled: Boolean = true,
    val importProgressVisible: Boolean = false,
    val ingredientsError: Text = Text.Empty,
    val textStyle: List<TextStyle> = emptyList()
)