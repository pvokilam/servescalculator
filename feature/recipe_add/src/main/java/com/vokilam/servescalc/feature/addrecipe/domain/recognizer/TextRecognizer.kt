package com.vokilam.servescalc.feature.addrecipe.domain.recognizer

import com.vokilam.servescalc.domain.Result

interface TextRecognizer {
    suspend fun detect(uri: String, onDevice: Boolean = true): Result<String>
}