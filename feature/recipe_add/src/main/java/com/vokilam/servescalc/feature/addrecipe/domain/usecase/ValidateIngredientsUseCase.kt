package com.vokilam.servescalc.feature.addrecipe.domain.usecase

import com.vokilam.servescalc.domain.Result
import com.vokilam.servescalc.domain.model.Ingredient
import com.vokilam.servescalc.domain.model.ParseResult
import com.vokilam.servescalc.domain.parser.IngredientsParser
import javax.inject.Inject

class ValidateIngredientsUseCase @Inject constructor(
    private val parser: IngredientsParser
) {
    suspend operator fun invoke(input: String): Result<List<Ingredient>> {
        val lines = input.split("\n").map(String::trim)
        val categories = parser.parse(lines)

        val error = lines.zip(categories)
            .firstOrNull { it.second == ParseResult.NoMatch || it.second is ParseResult.Amount }

        if (error != null) {
            return Result.Error(Exception(error.first))
        }

        val ingredients = categories
            .filterIsInstance<ParseResult.Ingredient>()
            .map { it.ingredient }

        return Result.Success(ingredients)
    }
}