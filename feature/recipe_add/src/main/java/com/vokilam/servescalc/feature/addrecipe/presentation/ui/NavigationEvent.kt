package com.vokilam.servescalc.feature.addrecipe.presentation.ui

import com.vokilam.servescalc.domain.model.Recipe

sealed class NavigationEvent {
    object ShowImportIngredientsFtueDialog : NavigationEvent()
    object OpenPhotoCropper : NavigationEvent()
    data class ShowConfirmExitDialog(val navigateUp: Boolean): NavigationEvent()
    data class Exit(val navigateUp: Boolean) : NavigationEvent()
    data class OpenRecipeDetails(val recipe: Recipe) : NavigationEvent()
}