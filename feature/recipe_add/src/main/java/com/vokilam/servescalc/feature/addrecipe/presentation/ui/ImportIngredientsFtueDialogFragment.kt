package com.vokilam.servescalc.feature.addrecipe.presentation.ui

import android.app.Dialog
import android.net.Uri
import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.view.SimpleDraweeView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.vokilam.servescalc.core.presentation.setView
import com.vokilam.servescalc.feature.addrecipe.R

class ImportIngredientsFtueDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.dialog_import_ingredients_ftue_title)
            .setMessage(R.string.dialog_import_ingredients_ftue_body)
            .setView(R.layout.view_ftue_animation) {
                it.findViewById<SimpleDraweeView>(R.id.animationView).run{
                    controller = Fresco.newDraweeControllerBuilder()
                        .setUri(Uri.parse("asset:///animations/ftue_crop_anim.webp"))
                        .setAutoPlayAnimations(true)
                        .build()
                }
            }
            .setPositiveButton(R.string.dialog_button_gotit) { _, _ ->
                setFragmentResult(REQUEST_KEY, bundleOf())
            }
            .create()
    }

    companion object {
        const val REQUEST_KEY = "ImportIngredientsFtueDialogFragment"

        fun newInstance(): ImportIngredientsFtueDialogFragment {
            return ImportIngredientsFtueDialogFragment()
        }
    }
}