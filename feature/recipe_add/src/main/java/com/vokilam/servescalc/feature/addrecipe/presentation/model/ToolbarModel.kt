package com.vokilam.servescalc.feature.addrecipe.presentation.model

data class ToolbarModel(
    val saveActionEnabled: Boolean = false
)