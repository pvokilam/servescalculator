package com.vokilam.servescalc.feature.addrecipe.presentation.ui

sealed class AddRecipeAction {
    data class ShowMessage(val text: String) : AddRecipeAction()
}
