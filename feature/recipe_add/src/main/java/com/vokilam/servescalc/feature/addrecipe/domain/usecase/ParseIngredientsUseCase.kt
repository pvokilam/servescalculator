package com.vokilam.servescalc.feature.addrecipe.domain.usecase

import com.vokilam.servescalc.core.domain.shift
import com.vokilam.servescalc.domain.model.ParseResult
import com.vokilam.servescalc.domain.parser.IngredientsParser
import javax.inject.Inject

class ParseIngredientsUseCase @Inject constructor(
    private val parser: IngredientsParser
) {
    suspend operator fun invoke(input: String): List<ParseResult> {
        val lines = input.split(DELIMITER)
        val categories = parser.parse(lines)
        return shiftRanges(lines, categories)
    }

    private fun shiftRanges(lines: List<String>, categories: List<ParseResult>): List<ParseResult> {
        val lineOffsets = lines.fold(mutableListOf(0)) { acc, s ->
            acc.apply {
                add(last() + s.length + DELIMITER.length)
            }
        }

        return categories.zip(lineOffsets).map {
            val (cat, offset) = it
            if (cat is ParseResult.Ingredient) {
                cat.copy(amountRange =  cat.amountRange.shift(offset))
            } else {
                cat
            }
        }
    }

    companion object {
        const val DELIMITER = "\n"
    }
}