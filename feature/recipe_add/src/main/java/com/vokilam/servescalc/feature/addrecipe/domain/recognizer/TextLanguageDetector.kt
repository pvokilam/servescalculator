package com.vokilam.servescalc.feature.addrecipe.domain.recognizer

import com.vokilam.servescalc.domain.Result

interface TextLanguageDetector {
    suspend fun detect(input: String): Result<String>

    companion object {
        const val NO_LANG = ""
    }
}