package com.vokilam.servescalc.feature.addrecipe.presentation.model

import com.vokilam.servescalc.core.presentation.Text

data class ServesValueModel(
    val prefixResId: Text = Text.Empty,
    val suffixResId: Text = Text.Empty
)