package com.vokilam.servescalc.feature.addrecipe.presentation.ui

import android.content.Context
import android.graphics.Typeface
import android.text.Spannable
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import com.vokilam.logger.Logger
import com.vokilam.servescalc.core.presentation.getAttrColor
import com.vokilam.servescalc.feature.addrecipe.R
import com.vokilam.servescalc.feature.addrecipe.presentation.model.TextStyle
import javax.inject.Inject

class TextStyleFormatter @Inject constructor() {

    fun applyTextStyles(context: Context, text: Spannable, styles: List<TextStyle>) {
        L.debug { "applyTextStyles(): textLength=${text.length}" }
        text.getSpans(0, text.length, StyleSpan::class.java).forEach(text::removeSpan)
        text.getSpans(0, text.length, ForegroundColorSpan::class.java).forEach(text::removeSpan)

        styles
            .filter { !it.range.isEmpty() }
            .forEach {
                when (it) {
                    is TextStyle.Amount -> applyAmountStyle(context, text, it)
                }
            }
    }

    private fun applyAmountStyle(context: Context, text: Spannable, style: TextStyle.Amount) {
        val spans = listOf(
            StyleSpan(Typeface.BOLD),
            ForegroundColorSpan(context.getAttrColor(R.attr.colorPrimary))
        )

        try {
            for (span in spans) {
                text.setSpan(
                    span,
                    style.range.first,
                    style.range.last + 1,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
        } catch (e: IndexOutOfBoundsException) {
            L.warn(e) { "applyAmountStyle()" } // investigate behavior
        }
    }

    companion object {
        private val L = Logger()
    }
}