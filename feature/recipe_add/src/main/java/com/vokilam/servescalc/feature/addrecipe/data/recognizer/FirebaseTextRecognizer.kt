package com.vokilam.servescalc.feature.addrecipe.data.recognizer

import android.content.Context
import android.net.Uri
import com.google.firebase.ml.common.FirebaseMLException
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer
import com.google.mlkit.vision.common.InputImage
import com.vokilam.logger.Logger
import com.vokilam.servescalc.domain.Result
import com.vokilam.servescalc.feature.addrecipe.domain.recognizer.TextRecognizer
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.suspendCancellableCoroutine
import java.io.IOException
import javax.inject.Inject
import kotlin.Result as KResult

class FirebaseTextRecognizer @Inject constructor(
    @ApplicationContext private val context: Context,
    private val onDeviceTextRecognizer: com.google.mlkit.vision.text.TextRecognizer,
    private val cloudTextRecognizer: FirebaseVisionTextRecognizer
) : TextRecognizer {
    override suspend fun detect(uri: String, onDevice: Boolean): Result<String> {
        L.debug { "detect(): uri=$uri, onDevice=$onDevice" }
        return if (onDevice) runOnDeviceRecognizer(uri) else runCloudRecognizer(uri)
    }

    private suspend fun runOnDeviceRecognizer(uri: String): Result<String> {
        return suspendCancellableCoroutine { cont ->
            try {
                val img = InputImage.fromFilePath(context, Uri.parse(uri))

                onDeviceTextRecognizer.process(img)
                    .addOnSuccessListener {
                        L.debug { "detect(): onSuccess $it" }
                        val res = Result.Success(it.text)
                        cont.resumeWith(KResult.success(res))
                    }
                    .addOnFailureListener {
                        when (it) {
                            is FirebaseMLException -> L.warn { "detect(): onFailure $it, code=${it.code}" }
                            else -> L.warn { "detect(): onFailure $it" }
                        }

                        val res = Result.Error(it)
                        cont.resumeWith(KResult.success(res))
                    }
                    .addOnCanceledListener {
                        L.debug { "detect(): onCancel" }
                        cont.cancel()
                    }
            } catch (e: IOException) {
                val res = Result.Error(e)
                cont.resumeWith(KResult.success(res))
            }
        }
    }

    private suspend fun runCloudRecognizer(uri: String): Result<String> {
        return suspendCancellableCoroutine { cont ->
            try {
                val img = FirebaseVisionImage.fromFilePath(context, Uri.parse(uri))

                cloudTextRecognizer.processImage(img)
                    .addOnSuccessListener {
                        L.debug { "detect(): onSuccess $it" }
                        val res = Result.Success(it.text)
                        cont.resumeWith(KResult.success(res))
                    }
                    .addOnFailureListener {
                        when (it) {
                            is FirebaseMLException -> L.warn { "detect(): onFailure $it, code=${it.code}" }
                            else -> L.warn { "detect(): onFailure $it" }
                        }

                        val res = Result.Error(it)
                        cont.resumeWith(KResult.success(res))
                    }
                    .addOnCanceledListener {
                        L.debug { "detect(): onCancel" }
                        cont.cancel()
                    }
            } catch (e: IOException) {
                val res = Result.Error(e)
                cont.resumeWith(KResult.success(res))
            }
        }
    }

    companion object {
        private val L = Logger()
    }
}