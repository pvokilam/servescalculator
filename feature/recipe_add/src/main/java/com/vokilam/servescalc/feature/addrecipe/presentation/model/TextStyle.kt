package com.vokilam.servescalc.feature.addrecipe.presentation.model

sealed class TextStyle {
    abstract val range: IntRange
    data class Amount(override val range: IntRange) : TextStyle()
}