package com.vokilam.servescalc.feature.addrecipe.presentation.ui

import android.app.Dialog
import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.vokilam.servescalc.feature.addrecipe.R

class ConfirmExitDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.dialog_title_confirm_exit)
            .setMessage(R.string.dialog_body_confirm_exit)
            .setPositiveButton(R.string.dialog_button_exit) { _, _ ->
                val navigateUp = arguments?.getBoolean(ARG_NAVIGATE_UP) ?: false
                setFragmentResult(REQUEST_KEY, bundleOf(KEY_NAVIGATE_UP to navigateUp))
            }
            .setNegativeButton(R.string.dialog_button_cancel, null)
            .create()
    }

    companion object {
        const val REQUEST_KEY = "ConfirmExitDialogFragment"
        const val KEY_NAVIGATE_UP = "navigate_up"

        private const val ARG_NAVIGATE_UP = "navigate_up"

        fun newInstance(navigateUp: Boolean): ConfirmExitDialogFragment {
            return ConfirmExitDialogFragment().apply {
                arguments = bundleOf(ARG_NAVIGATE_UP to navigateUp)
            }
        }
    }
}
