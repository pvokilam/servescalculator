package com.vokilam.servescalc.feature.addrecipe.di

import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer
import com.google.mlkit.nl.languageid.LanguageIdentification
import com.google.mlkit.nl.languageid.LanguageIdentifier
import com.google.mlkit.vision.text.TextRecognition
import com.vokilam.servescalc.feature.addrecipe.data.recognizer.FirebaseTextLanguageDetector
import com.vokilam.servescalc.feature.addrecipe.data.recognizer.FirebaseTextRecognizer
import com.vokilam.servescalc.feature.addrecipe.domain.recognizer.TextLanguageDetector
import com.vokilam.servescalc.feature.addrecipe.domain.recognizer.TextRecognizer
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@Module
@InstallIn(ActivityRetainedComponent::class)
abstract class TextRecognizerModule {

    @Binds
    abstract fun bindTextRecognizer(impl: FirebaseTextRecognizer): TextRecognizer

    @Binds
    abstract fun bindTextLanguageDetector(impl: FirebaseTextLanguageDetector): TextLanguageDetector

    companion object {
        @Provides
        fun provideOnDeviceFirebaseVisionTextRecognizer(): com.google.mlkit.vision.text.TextRecognizer {
            return TextRecognition.getClient()
        }

        @Provides
        fun provideCloudFirebaseVisionTextRecognizer(): FirebaseVisionTextRecognizer {
            return FirebaseVision.getInstance().cloudTextRecognizer
        }

        @Provides
        fun provideFirebaseLanguageIdentification() : LanguageIdentifier {
            return LanguageIdentification.getClient()
        }
    }
}