package com.vokilam.servescalc.feature.addrecipe.presentation.ui

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.distinctUntilChanged
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import com.vokilam.logger.Logger
import com.vokilam.servescalc.core.domain.usecase.SaveRecipeUseCase
import com.vokilam.servescalc.core.presentation.MediatorLiveData
import com.vokilam.servescalc.core.presentation.Text
import com.vokilam.servescalc.core.presentation.update
import com.vokilam.servescalc.core.presentation.util.LiveEvent
import com.vokilam.servescalc.domain.Result
import com.vokilam.servescalc.domain.exceptionOrNull
import com.vokilam.servescalc.domain.getOrDefault
import com.vokilam.servescalc.domain.isError
import com.vokilam.servescalc.domain.model.Adjustment
import com.vokilam.servescalc.domain.model.FullRecipe
import com.vokilam.servescalc.domain.model.ParseResult
import com.vokilam.servescalc.domain.model.Recipe
import com.vokilam.servescalc.domain.model.RecipeId
import com.vokilam.servescalc.domain.model.Serving
import com.vokilam.servescalc.domain.settings.AppSettings
import com.vokilam.servescalc.feature.addrecipe.R
import com.vokilam.servescalc.feature.addrecipe.domain.usecase.ParseIngredientsUseCase
import com.vokilam.servescalc.feature.addrecipe.domain.usecase.RecognizeTextUseCase
import com.vokilam.servescalc.feature.addrecipe.domain.usecase.SanitizeIngredientsDataUseCase
import com.vokilam.servescalc.feature.addrecipe.domain.usecase.ValidateIngredientsUseCase
import com.vokilam.servescalc.feature.addrecipe.presentation.model.IngredientsModel
import com.vokilam.servescalc.feature.addrecipe.presentation.model.ServesValueModel
import com.vokilam.servescalc.feature.addrecipe.presentation.model.TextStyle
import com.vokilam.servescalc.feature.addrecipe.presentation.model.ToolbarModel
import com.vokilam.servescalc.feature.addrecipe.presentation.ui.AddRecipeAction.ShowMessage
import com.vokilam.servescalc.feature.addrecipe.presentation.ui.NavigationEvent.Exit
import com.vokilam.servescalc.feature.addrecipe.presentation.ui.NavigationEvent.OpenPhotoCropper
import com.vokilam.servescalc.feature.addrecipe.presentation.ui.NavigationEvent.OpenRecipeDetails
import com.vokilam.servescalc.feature.addrecipe.presentation.ui.NavigationEvent.ShowConfirmExitDialog
import com.vokilam.servescalc.feature.addrecipe.presentation.ui.NavigationEvent.ShowImportIngredientsFtueDialog
import kotlinx.coroutines.launch

@Suppress("TooManyFunctions")
class AddRecipeViewModel @ViewModelInject constructor(
    private val recognizeTextUseCase: RecognizeTextUseCase,
    private val sanitizeIngredientsDataUseCase: SanitizeIngredientsDataUseCase,
    private val parseIngredientsUseCase: ParseIngredientsUseCase,
    private val validateIngredientsUseCase: ValidateIngredientsUseCase,
    private val saveRecipeUseCase: SaveRecipeUseCase,
    private val appSettings: AppSettings,
    @Assisted private val state: SavedStateHandle
) : ViewModel() {

    val title = state.getLiveData(KEY_TITLE, "")
    val ingredients = state.getLiveData(KEY_INGREDIENTS, "")
    val servesValue = state.getLiveData(KEY_SERVES_VALUE, "")
    val servesType = state.getLiveData(KEY_SERVES_TYPE, "")

    @Suppress("USELESS_CAST")
    val ingredientsTextStyle: LiveData<List<TextStyle>> = ingredients.switchMap {
        liveData {
            emit(parseIngredientsUseCase(it)
                .asSequence()
                .filterIsInstance<ParseResult.Ingredient>()
                .filter { !it.amountRange.isEmpty() }
                .map {
                    TextStyle.Amount(it.amountRange) as TextStyle
                }
                .toList()
            )
        }
    }

    private val hasChanges = MediatorLiveData<Boolean>()
    private val canSave = MediatorLiveData<Boolean>()

    val onBackPressedEnabled: LiveData<Boolean> = hasChanges
    
    private val selectedServesType = MutableLiveData<Int>() // TODO: 12/6/19 save state

    private val _toolbarModel = MediatorLiveData(ToolbarModel())
    val toolbarModel: LiveData<ToolbarModel> = _toolbarModel.distinctUntilChanged()

    private val _servesValueModel = MediatorLiveData<ServesValueModel>()
    val servesValueModel: LiveData<ServesValueModel> = _servesValueModel.distinctUntilChanged()

    private val _ingredientsModel = MediatorLiveData(IngredientsModel())
    val ingredientsModel: LiveData<IngredientsModel> = _ingredientsModel.distinctUntilChanged()

    private val _actions = LiveEvent<AddRecipeAction>()
    val actions: LiveData<AddRecipeAction> = _actions

    private val _navigationEvents = LiveEvent<NavigationEvent>()
    val navigationEvents: LiveData<NavigationEvent> = _navigationEvents

    init {
        setupHasChangesLiveData()
        setupCanSaveLiveData()
        setupIngredientsErrorLiveData()
        setupServesValueModelLiveData()

        _toolbarModel.addSource(canSave) {
            _toolbarModel.update { it.copy(saveActionEnabled = canSave.value == true) }
        }
    }

    private fun setupServesValueModelLiveData() {
        _servesValueModel.addSource(servesType) {
            _servesValueModel.value = ServesValueModel()
            selectedServesType.value = 0
        }
    }

    private fun setupIngredientsErrorLiveData() {
        _ingredientsModel.addSource(ingredients) {
            _ingredientsModel.update { it.copy(ingredientsError = Text.Empty) }
        }
    }

    private fun setupCanSaveLiveData() {
        val observer = Observer<Any> {
            canSave.value = !title.value.isNullOrEmpty() && !ingredients.value.isNullOrEmpty()
        }
        canSave.value = false
        canSave.addSource(title, observer)
        canSave.addSource(ingredients, observer)
    }

    private fun setupHasChangesLiveData() {
        val observer = Observer<String> {
            if (!it.isNullOrEmpty()) {
                hasChanges.value = true
            }
        }
        hasChanges.value = false
        hasChanges.addSource(title, observer)
        hasChanges.addSource(ingredients, observer)
        hasChanges.addSource(servesType, observer)
        hasChanges.addSource(servesValue, observer)
    }

    fun onSaveClicked() {
        L.debug { "onSaveClicked()" }
        viewModelScope.launch {
            val ingredients = validateIngredientsUseCase(ingredients.value.orEmpty())

            if (ingredients.isError) {
                _ingredientsModel.update { it.copy(
                    ingredientsError = Text.Resource(
                        R.string.error_unknown_ingredient,
                        ingredients.exceptionOrNull()?.message ?: ""
                    )
                ) }
                return@launch
            }

            val serves = servesValue.value?.toFloatOrNull() ?: 0f

            val serving = when (selectedServesType.value) {
                R.string.serves_type_ring_diameter -> Serving.RingDiameter(serves)
                else -> {
                    if (serves == 0f) Serving.NONE
                    else Serving.Custom(serves, servesType.value.orEmpty())
                }
            }

            val recipe = FullRecipe(
                Recipe(RecipeId.UNDEFINED, title.value.orEmpty()),
                serving,
                Adjustment.fromServing(serving), // TODO: feature/last_serving 12/8/20 hide this behind usecase?
                ingredients.getOrDefault(emptyList())
            )
            val id = saveRecipeUseCase(recipe)
            _navigationEvents.value = OpenRecipeDetails(recipe.recipe.copy(id = id))
        }
    }

    fun onBackPressed() {
        L.debug { "onBackPressed()" }
        _navigationEvents.value = ShowConfirmExitDialog(false)
    }

    fun onUpPressed() {
        L.debug { "onUpPressed()" }
        if (hasChanges.value == true) {
            _navigationEvents.value = ShowConfirmExitDialog(true)
        } else {
            _navigationEvents.value = Exit(true)
        }
    }

    fun onExitActionConfirmed(navigateUp: Boolean) {
        L.debug { "onExitActionConfirmed()" }
        _navigationEvents.value = Exit(navigateUp)
    }

    fun handleImportRecipeData(data: String) {
        L.debug { "handleImportRecipeData(): data=$data" }
        viewModelScope.launch {
            handleImportRecipeDataInternal(data)
        }
    }

    private suspend fun handleImportRecipeDataInternal(data: String) {
        ingredients.run {
            val left = value.orEmpty()
            val right = sanitizeIngredientsDataUseCase(data)
            val glue = if (left.isNotEmpty() && right.isNotEmpty()) CONCAT_GLUE else ""

            value = left + glue + right
        }
    }

    fun onImportButtonClicked() {
        L.debug { "onImportButtonClicked()" }

        if (appSettings.shouldShowImportIngredientsFtue) {
            appSettings.shouldShowImportIngredientsFtue = false
            _navigationEvents.value = ShowImportIngredientsFtueDialog
        } else {
            _navigationEvents.value = OpenPhotoCropper
        }
    }

    fun onPhotoSelected(uri: String) {
        L.debug { "onPhotoSelected(): $uri" }
        _ingredientsModel.update { it.copy(enabled = false, importProgressVisible = true) }

        viewModelScope.launch {
            when (val result = recognizeTextUseCase(uri)) {
                is Result.Success -> handleImportRecipeDataInternal(result.value)
                is Result.Error -> _actions.value = ShowMessage(result.exception.localizedMessage.orEmpty())
            }

            _ingredientsModel.update { it.copy(enabled = true, importProgressVisible = false) }
        }
    }

    fun onServesTypeSelected(position: Int) {
        L.debug { "onServesTypeSelected(): position=$position" }
        val id = SERVES_TYPE_VALUES.getOrNull(position) ?: return

        selectedServesType.value = id

        _servesValueModel.value = when (id) {
            R.string.serves_type_ring_diameter -> ServesValueModel(
                prefixResId = Text.Resource(R.string.helper_text_serves_type_ring_diameter)
            )
            else -> ServesValueModel()
        }
    }

    companion object {
        private val L = Logger()

        const val CONCAT_GLUE = "\n\n"
        const val KEY_TITLE = "title"
        const val KEY_INGREDIENTS = "ingredients"
        const val KEY_SERVES_VALUE = "serves_value"
        const val KEY_SERVES_TYPE = "serves_type"

        val SERVES_TYPE_VALUES = arrayOf(
            R.string.serves_type_ring_diameter
        )
    }
}
