package com.vokilam.servescalc.feature.addrecipe.data.recognizer

import com.google.mlkit.nl.languageid.LanguageIdentifier
import com.vokilam.logger.Logger
import com.vokilam.servescalc.domain.Result
import com.vokilam.servescalc.feature.addrecipe.domain.recognizer.TextLanguageDetector
import com.vokilam.servescalc.feature.addrecipe.domain.recognizer.TextLanguageDetector.Companion.NO_LANG
import kotlinx.coroutines.suspendCancellableCoroutine
import javax.inject.Inject
import kotlin.Result as KResult

class FirebaseTextLanguageDetector @Inject constructor(
    private val languageDetector: LanguageIdentifier
) : TextLanguageDetector {
    override suspend fun detect(input: String): Result<String> {
        L.debug { "detect(): input=$input" }
        return suspendCancellableCoroutine { cont ->
            languageDetector.identifyLanguage(input)
                .addOnSuccessListener {
                    val res = Result.Success(if (it == "und") NO_LANG else it)
                    cont.resumeWith(KResult.success(res))
                }
                .addOnFailureListener {
                    val res = Result.Error(it)
                    cont.resumeWith(KResult.success(res))
                }
        }
    }

    companion object {
        private val L = Logger()
    }
}