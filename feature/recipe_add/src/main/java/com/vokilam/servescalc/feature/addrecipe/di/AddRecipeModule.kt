package com.vokilam.servescalc.feature.addrecipe.di

import com.vokilam.servescalc.recipeparser.di.RecipeParserModule
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@Module(includes = [RecipeParserModule::class])
@InstallIn(ActivityRetainedComponent::class)
abstract class AddRecipeModule
