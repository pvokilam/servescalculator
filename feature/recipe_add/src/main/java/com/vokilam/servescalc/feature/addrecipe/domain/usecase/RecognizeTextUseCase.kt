package com.vokilam.servescalc.feature.addrecipe.domain.usecase

import com.vokilam.logger.Logger
import com.vokilam.servescalc.domain.Result
import com.vokilam.servescalc.domain.getOrDefault
import com.vokilam.servescalc.feature.addrecipe.domain.recognizer.TextLanguageDetector
import com.vokilam.servescalc.feature.addrecipe.domain.recognizer.TextLanguageDetector.Companion.NO_LANG
import com.vokilam.servescalc.feature.addrecipe.domain.recognizer.TextRecognizer
import javax.inject.Inject

class RecognizeTextUseCase @Inject constructor(
    private val textRecognizer: TextRecognizer,
    private val languageDetector: TextLanguageDetector
) {
    suspend operator fun invoke(uri: String): Result<String> {
        /*
         *  What we do here is reducing cloud requests by running on-device recognizer first.
         *  We assume that text is recognized correctly if language can be detected on that text.
         *  Otherwise we perform cloud recognition.
         */
        L.debug { "(): $uri" }
        val localResult = textRecognizer.detect(uri, onDevice = true)
        val lang = languageDetector.detect(localResult.getOrDefault(""))
        L.debug { "(): lang=$lang" }

        return if (lang.getOrDefault(NO_LANG) != NO_LANG) localResult
        else textRecognizer.detect(uri, onDevice = false)
    }

    companion object {
        private val L = Logger()
    }
}