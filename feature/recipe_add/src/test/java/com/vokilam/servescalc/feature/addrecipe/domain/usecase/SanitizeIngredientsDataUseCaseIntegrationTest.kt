package com.vokilam.servescalc.feature.addrecipe.domain.usecase

import com.vokilam.servescalc.domain.CoroutinesDispatcherProvider
import com.vokilam.servescalc.domain.parser.IngredientsParser
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Ignore

@ExperimentalCoroutinesApi
class SanitizeIngredientsDataUseCaseIntegrationTest {

    lateinit var useCase: SanitizeIngredientsDataUseCase
    lateinit var parser: IngredientsParser

    @Before
    fun setUp() {
//        val dispatchers = CoroutinesDispatcherProvider(TestCoroutineDispatcher())
//        parser = RegexIngredientsParser(
//            ParserModule.provideMatcherChain(),
//            dispatchers
//        )
//        useCase = SanitizeIngredientsDataUseCase(parser, dispatchers)
    }

    @Ignore("need to provide parser") // todo
    fun `when line contains amount only should join with previous line`() = runBlockingTest {
        val data = """
            ingredients
            
            
            flour
            500g
            sugar
             40g
            2 eggs
            salt
            to taste
            flour for dusting
        """.trimIndent()

        val expected = """
            ingredients
            
            
            flour 500g
            sugar 40g
            2 eggs
            salt to taste
            flour for dusting
        """.trimIndent()
        val actual = useCase(data)

        assertEquals(expected, actual)
    }

    @Ignore("need to provide parser") // todo
    fun `single line test`() = runBlockingTest {
        val data = "ingredients"
        val actual = useCase(data)

        assertEquals(data, actual)
    }
}