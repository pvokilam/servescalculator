package com.vokilam.servescalc.feature.addrecipe.domain.usecase

import com.vokilam.servescalc.domain.exceptionOrNull
import com.vokilam.servescalc.domain.getOrDefault
import com.vokilam.servescalc.domain.model.Amount
import com.vokilam.servescalc.domain.model.Ingredient
import com.vokilam.servescalc.domain.model.g
import com.vokilam.servescalc.domain.model.ParseResult
import com.vokilam.servescalc.domain.parser.IngredientsParser
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class ValidateIngredientsUseCaseTest {

    lateinit var useCase: ValidateIngredientsUseCase
    private val parser = mockk<IngredientsParser>()

    @Before
    fun setUp() {
        useCase =
            ValidateIngredientsUseCase(parser)
    }

    @Test
    fun `should return validated ingredients`() = runBlockingTest {
        val data = """
            flour 500g
            
            sugar 200g
        """.trimIndent()

        coEvery {
            parser.parse(data.split("\n"))
        } returns listOf(
            ParseResult.Ingredient(Ingredient("flour", 500.g), IntRange.EMPTY),
            ParseResult.Blank,
            ParseResult.Ingredient(Ingredient("sugar", 200.g), IntRange.EMPTY)
        )

        val actual = useCase(data)
        val expected = listOf(
            Ingredient("flour", 500.g),
            Ingredient("sugar", 200.g)
        )
        assertEquals(expected, actual.getOrDefault(emptyList()))
    }

    @Test
    fun `when parse failed with Amount should return error`() = runBlockingTest {
        val data = """
            flour
            500g
        """.trimIndent()

        coEvery {
            parser.parse(data.split("\n"))
        } returns listOf(
            ParseResult.Ingredient(Ingredient("flour", Amount.Empty), IntRange.EMPTY),
            ParseResult.Amount(500.g)
        )

        val actual = useCase(data)

        assertEquals("500g", actual.exceptionOrNull()?.message)
    }
}