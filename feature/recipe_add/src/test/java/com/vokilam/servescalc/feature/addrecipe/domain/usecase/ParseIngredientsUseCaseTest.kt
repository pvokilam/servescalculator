package com.vokilam.servescalc.feature.addrecipe.domain.usecase

import com.vokilam.servescalc.domain.model.Ingredient
import com.vokilam.servescalc.domain.model.ParseResult
import com.vokilam.servescalc.domain.model.g
import com.vokilam.servescalc.domain.parser.IngredientsParser
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class ParseIngredientsUseCaseTest {

    lateinit var useCase: ParseIngredientsUseCase
    private val parser = mockk<IngredientsParser>()

    @Before
    fun setUp() {
        useCase = ParseIngredientsUseCase(parser)
    }

    @Test
    fun `should offset amountRanges`() = runBlockingTest {
        val input = """
            flour 500g
            water 350g
            salt 10g
            yeast 10g
        """.trimIndent()

        coEvery { parser.parse(input.split("\n")) } returns listOf(
            ParseResult.Ingredient(Ingredient("flour", 500.g), 6..9),
            ParseResult.Ingredient(Ingredient("water", 500.g), 6..9),
            ParseResult.Ingredient(Ingredient("salt", 500.g), 5..7),
            ParseResult.Ingredient(Ingredient("yeast", 500.g), 6..8)
        )

        val actual = useCase(input).filterIsInstance<ParseResult.Ingredient>().map { it.amountRange }
        // parser returns ranges per line, we offset them to match full input
        val expected = listOf(6..9, 17..20, 27..29, 37..39)
        Assert.assertEquals(expected, actual)
    }
}