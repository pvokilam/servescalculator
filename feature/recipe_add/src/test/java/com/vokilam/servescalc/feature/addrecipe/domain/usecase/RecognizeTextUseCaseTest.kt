package com.vokilam.servescalc.feature.addrecipe.domain.usecase

import com.vokilam.servescalc.domain.Result
import com.vokilam.servescalc.domain.getOrDefault
import com.vokilam.servescalc.feature.addrecipe.domain.recognizer.TextLanguageDetector
import com.vokilam.servescalc.feature.addrecipe.domain.recognizer.TextLanguageDetector.Companion.NO_LANG
import com.vokilam.servescalc.feature.addrecipe.domain.recognizer.TextRecognizer
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.coVerifyOrder
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class RecognizeTextUseCaseTest {
    lateinit var useCase: RecognizeTextUseCase
    private val textRecognizer = mockk<TextRecognizer>()
    private val languageDetector = mockk<TextLanguageDetector>()

    @Before
    fun setUp() {
        useCase = RecognizeTextUseCase(textRecognizer, languageDetector)
    }

    @Test
    fun `when language is detected should run only local text recognizer`() = runBlockingTest {
        val uri = "uri://to/file"
        val text = "recognized text"

        coEvery { textRecognizer.detect(uri, true) } returns Result.Success(text)
        coEvery { languageDetector.detect(text) } returns Result.Success("en")

        val result = useCase(uri)

        coVerifyOrder {
            textRecognizer.detect(uri, true)
            languageDetector.detect(text)
        }
        assertEquals(text, result.getOrDefault(""))
    }

    @Test
    fun `when language cannot be detected should run local and remote text recognizer`() = runBlockingTest {
        val uri = "uri://to/file"
        val text = "recognized text"

        coEvery { textRecognizer.detect(uri, true) } returns Result.Success(text)
        coEvery { languageDetector.detect(text) } returns Result.Success(NO_LANG)
        coEvery { textRecognizer.detect(uri, false) } returns Result.Success(text)

        val result = useCase(uri)

        coVerify {
            textRecognizer.detect(uri, true)
            languageDetector.detect(text)
            textRecognizer.detect(uri, false)
        }
        assertEquals(text, result.getOrDefault(""))
    }
}