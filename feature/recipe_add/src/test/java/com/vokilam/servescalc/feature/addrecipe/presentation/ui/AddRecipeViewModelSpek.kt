package com.vokilam.servescalc.feature.addrecipe.presentation.ui

import androidx.arch.core.executor.ArchTaskExecutor
import androidx.lifecycle.SavedStateHandle
import com.vokilam.core.test.TestData
import com.vokilam.core.test.util.TestTaskExecutor
import com.vokilam.core.test.util.getOrAwaitValue
import com.vokilam.servescalc.core.domain.usecase.SaveRecipeUseCase
import com.vokilam.servescalc.core.presentation.Text
import com.vokilam.servescalc.domain.Result
import com.vokilam.servescalc.domain.model.FullRecipe
import com.vokilam.servescalc.domain.model.Ingredient
import com.vokilam.servescalc.domain.model.ParseResult
import com.vokilam.servescalc.domain.model.Serving
import com.vokilam.servescalc.domain.model.g
import com.vokilam.servescalc.domain.settings.AppSettings
import com.vokilam.servescalc.feature.addrecipe.R
import com.vokilam.servescalc.feature.addrecipe.domain.usecase.ParseIngredientsUseCase
import com.vokilam.servescalc.feature.addrecipe.domain.usecase.RecognizeTextUseCase
import com.vokilam.servescalc.feature.addrecipe.domain.usecase.SanitizeIngredientsDataUseCase
import com.vokilam.servescalc.feature.addrecipe.domain.usecase.ValidateIngredientsUseCase
import com.vokilam.servescalc.feature.addrecipe.presentation.model.ServesValueModel
import com.vokilam.servescalc.feature.addrecipe.presentation.model.TextStyle
import com.vokilam.servescalc.feature.addrecipe.presentation.ui.AddRecipeViewModel.Companion.CONCAT_GLUE
import com.vokilam.servescalc.feature.addrecipe.presentation.ui.AddRecipeViewModel.Companion.KEY_INGREDIENTS
import com.vokilam.servescalc.feature.addrecipe.presentation.ui.AddRecipeViewModel.Companion.KEY_SERVES_TYPE
import com.vokilam.servescalc.feature.addrecipe.presentation.ui.AddRecipeViewModel.Companion.KEY_SERVES_VALUE
import com.vokilam.servescalc.feature.addrecipe.presentation.ui.AddRecipeViewModel.Companion.KEY_TITLE
import com.vokilam.servescalc.feature.addrecipe.presentation.ui.NavigationEvent.*
import io.mockk.Runs
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.coVerifyOrder
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class AddRecipeViewModelSpek : Spek({
    val testDispatcher = TestCoroutineDispatcher()

    beforeEachTest {
        ArchTaskExecutor.getInstance().setDelegate(TestTaskExecutor())
        Dispatchers.setMain(testDispatcher)
    }

    afterEachTest {
        ArchTaskExecutor.getInstance().setDelegate(null)
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    val recognizeTextUseCase = mockk<RecognizeTextUseCase>()
    val sanitizeIngredientsDataUseCase = mockk<SanitizeIngredientsDataUseCase>()
    val parseIngredientsUseCase = mockk<ParseIngredientsUseCase>()
    val validateIngredientsUseCase = mockk<ValidateIngredientsUseCase>()
    val saveRecipeUseCase = mockk<SaveRecipeUseCase>()
    val appSettings = mockk<AppSettings>()
    val state by memoized { SavedStateHandle() }

    val viewModel by memoized {
        AddRecipeViewModel(
            recognizeTextUseCase,
            sanitizeIngredientsDataUseCase,
            parseIngredientsUseCase,
            validateIngredientsUseCase,
            saveRecipeUseCase,
            appSettings,
            state
        )
    }

    afterEachTest {
        clearAllMocks()
    }

    describe("init") {
        context("on restore") {
            beforeEachTest {
                state.set(KEY_TITLE, "title")
                state.set(KEY_INGREDIENTS, "ingredients")
                state.set(KEY_SERVES_VALUE, "serves value")
                state.set(KEY_SERVES_TYPE, "serves type")
            }

            it("should restore title") {
                assertEquals("title", viewModel.title.getOrAwaitValue())
            }

            it("should restore ingredients") {
                assertEquals("ingredients", viewModel.ingredients.getOrAwaitValue())
            }

            it("should restore serves value") {
                assertEquals("serves value", viewModel.servesValue.getOrAwaitValue())
            }

            it("should restore serves type") {
                assertEquals("serves type", viewModel.servesType.getOrAwaitValue())
            }
        }
    }

    describe("save action") {
        it("should be disabled by default") {
            assertFalse(viewModel.toolbarModel.getOrAwaitValue().saveActionEnabled)
        }

        context("when title is not empty") {
            beforeEachTest {
                viewModel.title.value = "title"
            }

            it("should be disabled") {
                assertFalse(viewModel.toolbarModel.getOrAwaitValue().saveActionEnabled)
            }
        }

        context("when ingredients is not empty") {
            beforeEachTest {
                viewModel.ingredients.value = "ingredients"
            }

            it("should be disabled") {
                assertFalse(viewModel.toolbarModel.getOrAwaitValue().saveActionEnabled)
            }
        }

        context("when title and ingredients are not empty") {
            beforeEachTest {
                viewModel.title.value = "title"
                viewModel.ingredients.value = "ingredients"
            }

            it("should be enabled") {
                assertTrue(viewModel.toolbarModel.getOrAwaitValue().saveActionEnabled)
            }
        }

        context("on click") {
            context("when all fields are filled") {
                val recipe = TestData.unsavedRecipe
                val rawIngredients = recipe.ingredients.joinToString("\n") { it.asString() }
                val savedRecipe = slot<FullRecipe>()

                beforeEachTest {
                    viewModel.title.value = recipe.recipe.title
                    viewModel.ingredients.value = rawIngredients
                    viewModel.servesValue.value = recipe.serving.value.toString()
                    viewModel.servesType.value = recipe.serving.unit

                    coEvery { validateIngredientsUseCase(any()) } returns Result.Success(recipe.ingredients)
                    coEvery { saveRecipeUseCase(capture(savedRecipe)) } returns recipe.recipe.id

                    viewModel.onSaveClicked()
                }

                it("should save all fields") {
                    coVerifyOrder {
                        validateIngredientsUseCase(rawIngredients)
                        saveRecipeUseCase(recipe)
                    }

                    assertEquals(recipe, savedRecipe.captured)
                }

                it("should navigate to recipe details") {
                    assertEquals(OpenRecipeDetails(recipe.recipe), viewModel.navigationEvents.value)
                }
            }

            context("on parse error") {
                beforeEachTest {
                    viewModel.ingredientsModel.observeForever {}
                    viewModel.ingredients.value = "500g sugar"
                    coEvery { validateIngredientsUseCase(any()) } returns Result.Error(Exception("500g sugar"))
                    viewModel.onSaveClicked()
                }

                it("should show error message") {
                    assertEquals(
                        Text.Resource(R.string.error_unknown_ingredient, "500g sugar"),
                        viewModel.ingredientsModel.getOrAwaitValue().ingredientsError
                    )
                }

                context("when ingredients field updated") {
                    beforeEachTest {
                        viewModel.ingredients.value = "500g sugar "
                    }

                    it("should clear error message") {
                        assertEquals(Text.Empty, viewModel.ingredientsModel.getOrAwaitValue().ingredientsError)
                    }
                }
            }
        }
    }

    describe("back pressed behavior") {
        it("should be false when no changes to fields were made") {
            val expected = viewModel.onBackPressedEnabled.getOrAwaitValue()
            assertFalse(expected)
        }

        context("when title is changed") {
            beforeEachTest {
                viewModel.title.value = "title"
            }

            it("should be true") {
                val expected = viewModel.onBackPressedEnabled.getOrAwaitValue()
                assertTrue(expected)
            }
        }

        context("when ingredients is changed") {
            beforeEachTest {
                viewModel.ingredients.value = "ingredients"
            }

            it("should be true") {
                val expected = viewModel.onBackPressedEnabled.getOrAwaitValue()
                assertTrue(expected)
            }
        }

        context("when serves value is changed") {
            beforeEachTest {
                viewModel.servesValue.value = "serves value"
            }

            it("should be true") {
                val expected = viewModel.onBackPressedEnabled.getOrAwaitValue()
                assertTrue(expected)
            }
        }

        context("when serves type is changed") {
            beforeEachTest {
                viewModel.servesType.value = "title"
            }

            it("should be true") {
                val expected = viewModel.onBackPressedEnabled.getOrAwaitValue()
                assertTrue(expected)
            }
        }

        context("when back is pressed") {
            beforeEachTest {
                viewModel.onBackPressed()
            }

            it("should emit confirmExitAction") {
                assertEquals(ShowConfirmExitDialog(false), viewModel.navigationEvents.value)
            }
        }

        context("when up is pressed and has changes") {
            beforeEachTest {
                viewModel.onBackPressedEnabled.observeForever {}
                viewModel.title.value = "title"
                viewModel.onUpPressed()
            }

            it("should emit confirmExitAction") {
                assertEquals(ShowConfirmExitDialog(true), viewModel.navigationEvents.value)
            }
        }

        context("when up is pressed and has no changes") {
            beforeEachTest {
                viewModel.onBackPressedEnabled.observeForever {}
                viewModel.onUpPressed()
            }

            it("should emit exitAction") {
                assertEquals(Exit(true), viewModel.navigationEvents.value)
            }
        }

        context("when exit confirmed") {
            beforeEachTest {
                viewModel.onExitActionConfirmed(false)
            }

            it("should exit") {
                assertEquals(Exit(false), viewModel.navigationEvents.value)
            }
        }
    }

    describe("import recipe data") {
        val data = "ingredients"

        beforeEachTest {
            coEvery { sanitizeIngredientsDataUseCase(data) } returns data
        }

        context("on input data") {
            beforeEachTest {
                viewModel.handleImportRecipeData(data)
            }

            it("should sanitize input data") {
                coVerify { sanitizeIngredientsDataUseCase(data) }
            }
        }

        context("when ingredients field is empty") {
            beforeEachTest {
                viewModel.ingredients.value = ""
                viewModel.handleImportRecipeData(data)
            }

            it("should replace ingredients text") {
                assertEquals(data, viewModel.ingredients.getOrAwaitValue())
            }
        }

        context("when ingredients field is not empty") {
            beforeEachTest {
                viewModel.ingredients.value = data
                viewModel.handleImportRecipeData(data)
            }

            it("should concat ingredients text") {
                val expected = data + CONCAT_GLUE + data
                assertEquals(expected, viewModel.ingredients.getOrAwaitValue())
            }
        }

        context("when recipe data is empty") {
            beforeEachTest {
                coEvery { sanitizeIngredientsDataUseCase("") } returns ""
                viewModel.ingredients.value = data
                viewModel.handleImportRecipeData("")
            }

            it("should not change ingredients text") {
                assertEquals(data, viewModel.ingredients.getOrAwaitValue())
            }
        }
    }

    describe("ingredients text style") {
        context("when text updated") {
            beforeEachTest {
                val input = "flour 500g"
                coEvery { parseIngredientsUseCase(input) } returns listOf(
                    ParseResult.Ingredient(Ingredient("flour", 500.g), 6..9)
                )

                viewModel.ingredients.value = "flour 500g"
            }

            it("should emit corresponding styles") {
                val actual = viewModel.ingredientsTextStyle.getOrAwaitValue()
                val expected = listOf(TextStyle.Amount(6..9))
                assertEquals(expected, actual)
            }
        }
    }

    describe("import button") {
        context("on click first time") {
            beforeEachTest {
                every { appSettings.shouldShowImportIngredientsFtue } returns true
                every { appSettings.shouldShowImportIngredientsFtue = false } just Runs
                viewModel.onImportButtonClicked()
            }

            it("should show ftue dialog") {
                assertEquals(ShowImportIngredientsFtueDialog, viewModel.navigationEvents.value)
            }

            it("should reset ftue flag") {
                verify { appSettings.shouldShowImportIngredientsFtue = false }
            }
        }

        context("on click") {
            beforeEachTest {
                every { appSettings.shouldShowImportIngredientsFtue } returns false
                viewModel.onImportButtonClicked()
            }

            it("should start crop photo flow") {
                assertEquals(OpenPhotoCropper, viewModel.navigationEvents.value)
            }
        }
    }

    describe("photo selected") {
        context("when called") {
            val uri = "uri://to/file"
            val text = "recognized text"

            beforeEachTest {
                coEvery { recognizeTextUseCase(uri) } returns Result.Success(text)
                coEvery { sanitizeIngredientsDataUseCase(text) } returns text

                testDispatcher.pauseDispatcher()
                viewModel.onPhotoSelected(uri)
            }

            it("should show progress while text is being recognized") {
                assertTrue(viewModel.ingredientsModel.getOrAwaitValue().importProgressVisible)
                testDispatcher.resumeDispatcher()
            }

            it("should hide progress after text was recognized") {
                testDispatcher.resumeDispatcher()
                assertFalse(viewModel.ingredientsModel.getOrAwaitValue().importProgressVisible)
            }
        }

        context("on error") {
            val error = "error text"

            beforeEachTest {
                val uri = "uri://to/file"

                coEvery { recognizeTextUseCase(uri) } returns Result.Error(Exception(error))
                viewModel.onPhotoSelected(uri)
            }

            it("should show error") {
                assertEquals(AddRecipeAction.ShowMessage(error), viewModel.actions.value)
            }
        }
    }

    describe("on serves type selected") {
        beforeEachTest {
            viewModel.servesValueModel.observeForever {}
            viewModel.onServesTypeSelected(AddRecipeViewModel.SERVES_TYPE_VALUES.indexOf(R.string.serves_type_ring_diameter))
        }

        it("should update servesValueModel") {
            assertEquals(
                ServesValueModel(prefixResId = Text.Resource(R.string.helper_text_serves_type_ring_diameter)),
                viewModel.servesValueModel.value
            )
        }

        context("when recipe is saved") {
            val recipe = TestData.unsavedRecipe
            val rawIngredients = recipe.ingredients.joinToString("\n") { it.asString() }
            val savedRecipe = slot<FullRecipe>()

            beforeEachTest {
                viewModel.ingredients.value = rawIngredients
                viewModel.servesValue.value = recipe.serving.value.toString()
                coEvery { validateIngredientsUseCase(rawIngredients) } returns Result.Success(recipe.ingredients)
                coEvery { saveRecipeUseCase(capture(savedRecipe)) } returns recipe.recipe.id
                viewModel.onSaveClicked()
            }

            it("should update serves type") {
                assertEquals(Serving.RingDiameter(recipe.serving.value), savedRecipe.captured.serving)
            }
        }
    }
})