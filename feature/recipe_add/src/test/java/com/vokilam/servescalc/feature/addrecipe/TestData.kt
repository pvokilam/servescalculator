package com.vokilam.servescalc.feature.addrecipe

import com.vokilam.servescalc.domain.model.Ingredient
import com.vokilam.servescalc.domain.model.g
import com.vokilam.servescalc.domain.model.ParseResult

object TestData {
    val parseData = listOf<Pair<String, ParseResult>>(
        "flour 500g" to ParseResult.Ingredient(Ingredient("flour", 500.g), IntRange.EMPTY)
    )
}