package com.vokilam.servescalc.feature.addrecipe.domain.usecase

import com.vokilam.servescalc.domain.CoroutinesDispatcherProvider
import com.vokilam.servescalc.domain.model.Amount
import com.vokilam.servescalc.domain.model.Ingredient
import com.vokilam.servescalc.domain.model.g
import com.vokilam.servescalc.domain.model.ParseResult
import com.vokilam.servescalc.domain.parser.IngredientsParser
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class SanitizeIngredientsDataUseCaseTest {

    lateinit var useCase: SanitizeIngredientsDataUseCase
    private val parser = mockk<IngredientsParser>()

    @Before
    fun setUp() {
        useCase = SanitizeIngredientsDataUseCase(
            parser,
            CoroutinesDispatcherProvider(TestCoroutineDispatcher())
        )
    }

    @Test
    fun `when line contains amount only should join with previous line`() = runBlockingTest {
        val data = """
            ingredients
            
            
            flour
            500g
            sugar
             40g
            2 eggs
            salt
            to taste
            flour for dusting
        """.trimIndent()

        coEvery { parser.parse(any<List<String>>()) } returns listOf(
            ParseResult.Ingredient(Ingredient("ingredients", Amount.Empty), IntRange.EMPTY),
            ParseResult.Blank,
            ParseResult.Blank,
            ParseResult.Ingredient(Ingredient("flour", Amount.Empty), IntRange.EMPTY),
            ParseResult.Amount(500.g),
            ParseResult.Ingredient(Ingredient("sugar", Amount.Empty), IntRange.EMPTY),
            ParseResult.Amount(40.g),
            ParseResult.Ingredient(Ingredient("eggs", Amount.Countable(2f)), IntRange.EMPTY),
            ParseResult.Ingredient(Ingredient("salt", Amount.Empty), IntRange.EMPTY),
            ParseResult.Amount(Amount.Unmeasurable("to taste")),
            ParseResult.Amount(Amount.Unmeasurable("flour for dusting"))
        )

        val expected = """
            ingredients
            
            
            flour 500g
            sugar 40g
            2 eggs
            salt to taste
            flour for dusting
        """.trimIndent()
        val actual = useCase(data)

        assertEquals(expected, actual)
    }


    @Test
    fun `single line test`() = runBlockingTest {
        val data = "ingredients"

        coEvery { parser.parse(any<List<String>>()) } returns listOf(
            ParseResult.Ingredient(Ingredient("ingredients", Amount.Empty), IntRange.EMPTY)
        )

        val actual = useCase(data)

        assertEquals(data, actual)
    }
}